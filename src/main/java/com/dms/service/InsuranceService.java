package com.dms.service;

//import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.InsuranceDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

/**
 * Service Interface for managing insurance.
 */
public interface InsuranceService {

  
	/**
     * Save a insurance.
     *
     * @param insuranceDTO the entity to save
     * @return the persisted entity
     */
	InsuranceDTO save(InsuranceDTO insuranceDTO);
    /**
     * Get all the Insurance.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<InsuranceDTO> findAll(Pageable pageable);

    /**
     * Get the "id" Insurance.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<InsuranceDTO> findOne(Long id);

    /**
     * Delete the "id" Insurance.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

}
