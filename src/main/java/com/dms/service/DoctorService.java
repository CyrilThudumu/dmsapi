package com.dms.service;

import com.dms.domain.Doctor;
import com.dms.service.dto.DoctorDTO;
import com.dms.service.dto.PatientDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Doctor.
 */
public interface DoctorService {

    /**
     * Save a doctor.
     *
     * @param doctorDTO the entity to save
     * @return the persisted entity
     */
    DoctorDTO save(DoctorDTO doctorDTO);

    /**
     * Get all the doctors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DoctorDTO> findAll(Pageable pageable);
    
    /**
     * Get all the doctors for practice.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DoctorDTO> findAllByPractice(String practiceCode, Pageable pageable);
    
    


    /**
     * Get the "id" doctor.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DoctorDTO> findOne(Long id);
    
    /**
     * Get the "id" doctor.
     *
     * @param id the id of the entity
     * @return the entity
     */
  //  Optional<DoctorDTO> findOne(String id);
    
    DoctorDTO findOneByDoctorCode(String doctorCode);

    /**
     * Delete the "id" doctor.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
