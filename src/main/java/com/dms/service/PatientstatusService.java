package com.dms.service;

import com.dms.service.dto.PatientstatusDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Patientstatus.
 */
public interface PatientstatusService {

    /**
     * Save a patientstatus.
     *
     * @param patientstatusDTO the entity to save
     * @return the persisted entity
     */
    PatientstatusDTO save(PatientstatusDTO patientstatusDTO);

    /**
     * Get all the patientstatuses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PatientstatusDTO> findAll(Pageable pageable);


    /**
     * Get the "id" patientstatus.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PatientstatusDTO> findOne(Long id);

    /**
     * Delete the "id" patientstatus.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
