package com.dms.service;

import java.util.List;


import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.Documententry;
import com.dms.domain.*; // for static metamodels
import com.dms.repository.DocumententryRepository;
import com.dms.service.dto.DocumententryCriteria;
import com.dms.service.dto.DocumententryDTO;
import com.dms.service.mapper.DocumententryMapper;

/**
 * Service for executing complex queries for Documententry entities in the database.
 * The main input is a {@link DocumententryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DocumententryDTO} or a {@link Page} of {@link DocumententryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DocumententryQueryService extends QueryService<Documententry> {

    private final Logger log = LoggerFactory.getLogger(DocumententryQueryService.class);

    private final DocumententryRepository documententryRepository;

    private final DocumententryMapper documententryMapper;

    public DocumententryQueryService(DocumententryRepository documententryRepository, DocumententryMapper documententryMapper) {
        this.documententryRepository = documententryRepository;
        this.documententryMapper = documententryMapper;
    }

    /**
     * Return a {@link List} of {@link DocumententryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DocumententryDTO> findByCriteria(DocumententryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Documententry> specification = createSpecification(criteria);
        return documententryMapper.toDto(documententryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DocumententryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DocumententryDTO> findByCriteria(DocumententryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Documententry> specification = createSpecification(criteria);
        return documententryRepository.findAll(specification, page)
            .map(documententryMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DocumententryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Documententry> specification = createSpecification(criteria);
        return documententryRepository.count(specification);
    }

    /**
     * Function to convert DocumententryCriteria to a {@link Specification}
     */
    private Specification<Documententry> createSpecification(DocumententryCriteria criteria) {
        Specification<Documententry> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Documententry_.id));
            }
            if (criteria.getDocumentCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDocumentCode(), Documententry_.documentCode));
            }
            if (criteria.getDocumentName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDocumentName(), Documententry_.documentName));
            }
            if (criteria.getFromField() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFromField(), Documententry_.fromField));
            }
            if (criteria.getToField() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getToField(), Documententry_.toField));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Documententry_.notes));
            }
            if (criteria.getFromTo1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo1(), Documententry_.fromTo1));
            }
            if (criteria.getFromTo2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo2(), Documententry_.fromTo2));
            }
            if (criteria.getFromTo3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo3(), Documententry_.fromTo3));
            }
            if (criteria.getFromTo4() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo4(), Documententry_.fromTo4));
            }
            if (criteria.getFromTo5() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo5(), Documententry_.fromTo5));
            }
            if (criteria.getFromTo6() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo6(), Documententry_.fromTo6));
            }
            if (criteria.getFromTo7() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo7(), Documententry_.fromTo7));
            }
            if (criteria.getFromTo8() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo8(), Documententry_.fromTo8));
            }
            if (criteria.getFromTo9() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFromTo9(), Documententry_.fromTo9));
            }
            if (criteria.getDocumenttypeForfromTo1Id() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeForfromTo1Id(),
                    root -> root.join(Documententry_.documenttypeForfromTo1, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getDocumenttypeForfromTo2Id() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeForfromTo2Id(),
                    root -> root.join(Documententry_.documenttypeForfromTo2, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getDocumenttypeForfromTo3Id() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeForfromTo3Id(),
                    root -> root.join(Documententry_.documenttypeForfromTo3, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getDocumenttypeForfromTo4Id() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeForfromTo4Id(),
                    root -> root.join(Documententry_.documenttypeForfromTo4, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getDocumenttypeForfromTo5Id() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeForfromTo5Id(),
                    root -> root.join(Documententry_.documenttypeForfromTo5, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getDocumenttypeForfromTo6Id() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeForfromTo6Id(),
                    root -> root.join(Documententry_.documenttypeForfromTo6, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getPatientId() != null) {
                specification = specification.and(buildSpecification(criteria.getPatientId(),
                    root -> root.join(Documententry_.patient, JoinType.LEFT).get(Patient_.id)));
            }
        }
        return specification;
    }
}
