package com.dms.service;
//import java.util.function.Predicate;
import io.github.jhipster.service.QueryService;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
//import org.apache.commons.lang3.StringUtils;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Caselog;
import com.dms.domain.Documents;
//import com.dms.domain.Documents_;
import com.dms.domain.Patient; // for static metamodels
//import com.dms.domain.Practice;
//import com.dms.domain.Practice_;
import com.dms.repository.CaselogRepository;
import com.dms.repository.DocumentsRepository;
import com.dms.repository.PatientRepository;
import com.dms.service.dto.PatientCriteria;
//import com.dms.service.dto.PatientSearchCriteria;
import com.dms.service.dto.ReportsDTO;
import com.dms.service.mapper.PatientMapper;

import java.util.Arrays;
//import java.util.HashSet;
import java.util.Map;
//import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import java.util.stream.Collectors;
/**
 * Service for executing complex queries for Patient entities in the database.
 * The main input is a {@link PatientCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReportsDTO} or a {@link Page} of {@link ReportsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReportsQueryService extends QueryService<Patient> {

    private final Logger log = LoggerFactory.getLogger(ReportsQueryService.class);

    private final DocumentsRepository documentRepository;

    private final PatientMapper patientMapper;
    
    private final PatientRepository patientRepository;
    private final CaselogRepository caselogRepository;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public ReportsQueryService(CaselogRepository caselogRepository, DocumentsRepository documentRepository, PatientMapper patientMapper, PatientRepository patientRepository) {
       this.patientRepository = patientRepository;
    	this.documentRepository=documentRepository;
        this.patientMapper = patientMapper;
        this.caselogRepository=caselogRepository;
        
    }

    /**
     * Return a {@link List} of {@link PatientDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
 /*   @Transactional(readOnly = true)
    public List<PatientDTO> findByCriteria(ReportsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Reports> specification = createSpecification(criteria);
        return patientMapper.toDto(patientRepository.findAll(specification));
    }*/

   
    
    /**
     * Function to convert PatientCriteria to a {@link Specification}
     */
    /* private Specification<Patient> createSpecification(ReportsCriteria criteria) {
        Specification<Patient> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Patient_.id));
            }
            if (criteria.getPatientCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPatientCode(), Patient_.patientCode));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), Patient_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), Patient_.lastName));
            }
            if (criteria.getSex() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSex(), Patient_.sex));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Patient_.address));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), Patient_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), Patient_.state));
            }
            if (criteria.getZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getZipCode(), Patient_.zipCode));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), Patient_.email));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), Patient_.mobileNo));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), Patient_.enable));
            }
            if (criteria.getPatientStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPatientStatus(), Patient_.patientStatus));
            }
            if (criteria.getStatusNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatusNotes(), Patient_.statusNotes));
            }
            if (criteria.getDob() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDob(), Patient_.dob));
            }
            if (criteria.getCaseType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCaseType(), Patient_.caseType));
            }
            if (criteria.getWorkPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWorkPhone(), Patient_.workPhone));
            }
            if (criteria.getAge() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAge(), Patient_.age));
            }
            if (criteria.getInsFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsFirstName(), Patient_.insFirstName));
            }
            if (criteria.getInsLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsLastName(), Patient_.insLastName));
            }
            if (criteria.getInsInitial() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsInitial(), Patient_.insInitial));
            }
            if (criteria.getInsAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsAddress(), Patient_.insAddress));
            }
            if (criteria.getInsCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsCity(), Patient_.insCity));
            }
            if (criteria.getInsState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsState(), Patient_.insState));
            }
            if (criteria.getInsZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInsZipCode(), Patient_.insZipCode));
            }
            if (criteria.getInsPhoneNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsPhoneNo(), Patient_.insPhoneNo));
            }
            if (criteria.getPrimaryInsCo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsCo(), Patient_.primaryInsCo));
            }
            if (criteria.getPrimaryInsClaimNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsClaimNo(), Patient_.primaryInsClaimNo));
            }
            if (criteria.getPrimaryInsPolicyNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsPolicyNo(), Patient_.primaryInsPolicyNo));
            }
            if (criteria.getPrimaryInsRelation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsRelation(), Patient_.primaryInsRelation));
            }
            if (criteria.getPrimaryInsCertifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsCertifier(), Patient_.primaryInsCertifier));
            }
            
            if (criteria.getPrimaryInsAdjuster() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsAdjuster(), Patient_.primaryInsAdjuster));
            }
            if (criteria.getPrimaryInsAdjusterFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsAdjusterFax(), Patient_.primaryInsAdjusterFax));
            }
            
            if (criteria.getPrimaryInsNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsNotes(), Patient_.primaryInsNotes));
            }
            if (criteria.getDoa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoa(), Patient_.doa));
            }
            
            if (criteria.getInsLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsLastName(), Patient_.insLastName));
            }
            if (criteria.getInsFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsFirstName(), Patient_.insFirstName));
            }
            
            if (criteria.getInsInitial() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsInitial(), Patient_.insInitial));
            }
            if (criteria.getInsAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsAddress(), Patient_.insAddress));
            }
            
            if (criteria.getInsCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsCity(), Patient_.insCity));
            }
            if (criteria.getInsState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsState(), Patient_.insState));
            }
            if (criteria.getInsZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInsZipCode(), Patient_.insZipCode));
            }
            if (criteria.getInsPhoneNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsPhoneNo(), Patient_.insPhoneNo));
            }
            
            if (criteria.getSecInsClaimNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsClaimNo(), Patient_.secInsClaimNo));
            }
            if (criteria.getSecInsPolicyNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsPolicyNo(), Patient_.secInsPolicyNo));
            }
            if (criteria.getSecInsRelation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsRelation(), Patient_.secInsRelation));
            }
            if (criteria.getSecInsNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsNotes(), Patient_.secInsNotes));
            }
            
            if (criteria.getSecInsCertifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsCertifier(), Patient_.secInsCertifier));
            }
            
            if (criteria.getSecInsAdjuster() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsAdjuster(), Patient_.secInsAdjuster));
            }
            
            if (criteria.getSecInsAdjusterFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsAdjusterFax(), Patient_.secInsAdjusterFax));
            }
            
            if (criteria.getAddnInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddnInfo(), Patient_.addnInfo));
            }
            
            if (criteria.getAttorneyFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyFirstName(), Patient_.attorneyFirstName));
            }
            if (criteria.getAttorneyLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyLastName(), Patient_.attorneyLastName));
            }

            if (criteria.getAttorneyCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyCity(), Patient_.attorneyCity));
            }
            if (criteria.getAttorneyState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyState(), Patient_.attorneyState));
            }

            if (criteria.getAttorneyFaxNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyFaxNo(), Patient_.attorneyFaxNo));
            }
            if (criteria.getAttorneyMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyMobileNo(), Patient_.attorneyMobileNo));
            }

            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Patient_.createdBy));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), Patient_.creationDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), Patient_.updatedBy));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), Patient_.updatedDate));
            }
            if(criteria.getPracticeCode() != null){
            	specification = specification.and(buildStringSpecification(criteria.getPracticeCode(), Patient_.practiceCode));
            }
            if (criteria.getAttorneyLogin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyLogin(), Patient_.attorneyLogin));
            }
            if (criteria.getDoctorLogin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDoctorLogin(), Patient_.doctorLogin));
            }

            if (criteria.getDoctorId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDoctorId(), Patient_.doctorId));
            }

            if (criteria.getAttorneyId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyId(), Patient_.attorneyId));
            }
            
//            if (criteria.getAttornyId() != null) {
//                specification = specification.and(buildSpecification(criteria.getAttornyId(),
//                    root -> root.join(Patient_.attorny, JoinType.LEFT).get(User_.id)));
//            }
//            if (criteria.getDoctorId() != null) {
//                specification = specification.and(buildSpecification(criteria.getDoctorId(),
//                    root -> root.join(Patient_.doctor, JoinType.LEFT).get(User_.id)));
//            }
            if (criteria.getEdocumentId() != null) {
                specification = specification.and(buildSpecification(criteria.getEdocumentId(),
                    root -> root.join(Patient_.edocuments, JoinType.LEFT).get(Edocument_.id)));
            }
        }
        return specification;
    }*/
    
   
  
	 public List<Documents> searchReport(Documents documents) {
	    	List<Documents> document = documentRepository.findAll(new Specification<Documents>(){
				public Predicate toPredicate(Root<Documents> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					List<Predicate> predicates = new ArrayList<Predicate>();
					if (documents.getPracticeCode() != null) {
						  predicates.add(cb.equal(cb.lower(root.get("practiceCode")), documents.getPracticeCode().toLowerCase()));
					}
					if (documents.getPatientCode() != null) {
						  predicates.add(cb.equal(cb.lower(root.get("patientCode")), documents.getPatientCode().toLowerCase()));
					}
				
					if (documents.getStatus() != null) {
					    predicates.add(cb.equal(cb.lower(root.get("status")), documents.getStatus().toLowerCase()));
					}
	     			if (documents.getAttorneyCode() != null) {				
	     			  predicates.add(cb.equal(cb.lower(root.get("attorneyCode")), documents.getAttorneyCode().toLowerCase()));
					}
					if (documents.getFromDos() != null) {
						predicates.add(cb.greaterThanOrEqualTo(root.get("fromDos"), documents.getFromDos()));
					}
					if (documents.getToDos() != null) {
						predicates.add(cb.lessThanOrEqualTo(root.get("toDos"), documents.getToDos()));
					}
				
					if(documents.isProcessedFlag()!=null) {
						if(documents.isProcessedFlag()==false) {
						Predicate processedFalse=	cb.isFalse(root.get("processedFlag"));
						//Predicate isAddendumToPriorFlagNull=cb.isNull(root.get("addendumToPriorFlag"));
						Predicate isAddendumToPriorFlagFalse=cb.isFalse(root.get("addendumToPriorFlag"));
						predicates.add(cb.and(processedFalse, isAddendumToPriorFlagFalse));
							log.debug(" new documents processed flag is "+documents.isProcessedFlag());
						}
						else if(documents.isProcessedFlag()==true) {
							predicates.add(cb.isTrue(root.get("processedFlag")));
							log.debug("processed docs processed flag is "+documents.isProcessedFlag());
						}
						else {}
					}
					
					if(documents.isAddendumToPriorFlag()!=null) {
						if(documents.isAddendumToPriorFlag()==true) {
							Predicate processedfalse=cb.isFalse(root.get("processedFlag"));
							Predicate isAddendumToPriorFlagTrue=cb.isTrue(root.get("addendumToPriorFlag"));
							predicates.add(cb.and(processedfalse, isAddendumToPriorFlagTrue));
							log.debug(" addendum to prior documents  is "+documents.isAddendumToPriorFlag());
						}
						else {}
					}
					 
					return cb.and(predicates.toArray(new Predicate[0]));
				}
			});
	   
	    	return  document
	                .stream()
	                .filter(distinctByKeys(Documents::getPatientCode, Documents::getFromDos, Documents::getToDos))
	                .collect(Collectors.toList());	    	
	    }
	

	private static <T> java.util.function.Predicate<T> distinctByKeys(Function<? super T, ?> ...keyExtractors) 
	  {
	    final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();
	     
	    return t -> 
	    {
	      final List<?> keys = Arrays.stream(keyExtractors)
	                  .map(ke -> ke.apply(t))
	                  .collect(Collectors.toList());
	       
	      return seen.putIfAbsent(keys, Boolean.TRUE) == null;
	    };
	  }
    
    public List<Patient> searchPatient(Patient patients) {
    	List<Patient> patient = patientRepository.findAll(new Specification<Patient>(){
			public Predicate toPredicate(Root<Patient> root, CriteriaQuery< ?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList();
				if (patients.getFirstName() != null) {
					predicates.add(cb.like(cb.lower(root.get("firstName")), "%" + patients.getFirstName().toLowerCase() + "%"));
				}
				if (patients.getDob() != null) {
					predicates.add(cb.equal(root.get("DOB"), patients.getDob()));
				}
				if (patients.getDoa() != null) {
					predicates.add(cb.equal(root.get("DOA"), patients.getDoa()));
				}
				if (patients.getPrimaryInsClaimNo() != null) {
					predicates.add(cb.like(cb.lower(root.get("primaryIns_claim_no")), "%" + patients.getPrimaryInsClaimNo().toLowerCase() + "%"));
				}
				if (patients.getPrimaryInsPolicyNo() != null) {
					predicates.add(cb.like(cb.lower(root.get("primary_policy_no")), "%" + patients.getPrimaryInsPolicyNo().toLowerCase() + "%"));
				}
				if (patients.getPracticeCode() != null) {
					predicates.add(cb.like(cb.lower(root.get("practice_code")), 
							patients.getPracticeCode().toLowerCase()));
				}
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		});
    	return patient;
    	
    }
 
      
    public List<Caselog> searchcaselog(Caselog caselogs) {
    	List<Caselog> caselog = caselogRepository.findAll(new Specification<Caselog>(){
			public Predicate toPredicate(Root<Caselog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				if (caselogs.getPatientCode() != null) {
					predicates.add(cb.like(cb.lower(root.get("patientCode")), "%" + caselogs.getPatientCode().toLowerCase() + "%"));
				}
				if (caselogs.getCaseId() != null) {
					predicates.add(cb.like(cb.lower(root.get("caseId")), "%" +caselogs.getCaseId().toLowerCase() + "%"));
				}
     			if (caselogs.getFromDos() != null) {				
				predicates.add(cb.like(cb.lower(root.get("fromDos")), "%" +caselogs.getFromDos().toLowerCase() + "%"));
				}
     			
     			if (caselogs.getToDos() != null) {				
    				predicates.add(cb.like(cb.lower(root.get("toDos")), "%" +caselogs.getToDos().toLowerCase() + "%"));
    				}
			/*	if (caselogs.getCaseComments() != null) {
					predicates.add(cb.like(cb.lower(root.get("caseComments")), "%" +caselogs.getCaseComments().toLowerCase() + "%"));
				}*/
				
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		});
    	return caselog;
    	
    }

}
