package com.dms.service;

import java.time.Instant;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.config.Constants;
import com.dms.domain.Attorney;
import com.dms.domain.Authority;
import com.dms.domain.Doctor;
import com.dms.domain.Practice;
import com.dms.domain.User;
import com.dms.repository.AttorneyRepository;
import com.dms.repository.AuthorityRepository;
import com.dms.repository.DoctorRepository;
import com.dms.repository.PracticeRepository;
import com.dms.repository.UserRepository;
import com.dms.security.AuthoritiesConstants;
import com.dms.security.SecurityUtils;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.dto.DoctorDTO;
import com.dms.service.dto.PracticeDTO;
import com.dms.service.dto.UserDTO;
import com.dms.service.mapper.UserMapper;
import com.dms.service.util.RandomUtil;
import com.dms.web.rest.errors.EmailAlreadyUsedException;
import com.dms.web.rest.errors.InvalidPasswordException;
import com.dms.web.rest.errors.LoginAlreadyUsedException;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

     private final UserRepository userRepository;
     private final AttorneyRepository attorneyRepository;
     private final DoctorRepository doctorRepository;
     private final PasswordEncoder passwordEncoder;
     private final AuthorityRepository authorityRepository;
     private final CacheManager cacheManager;
    // private final DoctorService doctorService;
     private final PracticeRepository practiceRepository;

    public UserService(PracticeRepository practiceRepository,UserRepository userRepository, AttorneyRepository attorneyRepository, DoctorRepository doctorRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, CacheManager cacheManager) {
        this.userRepository = userRepository;
        this.attorneyRepository = attorneyRepository;
        this.doctorRepository = doctorRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
      //  this.doctorService=doctorService;
        this.practiceRepository=practiceRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                this.clearUserCaches(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                this.clearUserCaches(user);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                this.clearUserCaches(user);
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {
        userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new LoginAlreadyUsedException();
            }
        });
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin().toLowerCase());
        
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail().toLowerCase());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        newUser.setUserCode(userDTO.getUserCode());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.ADMIN).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser){
        if (existingUser.getActivated()) {
             return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        Attorney attorney = new Attorney();
        Doctor doctor = new Doctor();
        
        boolean attorneyFlag = false;
        boolean doctorFlag = false;
//        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setImageUrl(userDTO.getImageUrl());
        user.setUserCode(userDTO.getUserCode());
        user.setPhone(userDTO.getPhone());
        user.setActivationKey(null);
        user.setAttornyCode(userDTO.getAttornyCode());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        System.out.println("***********************"+(userDTO.getAuthorities()));
        
        if(userDTO.getAuthorities().contains("ROLE_ATTORNEY_ADMIN") && userDTO.getSubAttorneys()!=null) {
			System.out.println("sub attorneys are "+userDTO.getSubAttorneys());
		Set<User> usrs=userDTO.getSubAttorneys().stream()
			.map(userRepository::findOneByUserCode)
			.filter(Optional::isPresent)
			.map(Optional::get).map(subUser->{
				System.out.println("subUser is "+subUser);
				//subUser.setMasterAttorney(user);
				subUser.getMasterAttorney().add(user);
				return subUser;
			}).collect(Collectors.toSet());
		System.out.println("subUser is outside"+usrs);
			//user.setSubAttorneys(subAttorneys);
		}
        
      //  if(userDTO.getAuthorities().contains("ROLE_ATTORNEY")|| userDTO.getAuthorities().contains("ROLE_ATTORNEY_ADMIN")) { 
        if(userDTO.getAuthorities().contains("ROLE_ATTORNEY")) {
			attorney.setAttornyCode(userDTO.getUserCode());
			attorney.firstName(userDTO.getFirstName());
			attorney.lastName(userDTO.getLastName());
			attorney.phone(userDTO.getPhone());
			attorneyFlag=true;
			System.out.println("in role_attorney and role_attorney_admin true");
		}
        
        else if(userDTO.getAuthorities().contains("ROLE_DOCTOR")) {
			
			doctor.setDoctorCode(userDTO.getUserCode());
			doctor.setFirstName(userDTO.getFirstName());
			doctor.setLastName(userDTO.getLastName());
			 user.setTaxonomy(userDTO.getTaxonomy());
		     user.setProviderNpi(userDTO.getProviderNpi());
		     user.setSpeciality(userDTO.getSpeciality());
			
			if(userDTO.getPractices()!=null) {
				Set<Practice> practices=userDTO.getPractices().stream()
						.map(practiceRepository::findOneByPracticeCode)
						.filter(Optional::isPresent)
						.map(Optional::get)
						.collect(Collectors.toSet());
               user.setPractices(practices);
				//user.setPractices(userDTO.getPractices());
			  }
		
			doctorFlag = true;
		}
        
        if (userDTO.getAuthorities() != null) {
        	log.debug("userDTO.getAuthorities "+userDTO.getAuthorities());
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            log.debug("authorities after stream  "+authorities);
            user.setAuthorities(authorities);
        }
        userRepository.save(user);
        if(attorneyFlag) {
        	attorneyRepository.save(attorney);
        }
        if(doctorFlag) {
        	log.debug("doctor is "+doctor);
        	doctorRepository.save(doctor);
        }
        System.out.println("Attorney Flag"+attorneyFlag);
     /*   if(attorneyFlag) {
        	userRepository.save(user);
        }*/
       // userRepository.save(user);
       // userRepository.save(user);
        
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email.toLowerCase());
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                this.clearUserCaches(user);
                System.out.println("userDTO is "+userDTO);
                System.out.println("user from db is "+user);
                user.setLogin(userDTO.getLogin().toLowerCase());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail().toLowerCase());
                user.setImageUrl(userDTO.getImageUrl());
               // user.setActivated(userDTO.isActivated());
                user.setActivated(true);
                if(userDTO.getPassword()!=null) {
                	user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
                }
                user.setLangKey(userDTO.getLangKey());
                user.setProviderNpi(userDTO.getProviderNpi());
                user.setSpeciality(userDTO.getSpeciality());
                user.setTaxonomy(userDTO.getTaxonomy());
                user.setUserCode(userDTO.getUserCode());
                user.setPhone(userDTO.getPhone());
                user.setAttornyCode(userDTO.getAttornyCode());
                
               if(!user.getPractices().isEmpty() && userDTO.getAuthorities().contains("ROLE_DOCTOR")) {
                Set<Practice> practic=user.getPractices();
                System.out.println("users practices are "+practic);
                practic.clear();
                userDTO.getPractices().stream()
                .map(practiceRepository::findOneByPracticeCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(practic::add);
                }
               if(user.getPractices().isEmpty() && userDTO.getAuthorities().contains("ROLE_DOCTOR")) {
                	 Set<Practice> practice=new HashSet<>();
                	  userDTO.getPractices().stream()
                     .map(practiceRepository::findOneByPracticeCode)
                     .filter(Optional::isPresent)
                     .map(Optional::get)
                     .forEach(practice::add);
                	 user.setPractices(practice);
                }
               
               if(!user.getSubAttorneys().isEmpty() && userDTO.getAuthorities().contains("ROLE_ATTORNEY_ADMIN")) {
            	   user.getSubAttorneys().stream().forEach(u->{
            		   u.getMasterAttorney().remove(user);
            	   });
                   userDTO.getSubAttorneys().stream()
                   .map(userRepository::findOneByUserCode)
                   .filter(Optional::isPresent)
                   .map(Optional::get).forEach(usr->{
                	   usr.getMasterAttorney().add(user);
                   });
                  
                   }
               if(user.getSubAttorneys().isEmpty() && userDTO.getAuthorities().contains("ROLE_ATTORNEY_ADMIN")) {
                  /*	 Set<User> subUsrs=new HashSet<>();
                  	 System.out.println("new sub users  are ");
               	      userDTO.getSubAttorneys().stream()
                    .map(userRepository::findOneByUserCode)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(subUsrs::add);
               	 user.setSubAttorneys(subUsrs);*/
            	   Set<User> usrs=userDTO.getSubAttorneys().stream()
            				.map(userRepository::findOneByUserCode)
            				.filter(Optional::isPresent)
            				.map(Optional::get).map(subUser->{
            					System.out.println("subUser is "+subUser);
            					subUser.getMasterAttorney().add(user);
            					return subUser;
            				}).collect(Collectors.toSet());
               }
               
                Set<Authority> managedAuthorities = user.getAuthorities();
                System.out.println("managed authorities are "+managedAuthorities);
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(managedAuthorities::add);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
   // Optional<User> user1=	userRepository.findOneByLogin("user142@gmail.com");
   // System.out.println("user1 is "+user1);
    	userRepository.findOneByLogin(login).ifPresent(user -> {
        	System.out.println("deleting user "+user);
        	Set<String> userRoles=user.getAuthorities().stream().map(Authority::getName)
            .collect(Collectors.toSet());
        	if(userRoles.contains("ROLE_ATTORNEY_ADMIN")) {
        		user.getSubAttorneys().stream().forEach(u->{
         		   u.getMasterAttorney().remove(user);
         	   });
        	}
            userRepository.delete(user);
        	
            System.out.println("role is  "+user.getAuthorities());
            Doctor doctor= doctorRepository.findByDocCode(user.getUserCode());
            System.out.println("doctor is before if "+doctor);
            
            if(doctor!=null) {
        	System.out.println("deleting doctor inside if "+doctor);
            doctorRepository.delete(doctor);
           }
           
         	System.out.println("role is  "+user.getAuthorities());
         	Attorney attorney= attorneyRepository.findOneByAttorneyCode(user.getUserCode());
            System.out.println("attorney is before if "+attorney);
            if(attorney!=null) {
          	System.out.println("deleting attorney  inside if"+attorney);
           	attorneyRepository.delete(attorney);
           }
              
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
           
        });
    }

    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String currentEncryptedPassword = user.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(encryptedPassword);
                this.clearUserCaches(user);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        Page<UserDTO> pageuserdto= userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
        return pageuserdto;
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(user -> {
                log.debug("Deleting not activated user {}", user.getLogin());
                userRepository.delete(user);
                this.clearUserCaches(user);
            });
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    public Optional<User> getData(String email) {
    	return userRepository.findOneWithAuthoritiesByEmail(email);
    }
    
    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
    }
    
    /**
     * Get one user by userName.
     *
     * @param userName the userName of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Page<UserDTO> findUserInfo(String userName, Pageable page) {
        log.debug("Request to get User : {}", userName);
        return userRepository.findByUserName(userName, page).map(UserDTO::new);
            
    }
    
    @Transactional(readOnly = true)
    public Page<UserDTO> getUsersWithAuthorityAttorney(Authority name, Pageable page) {
        log.debug("Request to get User : {}", name);
        return userRepository.findAllByAuthorities(name, page).map(UserDTO::new);
            
    }
    /*
     * without pagination
     */
    @Transactional(readOnly = true)
    public List<UserDTO> getUsersWithAuthoritiesAttorney(Authority name) {
        log.debug("Request to get User : {}", name);
        return userRepository.findAllByAuthorities(name);
              
    }
    
    /**
     * Get one user by userName.
     *
     * @param userName the userName of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Page<UserDTO> findUserInfoByemail(String email, Pageable page) {
        log.debug("Request to get email : {}", email);
        return userRepository.findOneWithAuthoritiesByEmail(email,page).map(UserDTO::new);
            
    }
    @Transactional(readOnly = true)
    public Optional<UserDTO> getUser(String userCode) {
        Optional<UserDTO> userdto= userRepository.findByUserCode(userCode).map(UserDTO::new);
       // System.out.println("u is "+u);
        return userdto;
    }
 /*   @Transactional(readOnly = true)
    public Set<UserDTO> getSubAttorneys() {
        Optional<User> user=SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
        Set<UserDTO> usersdto=user.get().getSubAttorneys().stream().map(UserDTO::new).collect(Collectors.toSet());
        return usersdto;
    }*/
    
}
