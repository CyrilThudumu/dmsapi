package com.dms.service;

import io.github.jhipster.service.QueryService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Casetype;
import com.dms.domain.Certifier;
import com.dms.domain.Certifier_;
import com.dms.repository.CasetypeRepository;
import com.dms.repository.CertifierRepository;
import com.dms.service.dto.CasetypeCriteria;
import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.CertifierCriteria;
import com.dms.service.dto.CertifierDTO;
import com.dms.service.mapper.CasetypeMapper;
// for static metamodels
import com.dms.service.mapper.CertifierMapper;

/**
 * Service for executing complex queries for Certifier entities in the database.
 * The main input is a {@link CertifierCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CertifierDTO} or a {@link Page} of {@link CertifierDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CertifierQueryService extends QueryService<Certifier> {

    private final Logger log = LoggerFactory.getLogger(CertifierQueryService.class);

    private final CertifierRepository certifierRepository;

    private final CertifierMapper certifierMapper;

    public CertifierQueryService(CertifierRepository certifierRepository, CertifierMapper certifierMapper) {
        this.certifierRepository = certifierRepository;
        this.certifierMapper = certifierMapper;
    }

  
    /**
     * Return a {@link Page} of {@link CasetypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CertifierDTO> findByCriteria(CertifierCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Certifier> specification = createSpecification(criteria);
      //  log.debug("$$$$$$$$$$$$ From DB:  "+certifierRepository.findAll(specification, page).getContent());
        return certifierRepository.findAll(specification, page)
            .map(certifierMapper::toDto);
    }

  
    /**
     * Function to convert CertifierCriteria to a {@link Specification}
     */
    private Specification<Certifier> createSpecification(CertifierCriteria criteria) {
        Specification<Certifier> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Certifier_.id));
            }
            if (criteria.getCertifierCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCertifierCode(), Certifier_.certifierCode));
            }
            if (criteria.getCertifierName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCertifierName(), Certifier_.certifierName));
            }
        }
        return specification;
    }
}
