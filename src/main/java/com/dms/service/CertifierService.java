package com.dms.service;

import com.dms.service.dto.CasetypeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Certifier.
 */
public interface CertifierService {

  /*  */
	
	/**
     * Save a certi.
     *
     * @param casetypeDTO the entity to save
     * @return the persisted entity
     */
	
    /**
     * Get all the casetypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<CasetypeDTO> findAll(Pageable pageable);


/*    */

}
