package com.dms.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Attorney;
import com.dms.domain.Caselog;
import com.dms.domain.Doctor;
import com.dms.domain.Documents;
import com.dms.domain.User;
import com.dms.repository.DocumentsRepository;
import com.dms.service.DocumentsService;
import com.dms.service.dto.CaselogDTO;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.mapper.DocumentsMapper;

/**
 * Service Implementation for managing documents.
 */
@Service
@Transactional
public class DocumentServiceImpl implements DocumentsService {

    private final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

    private final DocumentsRepository documentsRepository;

    private final DocumentsMapper documentsMapper;

   
	public DocumentServiceImpl(DocumentsRepository documentsRepository, DocumentsMapper documentsMapper) {
        this.documentsRepository = documentsRepository;
        this.documentsMapper = documentsMapper;
    }
	
	
	@Override
	    public DocumentsDTO getAttorneyDos(DocumentsDTO documentsDTO) {
	        log.debug("Request to get Document : {}", documentsDTO);
	      DocumentsDTO documentsdto = documentsMapper.toDto(documentsRepository.getAttorneyForDos(documentsDTO.getPatientCode(), documentsDTO.getPracticeCode(), documentsDTO.getFromDos(), documentsDTO.getToDos()));
	       // DocumentsDTO documentsdto = documentsMapper.toDto(documentsRepository.getAttorneyForDos(documentsDTO.getPatientCode(), documentsDTO.getPracticeCode()));
	        return documentsdto;
	    } 
	    
	    @Override
	    public Documents previousAssignedAttorneyDates(DocumentsDTO documentsDTO) {
	        log.debug("Request to get Document : {}", documentsDTO);
	        Documents documents = documentsRepository.previousAssignedAttorneyDates(documentsDTO.getPatientCode(), documentsDTO.getPracticeCode(), documentsDTO.getAttorneyCode(), documentsDTO.getAttorneyAssignedDate());
	        return documents;
	    }
    /**
     * Save a document.
     *
     * @param documentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DocumentsDTO save(DocumentsDTO documentsDTO) {
        log.debug("Request to save Document : {}", documentsDTO);

        Documents documents = documentsMapper.toEntity(documentsDTO);
        documents = documentsRepository.save(documents);
        return documentsMapper.toDto(documents);
    }
    
   /* @Override
    public CaselogDTO savecaselog(CaselogDTO caselogDTO) {
        log.debug("Request to save Document : {}", caselogDTO);
        Caselog caselog = new Caselog();
       
        
     
        caselog.setCaseId(caselogDTO.getCaseId());
        caselog.setPatientCode(caselogDTO.getPatientCode());
        caselog.setFromDos(caselogDTO.getFromDos());
        caselog.setToDos(caselogDTO.getToDos());
        caselog.setCaseComments(caselogDTO.getCaseComments());
        
        userRepository.save(user);
     
    }*/
    
    /**
	 * Get DOS for a  patient based on patientCode.
	 *
	 * @param documentsDTO the DocumentsDTO of the DTO
	 * @return the Documents Entity
	 */
    /*@Transactional(readOnly = true)
    @Override
    public Page<DocumentsDTO> findByPatientCode(DocumentsDTO documentsDTO, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", documentsDTO);
        return documentsRepository.findByPatientCode(documentsDTO.getPatientCode(),documentsDTO.getFromDos(),documentsDTO.getToDos(), page).map(documentsMapper::toDto);
        
    }*/
    
    @Override
    @Transactional
    public void updatePatientDOS(DocumentsDTO documentsDTO){
        log.info("Finding Docoument entries by search term:{} and page request:{}", documentsDTO);
       // System.out.println("update patient dos document dto is "+documentsDTO);
       log.debug("Attorney code in : "+documentsDTO.getAttorneyCode()+ "document dto is "+documentsDTO);
       /* if(!(documentsDTO.getAttorneyCode().equals("")) || documentsDTO.getAttorneyCode().equalsIgnoreCase("All Practices")|| documentsDTO.getAttorneyCode().equalsIgnoreCase("-NA-")){
        	documentsDTO.setAttorneyCode("-NA-");, documentsDTO.isAddendumToPriorFlag()
        	log.info("changing attorney code if it doesn't have proper values..");
        }*/
       log.debug("addendum flag is "+documentsDTO.isAddendumToPriorFlag());
       log.debug("getAttorneyAssignedDate  is "+documentsDTO.getAttorneyAssignedDate());
        documentsRepository.updatePatientDOS(documentsDTO.getStatus(),documentsDTO.getStatusDate(), documentsDTO.getAttorneyAssignedDate(), Boolean.valueOf(documentsDTO.isAddendumToPriorFlag()) ,documentsDTO.getArbAmount(),
        		documentsDTO.getSentDate(),documentsDTO.getPaymentAmount(),documentsDTO.getPaymentDate(),documentsDTO.getArbNotes(),
        		documentsDTO.getNatureOfDispute(),documentsDTO.getAttorneyCode(),documentsDTO.getPatientCode(),documentsDTO.getFromDos(),
        		documentsDTO.getToDos());
    }
    /**
	 * Get DOS for a  patient based on doctorCode.
	 *
	 * @param documentsDTO the DocumentsDTO of the DTO
	 * @return the Documents Entity
	 */
    
    @Transactional(readOnly = true)
    @Override
    public Page<DocumentsDTO> findByDoctorCode(String doctorCode,String patientCode, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", doctorCode, patientCode);
        return documentsRepository.getDOSForDoctor(doctorCode,patientCode, page).map(documentsMapper::toDto);
        
    }
    
    /**
	 * Get DOS for a  patient based on patientCode.
	 *
	 * @param documentsDTO the DocumentsDTO of the DTO
	 * @return the Documents Entity
	 */
    @Transactional(readOnly = true)
    @Override
    public Page<DocumentsDTO> findByPatientCode(String patientCode,String practiceCode, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", patientCode);
       // System.out.println("In findByPatientCode  "+documentsRepository.getDOSForPatient(patientCode,practiceCode, page).map(documentsMapper::toDto).getContent());
        return documentsRepository.getDOSForPatient(patientCode,practiceCode, page).map(documentsMapper::toDto);
        
    }
    
    /**
   	 * Get DOS for a  patient based on attorneyCode.
   	 *
   	 * @param documentsDTO the DocumentsDTO of the DTO
   	 * @return the Documents Entity
   	 */
       @Transactional(readOnly = true)
       @Override
       public Page<DocumentsDTO> findByAttorneyCode(String attorneyCode, String patientCode, Pageable page) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
           return documentsRepository.getDOSForAttorney(attorneyCode,patientCode, page).map(documentsMapper::toDto);
           
       }
       /**
      	 * Get DOS for a  patient based on attorneyCode.
      	 *
      	 * @param documentsDTO the DocumentsDTO of the DTO
      	 * @return the Documents Entity
      	 */
       
       @Transactional(readOnly = true)
       @Override
     //  public Page<DocumentsDTO> getAttorneyNotProcessedDOS(String attorneyCode, String patientCode, Pageable page) {
       public Page<DocumentsDTO> getAttorneyNotProcessedDOS(String attorneyCode, String patientCode, String status, Pageable page) {
           log.info("Finding Docoument entries by search term: {} and page request: {} ", attorneyCode,patientCode);
         //  return documentsRepository.findAttorneyNotProcessedDOS(attorneyCode, patientCode, page).map(documentsMapper::toDto);
           return documentsRepository.findAttorneyNotProcessedDOS(attorneyCode, patientCode, status, page).map(documentsMapper::toDto);
       }
       
          @Transactional(readOnly = true)
          @Override
         // public Page<DocumentsDTO> getAttorneyProcessedDOS(String attorneyCode, String patientCode, Pageable page) {
          public Page<DocumentsDTO> getAttorneyProcessedDOS(String attorneyCode, String patientCode, String status, Pageable page) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode, patientCode);
              return documentsRepository.findAttorneyProcessedDOS(attorneyCode,patientCode, status, page).map(documentsMapper::toDto);
              
          }
          
          @Transactional(readOnly = true)
          @Override
        //  public Page<DocumentsDTO> findAttorneysAddendumToPriorDOS(String attorneyCode, String patientCode, Pageable page) {
            public Page<DocumentsDTO> findAttorneysAddendumToPriorDOS(String attorneyCode, String patientCode, String status, Pageable page) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
             // return documentsRepository.findAttorneysAddendumToPriorDOS(attorneyCode,patientCode, page).map(documentsMapper::toDto);
              return documentsRepository.findAttorneysAddendumToPriorDOS(attorneyCode,patientCode, status, page).map(documentsMapper::toDto);
          }
          
         @Transactional(readOnly = true)
          @Override
          public Page<DocumentsDTO> findAttorneySearchDos(String attorneyCode, String patientCode, Pageable page) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
              return documentsRepository.findAttorneySearchDos(attorneyCode,patientCode, page).map(documentsMapper::toDto);
          }
          
          @Transactional(readOnly = true)
          @Override
          public List<String> findDocumentsByAttorneyCode(String attorneyCode) {
         // public List<Documents> findDocumentsByAttorneyCode(String attorneyCode) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
              String status="ARB";
              return documentsRepository.findDocumentsByAttorneyCode(attorneyCode, status);
          }
          
          @Transactional(readOnly = true)
          @Override
          public List<String> findPatientCodeByAttorneyCode(String attorneyCode) {
              log.info("Finding Distinct Patients entries by search term: {} and page request: {}", attorneyCode);
              String status="ARB";
              return documentsRepository.findPatientCodeByAttorneyCode(attorneyCode, status);
          }
         
       
       @Override
       @Transactional
       public void updatePatientDOSToProcessed(DocumentsDTO documentsDTO){
           log.info("Updating DOS to Processed By Attorney: {} and page request: {}", documentsDTO);
           String pattern = "yyyy-MM-dd";
   			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
   			String date = simpleDateFormat.format(new Date());
   			documentsDTO.setProcessedDate(date);
           documentsRepository.updatePatientDOSToProcessed(documentsDTO.isProcessedFlag(),documentsDTO.getProcessedDate(),documentsDTO.getAttorneyCode(), documentsDTO.getFromDos(), documentsDTO.getToDos());
       }
       
       /**
   	 * Get DOS for a  patient based on patientCode and practice code.
   	 *
   	 * @param documentsDTO the DocumentsDTO of the DTO
   	 * @return the Documents Entity
   	 */
       @Transactional(readOnly = true)
       @Override
       public Page<DocumentsDTO> downloadPatientDocs(String patientCode, String practiceCode,String fromDOS, String toDOS, Pageable page) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", patientCode);
           
           return documentsRepository.getPatientDocsDownload(patientCode,practiceCode,fromDOS,toDOS, page).map(documentsMapper::toDto);
       }
       
       @Transactional(readOnly = true)
       @Override
       public Page<DocumentsDTO> getReportsBySearch(String practiceCode, String status, String attorneyCode, String fromDate, String toDate, Pageable page) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", practiceCode);
          // System.out.println("docmntservice impl "+documentsRepository.reportsBySearch(practiceCode,status,attorneyCode,fromDate,toDate, page).map(documentsMapper::toDto).getContent());
          // System.out.println(page.getPageNumber());
           return documentsRepository.reportsBySearch(practiceCode,status,attorneyCode,fromDate,toDate, page).map(documentsMapper::toDto);
       }
       
      /* @Transactional(readOnly = true)
       @Override
       public Optional<DocumentsDTO> downloadPatientDocs(DocumentsDTO dtoObj) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", dtoObj);
           
          // Optional<Documents> documents = documentsRepository.getPatientDocsDownload(documentsDTO.getPatientCode(),documentsDTO.getPracticeCode(),documentsDTO.getFromDos(),documentsDTO.getToDos());
         
           //return documentsRepository.getPatientDocsDownload(documentsDTO.getPatientCode(),documentsDTO.getPracticeCode(),documentsDTO.getFromDos(),documentsDTO.getToDos()).map(documentsMapper::toDto);
           return documentsRepository.getPatientDocsDownload(dtoObj.getPatientCode(),dtoObj.getPracticeCode(),dtoObj.getFromDos(),dtoObj.getToDos()).map(documentsMapper::toDto);
       }*/
       
    
       /**
      	 * Get DOS for a  patient based on attorneyCode.
      	 *
      	 * @param documentsDTO the DocumentsDTO of the DTO
      	 * @return the Documents Entity
      	 */
          @Transactional(readOnly = true)
          @Override
          public Page<DocumentsDTO> findDocsHistoryByAttorneyCode(String attorneyCode, String patientCode, Pageable page) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
              return documentsRepository.getHistoryDOSForAttorney(attorneyCode,patientCode, page).map(documentsMapper::toDto);
          }
      	
      	/*@Override
      	public Page<DocumentsDTO> searchAttorneyPatientHistory(DocumentsDTO documentsDTO, Pageable page) {
      		log.debug("Request to fetch patients history For Attorney : {}", documentsDTO);
      		return documentsRepository.searchPatientHistoryByAttorney(documentsDTO.getAttorneyCode(),documentsDTO.getFromDos(),documentsDTO.getToDos(),documentsDTO.getSearchKey(), page).map(documentsMapper::toDto);
      		
      	}*/
      	
      	@Override
      	public Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode, String fromDOS, String toDOS,  Pageable page) {
      		log.debug("Request to fetch patients history For Attorney : {}", attorneyCode);
      		return documentsRepository.searchPatientHistoryByAttorney(attorneyCode,fromDOS,toDOS, page).map(documentsMapper::toDto);
      		
      	}
      	
    	@Override
      	public Page<DocumentsDTO> getDocumentsForHistorySearch(String attorneyCode, String processedFrom, String processedTo, Pageable page) {
      		log.debug("Request to fetch patients docs history For Attorney : {}", attorneyCode);
      		return documentsRepository.searchPatientHistoryDocs(attorneyCode,processedFrom,processedTo, page).map(documentsMapper::toDto);
      		
      	}
      	

    	/**
    	 * Delete the Document by id.
    	 *
    	 * @param id the id of the entity
    	 */
    	@Override
    	public void delete(Long id) {
    		log.debug("Request to delete Document : {}", id);
    		documentsRepository.deleteById(id);
    		
    	}

    	
    	/*@Override
      	public Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode, String fromDOS, String toDOS,  Pageable page) {
      		log.debug("Request to fetch patients history For Attorney : {}", attorneyCode);
      		return documentsRepository.searchPatientHistoryByAttorney(attorneyCode,fromDOS,toDOS, page).map(documentsMapper::toDto);
      		
      	}*/
      	
    	
    	@Override
        @Transactional
        public void updateCaseIdForDOS(DocumentsDTO documentsDTO){
            log.info("Update CaseId for DOS", documentsDTO);
            documentsRepository.updateCaseIdForDOS(documentsDTO.getCaseId(), documentsDTO.getCaseComments(),documentsDTO.getPatientCode(),documentsDTO.getFromDos(),documentsDTO.getToDos());
        }
}
