package com.dms.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import com.dms.domain.Casetype;
import com.dms.domain.Insurance;
//import com.dms.repository.CasetypeRepository;
import com.dms.repository.InsuranceRepository;
import com.dms.service.InsuranceService;
//import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.InsuranceDTO;
//import com.dms.service.mapper.CasetypeMapper;
import com.dms.service.mapper.InsuranceMapper;

/**
 * Service Implementation for managing insurance.
 */
@Service
@Transactional
public class InsuranceServiceImpl implements InsuranceService {

	private final Logger log = LoggerFactory.getLogger(CasetypeServiceImpl.class);

    private final InsuranceRepository insuranceRepository;

    private final InsuranceMapper insuranceMapper;

    public InsuranceServiceImpl(InsuranceRepository insuranceRepository, InsuranceMapper insuranceMapper) {
        this.insuranceRepository = insuranceRepository;
        this.insuranceMapper = insuranceMapper;
    }
    
    /**
     * Save a insurance.
     *
     * @param insuranceDTO the entity to save
     * @return the persisted entity
     */
	@Override
	public InsuranceDTO save(InsuranceDTO insuranceDTO) {
		 log.debug("Request to save insurance : {}", insuranceDTO);
	        Insurance insurance = insuranceMapper.toEntity(insuranceDTO);
	        insurance = insuranceRepository.save(insurance);
	        return insuranceMapper.toDto(insurance);
	}

	  /**
     * Get all the insurances.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
	@Override
	public Page<InsuranceDTO> findAll(Pageable pageable) {
		log.debug("Request to get all insurances");
        return insuranceRepository.findAll(pageable)
            .map(insuranceMapper::toDto);
	}
	
	
	 /**
     * Get one insurance by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<InsuranceDTO> findOne(Long id) {
        log.debug("Request to get insurance : {}", id);
        return insuranceRepository.findById(id)
            .map(insuranceMapper::toDto);
    }
    
    /**
     * Delete the insurance by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete insurance : {}", id);
        insuranceRepository.deleteById(id);
    }

}
