package com.dms.service.impl;

import com.dms.service.DoctorService;
import com.dms.domain.Doctor;
import com.dms.domain.Patient;
import com.dms.repository.DoctorRepository;
import com.dms.service.dto.DoctorDTO;
import com.dms.service.dto.PatientDTO;
import com.dms.service.mapper.DoctorMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Doctor.
 */
@Service
@Transactional
public class DoctorServiceImpl implements DoctorService {

    private final Logger log = LoggerFactory.getLogger(DoctorServiceImpl.class);

    private final DoctorRepository doctorRepository;

    private final DoctorMapper doctorMapper;

    public DoctorServiceImpl(DoctorRepository doctorRepository, DoctorMapper doctorMapper) {
        this.doctorRepository = doctorRepository;
        this.doctorMapper = doctorMapper;
    }

    /**
     * Save a doctor.
     *
     * @param doctorDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DoctorDTO save(DoctorDTO doctorDTO) {
        log.debug("Request to save Doctor : {}", doctorDTO);

        Doctor doctor = doctorMapper.toEntity(doctorDTO);
        doctor = doctorRepository.save(doctor);
        return doctorMapper.toDto(doctor);
    }

    /**
     * Get all the doctors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoctorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Doctors");
        return doctorRepository.findAll(pageable)
            .map(doctorMapper::toDto);
    }


    /**
     * Get one doctor by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DoctorDTO> findOne(Long id) {
        log.debug("Request to get Doctor : {}", id);
        return doctorRepository.findById(id)
            .map(doctorMapper::toDto);
    }
    
    
    @Override
	@Transactional(readOnly = true)
	public DoctorDTO findOneByDoctorCode(String doctorCode) {
		log.info("attorneyCode is: "+doctorCode);
		//log.info("data is: "+doctorRepository.findByDoctorCode(doctorCode));
		DoctorDTO doctor = doctorRepository.findByDoctorCode(doctorCode);
		DoctorDTO doctorDTO = new DoctorDTO();
		doctorDTO.setId(doctor.getId());
		doctorDTO.setDoctorCode(doctor.getDoctorCode());
		doctorDTO.setFirstName(doctor.getFirstName());
		doctorDTO.setLastName(doctor.getLastName());
		doctorDTO.setPhone(doctor.getPhone());
		doctorDTO.setFax(doctor.getFax());
		doctorDTO.setAddress(doctor.getAddress());
		doctorDTO.setCity(doctor.getCity());
		doctorDTO.setStateProvince(doctor.getStateProvince());
		doctorDTO.setZipCode(doctor.getZipCode());
		doctorDTO.setPractice(doctor.getPractice());
		
		return doctorDTO;
		
	}
    
  /*  @Override
    @Transactional(readOnly = true)
    public Optional<DoctorDTO> findOne(String doctorCode) {
        log.debug("Request to get Doctor : {}", doctorCode);
        return doctorRepository.findByDoctorCode(doctorCode).map(doctorMapper::toDto);
    }*/

    /**
     * Delete the doctor by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Doctor : {}", id);
        doctorRepository.deleteById(id);
    }

	@Override
	public Page<DoctorDTO> findAllByPractice(String practiceCode,
			Pageable pageable) {
		return doctorRepository.findAll(practiceCode, pageable)
	            .map(doctorMapper::toDto);
		
		// TODO Auto-generated method stub
	}
}
