package com.dms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Attorney;
import com.dms.domain.Caselog;
import com.dms.domain.Doctor;
import com.dms.domain.User;
import com.dms.repository.AttorneyRepository;
import com.dms.repository.AuthorityRepository;
import com.dms.repository.CaselogRepository;
import com.dms.repository.DoctorRepository;
import com.dms.repository.PracticeRepository;
import com.dms.repository.UserRepository;
import com.dms.service.CaseLogService;
import com.dms.service.UserService;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.dto.CaselogDTO;
import com.dms.service.mapper.AttorneyMapper;
import com.dms.service.mapper.CaselogMapper;

@Service
@Transactional
public class CaselogServiceImpl implements CaseLogService {
    	    
	  private final Logger log = LoggerFactory.getLogger(UserService.class);

	     private final CaselogRepository caselogRepository;
	     private final CaselogMapper caselogMapper;

	     public CaselogServiceImpl(CaselogRepository caselogRepository, CaselogMapper caselogMapper ) {
	    	 this.caselogRepository=caselogRepository;
	    	 this.caselogMapper=caselogMapper;
	     }
	     
	     
	     @Override
	     public CaselogDTO save(CaselogDTO caselogDTO) {
	         log.debug("Request to save caselog : {}", caselogDTO);
	         Caselog caselog = caselogMapper.toEntity(caselogDTO);
	         caselog = caselogRepository.save(caselog);
	         return caselogMapper.toDto(caselog);
	     }
	   
	 /*  public Caselog save(CaselogDTO caselogDTO) {
		
		 Caselog caselog = new Caselog();
		 caselog.setPatientCode(caselogDTO.getPatientCode());
		 caselog.setFromDos(caselogDTO.getFromDos());
		 caselog.setToDos(caselogDTO.getToDos());
		 caselog.setCaseId(caselogDTO.getCaseId());
		 caselog.setCaseComments(caselogDTO.getCaseComments());
		 caselogRepository.save(caselog);
		return caselog;
	    
	 }*/
}
