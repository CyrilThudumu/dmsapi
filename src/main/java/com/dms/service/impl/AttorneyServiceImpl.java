package com.dms.service.impl;

import com.dms.service.AttorneyService;
import com.dms.domain.Attorney;
import com.dms.repository.AttorneyRepository;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.mapper.AttorneyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Attorney.
 */
@Service
@Transactional
public class AttorneyServiceImpl implements AttorneyService {

    private final Logger log = LoggerFactory.getLogger(AttorneyServiceImpl.class);

    private final AttorneyRepository attorneyRepository;

    private final AttorneyMapper attorneyMapper;

    public AttorneyServiceImpl(AttorneyRepository attorneyRepository, AttorneyMapper attorneyMapper) {
        this.attorneyRepository = attorneyRepository;
        this.attorneyMapper = attorneyMapper;
    }

    /**
     * Save a attorney.
     *
     * @param attorneyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AttorneyDTO save(AttorneyDTO attorneyDTO) {
        log.debug("Request to save Attorney : {}", attorneyDTO);

        Attorney attorney = attorneyMapper.toEntity(attorneyDTO);
        attorney = attorneyRepository.save(attorney);
        return attorneyMapper.toDto(attorney);
    }

    /**
     * Get all the attorneys.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AttorneyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Attorneys");
        return attorneyRepository.findAll(pageable)
            .map(attorneyMapper::toDto);
    }


    /**
     * Get one attorney by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AttorneyDTO> findOne(Long id) {
        log.debug("Request to get Attorney : {}", id);
        return attorneyRepository.findById(id)
            .map(attorneyMapper::toDto);
    }

    /**
     * Delete the attorney by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Attorney : {}", id);
        attorneyRepository.deleteById(id);
    }
}
