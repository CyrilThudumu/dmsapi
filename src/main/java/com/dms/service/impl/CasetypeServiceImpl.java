package com.dms.service.impl;

import com.dms.service.CasetypeService;
import com.dms.domain.Casetype;
import com.dms.repository.CasetypeRepository;
import com.dms.service.dto.CasetypeDTO;
import com.dms.service.mapper.CasetypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Casetype.
 */
@Service
@Transactional
public class CasetypeServiceImpl implements CasetypeService {

    private final Logger log = LoggerFactory.getLogger(CasetypeServiceImpl.class);

    private final CasetypeRepository casetypeRepository;

    private final CasetypeMapper casetypeMapper;

    public CasetypeServiceImpl(CasetypeRepository casetypeRepository, CasetypeMapper casetypeMapper) {
        this.casetypeRepository = casetypeRepository;
        this.casetypeMapper = casetypeMapper;
    }

    /**
     * Save a casetype.
     *
     * @param casetypeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CasetypeDTO save(CasetypeDTO casetypeDTO) {
        log.debug("Request to save Casetype : {}", casetypeDTO);
        Casetype casetype = casetypeMapper.toEntity(casetypeDTO);
        casetype = casetypeRepository.save(casetype);
        return casetypeMapper.toDto(casetype);
    }

    /**
     * Get all the casetypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CasetypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Casetypes");
        return casetypeRepository.findAll(pageable)
            .map(casetypeMapper::toDto);
    }


    /**
     * Get one casetype by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CasetypeDTO> findOne(Long id) {
        log.debug("Request to get Casetype : {}", id);
        return casetypeRepository.findById(id)
            .map(casetypeMapper::toDto);
    }

    /**
     * Delete the casetype by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Casetype : {}", id);
        casetypeRepository.deleteById(id);
    }
}
