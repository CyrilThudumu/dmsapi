package com.dms.service.impl;

import com.dms.service.PatientstatusService;
import com.dms.domain.Patientstatus;
import com.dms.repository.PatientstatusRepository;
import com.dms.service.dto.PatientstatusDTO;
import com.dms.service.mapper.PatientstatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Patientstatus.
 */
@Service
@Transactional
public class PatientstatusServiceImpl implements PatientstatusService {

    private final Logger log = LoggerFactory.getLogger(PatientstatusServiceImpl.class);

    private final PatientstatusRepository patientstatusRepository;

    private final PatientstatusMapper patientstatusMapper;

    public PatientstatusServiceImpl(PatientstatusRepository patientstatusRepository, PatientstatusMapper patientstatusMapper) {
        this.patientstatusRepository = patientstatusRepository;
        this.patientstatusMapper = patientstatusMapper;
    }

    /**
     * Save a patientstatus.
     *
     * @param patientstatusDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PatientstatusDTO save(PatientstatusDTO patientstatusDTO) {
        log.debug("Request to save Patientstatus : {}", patientstatusDTO);

        Patientstatus patientstatus = patientstatusMapper.toEntity(patientstatusDTO);
        patientstatus = patientstatusRepository.save(patientstatus);
        return patientstatusMapper.toDto(patientstatus);
    }

    /**
     * Get all the patientstatuses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PatientstatusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Patientstatuses");
        return patientstatusRepository.findAll(pageable)
            .map(patientstatusMapper::toDto);
    }


    /**
     * Get one patientstatus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PatientstatusDTO> findOne(Long id) {
        log.debug("Request to get Patientstatus : {}", id);
        return patientstatusRepository.findById(id)
            .map(patientstatusMapper::toDto);
    }

    /**
     * Delete the patientstatus by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Patientstatus : {}", id);
        patientstatusRepository.deleteById(id);
    }
}
