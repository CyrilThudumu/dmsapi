package com.dms.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dms.domain.Documents;
import com.dms.domain.Patient;
import com.dms.domain.Reports;
import com.dms.repository.PatientRepository;
import com.dms.service.ReportsService;
import com.dms.service.dto.ReportsCriteria;
import com.dms.service.dto.ReportsDTO;
import com.dms.service.mapper.ReportsMapper;

/**
 * Service Implementation for managing Patient.
 */
@Service
@Transactional
public class ReportsServiceImpl implements ReportsService{

	private final Logger log = LoggerFactory.getLogger(ReportsServiceImpl.class);

	private final PatientRepository patientRepository;
	private final ReportsMapper reportsMapper;

	@PersistenceContext
	private EntityManager entityManager;
	 
	public ReportsServiceImpl(PatientRepository patientRepository, EntityManager entityManager,ReportsMapper reportsMapper) {
		this.patientRepository = patientRepository;
		this.entityManager = entityManager;
		this.reportsMapper = reportsMapper;
	}

	
	
	@Override
	public Page<ReportsDTO> findByCriteria(ReportsDTO reportsDTO,
			Pageable pageable) {
		// TODO Auto-generated method stub
		//ReportsDTO  reportsDTO = new ReportsDTO(); 
		return null;
	}


/*	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Page<ReportsDTO> getFinalReport(ReportsCriteria reportsCriteria) {
		// TODO Auto-generated method stub
		    String strPractice=reportsCriteria.getPractice();
		    String strSt=reportsCriteria.getStatus();
		    String strLawyer=reportsCriteria.getAttorney();
		    String fromDate=reportsCriteria.getFromDate();
		    String toDate=reportsCriteria.getToDate();
		    strPractice=(strPractice==null?"":strPractice);
		    strSt=(strSt==null?"":strSt);
		    strLawyer=(strLawyer==null?"":strLawyer);
		    fromDate=(fromDate==null?"":fromDate);
		    toDate=(toDate==null?"":toDate);
		    String temp="s";
		    boolean blnW=false;
		    Page<ReportsDTO> reportsDTOPage =null;
		    Page<Reports> page = null;
		    String strQuery = "select DISTINCT documents.patient_code from Documents documents";
		if(strPractice != null && !"".equals(strPractice) && strPractice.indexOf("null")==-1 && strPractice.length()>0)
		    {
			System.out.println("CAME HERE ...1");
				String strP = strPractice;
		        temp=strP;
		        if(strP.equalsIgnoreCase("AllPractices")){}
		        else
		        	  strQuery+=" where documents.practice_code='"+strP+"' ";
		        blnW=true;
		    }

		    if(strSt!=null && !"".equals(strSt) && strSt.length()>0 && strSt.indexOf("null")==-1)
		    {
		    	System.out.println("CAME HERE ...2");
		        if(strSt.indexOf("All")==-1)
		        {
		            if(!blnW)strQuery+=" where ";
		            else strQuery+=" and ";

		            strQuery+=" documents.status='"+strSt+"'";
		            blnW=true;
		        }
		        else
		        {
		            if(!blnW)strQuery+=" where ";
		            else strQuery+=" and ";
		            strQuery+="documents.status='"+strSt+"'";
		            blnW=true;
		        }
		    }
		    else
		    {}

		    if(strLawyer!=null && strLawyer.length()>0 && strLawyer.indexOf("null")==-1)
		    {
		        if(strLawyer.indexOf("All")==-1)
		        {
		        	System.out.println("CAME HERE ...4");
		            if(!blnW)strQuery+=" where ";
		            else strQuery+=" and ";

		            String strP = strLawyer;
		          //  strQuery+=" documents.attorneyCode='"+strP+"'";
		            strQuery+=" documents.attorney_code='"+strP+"'";
		            blnW=true;
		        }else{
		        	 if(!blnW)strQuery+=" where ";
			            else strQuery+=" and ";

			            String strP = strLawyer;
			         //   strQuery+=" documents.attorneyCode='"+strP+"'";
			            strQuery+=" documents.attorney_code='"+strP+"'";
			            blnW=true;
		        }
		    }


		    if((fromDate!=null && fromDate.length()>0 ) &&
		            (toDate!=null && toDate.length()>0))
		    {
		    	System.out.println("CAME HERE ...5");
		        if(!blnW)strQuery+=" where ";
		        else strQuery+=" and ";

		        strQuery+=" documents.from_dos >='"+fromDate+"'  AND  documents.to_dos <='"+toDate+"'";
		      //  strQuery+=" documents.sentDate >='"+fromDate+"'  AND  documents.sentDate <='"+toDate+"'";from_dos
		        blnW=true;
		    }

		    System.out.println("CAME HERE ...blnW <"+blnW+">");
		    try {
			if( blnW == true ) {
				System.out.println("Trying to execute ... "+strQuery);
				Query q = entityManager.createNativeQuery(strQuery);
				Page<Reports> var = new PageImpl<Reports>(q.getResultList());
				log.debug("entity is: "+var.getContent().get(0).getPatientCode());
				}
			
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return reportsDTOPage;	
		    
	}
	*/
/*	private ReportsDTO convertToReportsDto(final Reports report) {
	    final ReportsDTO reportsDTO = new ReportsDTO();
	    //get values from contact entity and set them in contactDto
	    //e.g. contactDto.setContactId(contact.getContactId());
	    reportsDTO.setPatientCode(report.getPatientCode());
	    reportsDTO.setFirstName(report.getFirstName());
	    reportsDTO.setLastName(report.getLastName());
	    reportsDTO.setStatus(report.getStatus());
	    reportsDTO.setPractice(report.getPractice());
	    reportsDTO.setArbAmount(report.getArbAmount());
	    reportsDTO.setAttorneyName(report.getAttorneyName());
	    reportsDTO.setPaymentAmount(report.getPaymentAmount());
	    reportsDTO.setPaymentDate(report.getPaymentDate());
	    reportsDTO.setSentDate(report.getSentDate());
	    reportsDTO.setArbNotes(report.getArbNotes());
	    reportsDTO.setStatusDate(report.getStatusDate());
	    reportsDTO.setCaseType(report.getCaseType());
	    reportsDTO.setProcessedDate(report.getProcessedDate());
	    reportsDTO.setNatureOfDispute(report.getNatureOfDispute());
	    return reportsDTO;
	}*/
	
}