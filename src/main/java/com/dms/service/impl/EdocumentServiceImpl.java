package com.dms.service.impl;

import com.dms.service.EdocumentService;
import com.dms.domain.Edocument;
import com.dms.repository.EdocumentRepository;
import com.dms.service.dto.EdocumentDTO;
import com.dms.service.mapper.EdocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Edocument.
 */
@Service
@Transactional
public class EdocumentServiceImpl implements EdocumentService {

    private final Logger log = LoggerFactory.getLogger(EdocumentServiceImpl.class);

    private final EdocumentRepository edocumentRepository;

    private final EdocumentMapper edocumentMapper;

    public EdocumentServiceImpl(EdocumentRepository edocumentRepository, EdocumentMapper edocumentMapper) {
        this.edocumentRepository = edocumentRepository;
        this.edocumentMapper = edocumentMapper;
    }

    /**
     * Save a edocument.
     *
     * @param edocumentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EdocumentDTO save(EdocumentDTO edocumentDTO) {
        log.debug("Request to save Edocument : {}", edocumentDTO);

        Edocument edocument = edocumentMapper.toEntity(edocumentDTO);
        edocument = edocumentRepository.save(edocument);
        return edocumentMapper.toDto(edocument);
    }

    /**
     * Get all the edocuments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EdocumentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Edocuments");
        return edocumentRepository.findAll(pageable)
            .map(edocumentMapper::toDto);
    }


    /**
     * Get one edocument by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EdocumentDTO> findOne(Long id) {
        log.debug("Request to get Edocument : {}", id);
        return edocumentRepository.findById(id)
            .map(edocumentMapper::toDto);
    }

    /**
     * Delete the edocument by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Edocument : {}", id);
        edocumentRepository.deleteById(id);
    }
}
