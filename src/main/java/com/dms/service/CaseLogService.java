package com.dms.service;


import com.dms.domain.Caselog;
import com.dms.service.dto.CaselogDTO;

public interface CaseLogService {
	
	 CaselogDTO save(CaselogDTO caselogDTO);

}
