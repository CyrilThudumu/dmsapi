package com.dms.service.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.dms.domain.Documents;
import com.dms.service.dto.DocumentsDTO;

/**
 * Mapper for the entity documents and its DTO documentDTO.
 */
@Mapper(componentModel = "spring")
public interface DocumentsMapper extends EntityMapper<DocumentsDTO, Documents> {

   
    DocumentsDTO toDto(Documents documents);

    
    Documents toEntity(DocumentsDTO mapDocumentsDTO);

    default Documents fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documents documents = new Documents();
        documents.setId(id);
        return documents;
    }
    
    /**
     * Transforms {@code Page<ENTITY>} objects into {@code Page<DTO>} objects.
     * @param pageRequest   The information of the requested page.
     * @param source        The {@code Page<ENTITY>} object.
     * @return The created {@code Page<DTO>} object.
     */
 /*   Page<DocumentsDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<Documents> source) {
        List<DocumentsDTO> dtos = toDto(source.getContent());
        return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
    }
    */
    
    /**
     * Transforms the list of {@link Todo} objects given as a method parameter
     * into a list of {@link TodoDTO} objects and returns the created list.
     *
     * @param entities
     * @return
     */
 /*   static List<DocumentsDTO> mapEntitiesIntoDTOs(Iterable<Documents> entities) {
        List<DocumentsDTO> dtos = new ArrayList<>();

        entities.forEach(e -> dtos.add(mapEntityIntoDTO(e)));

        return dtos;
    }*/


	/**
     * Transforms the {@link Todo} object given as a method parameter into a
     * {@link TodoDTO} object and returns the created object.
     *
     * @param entity
     * @return
     */
  /*  static DocumentsDTO mapEntityIntoDTO(Documents entity) {
    	DocumentsDTO dto = new DocumentsDTO();

    	dto.setDocumentName(entity.getDocumentName());
    	dto.setFromDos(entity.getFromDos());
    	dto.setToDos(entity.getToDos());
    	dto.setPatientCode(entity.getPatientCode());
    	dto.setDocumentTypeId(entity.getDocumentTypeId());
    	dto.setPracticeCode(entity.getPracticeCode());
    	dto.setAttorneyCode(entity.getAttorneyCode());
    	dto.setDoctorCode(entity.getDoctorCode());
    	dto.setFilePath(entity.getFilePath());
        return dto;
    }*/
    
    
}
