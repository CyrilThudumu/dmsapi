package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.PatientDTO;
import com.dms.service.dto.ReportsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reports and its DTO ReportsDTO.
 */
@Mapper(componentModel = "spring", uses = {PracticeMapper.class, EdocumentMapper.class})
public interface ReportsMapper extends EntityMapper<ReportsDTO, Reports> {


	ReportsDTO toDto(Reports reports);

	Reports toEntity(ReportsDTO reportsDTO);

    default Reports fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reports reports = new Reports();
        reports.setId(id);
        return reports;
    }
}
