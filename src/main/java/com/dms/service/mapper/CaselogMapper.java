package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.CaselogDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Caselog and its DTO caselogDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CaselogMapper extends EntityMapper<CaselogDTO, Caselog> {



    default Caselog fromId(Long id) {
        if (id == null) {
            return null;
        }
        Caselog caselog = new Caselog();
        caselog.setId(id);
        return caselog;
    }
}
