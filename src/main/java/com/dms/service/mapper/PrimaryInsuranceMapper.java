package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.PrimaryInsuranceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Casetype and its DTO CasetypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PrimaryInsuranceMapper extends EntityMapper<PrimaryInsuranceDTO, PrimaryInsurance> {

    default PrimaryInsurance fromId(Long id) {
        if (id == null) {
            return null;
        }
        PrimaryInsurance primaryIns = new PrimaryInsurance();
        primaryIns.setId(id);
        return primaryIns;
    }
}
