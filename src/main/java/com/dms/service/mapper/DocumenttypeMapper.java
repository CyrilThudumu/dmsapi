package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.DocumenttypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Documenttype and its DTO DocumenttypeDTO.
 */
@Mapper(componentModel = "spring", uses = {PracticeMapper.class})
public interface DocumenttypeMapper extends EntityMapper<DocumenttypeDTO, Documenttype> {

    @Mapping(source = "practice.id", target = "practiceId")
    @Mapping(source = "practice.practiceCode", target = "practicePracticeCode")
    DocumenttypeDTO toDto(Documenttype documenttype);

    @Mapping(source = "practiceId", target = "practice")
    Documenttype toEntity(DocumenttypeDTO documenttypeDTO);

    default Documenttype fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documenttype documenttype = new Documenttype();
        documenttype.setId(id);
        return documenttype;
    }
}
