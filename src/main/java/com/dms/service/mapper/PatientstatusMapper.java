package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.PatientstatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Patientstatus and its DTO PatientstatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PatientstatusMapper extends EntityMapper<PatientstatusDTO, Patientstatus> {



    default Patientstatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        Patientstatus patientstatus = new Patientstatus();
        patientstatus.setId(id);
        return patientstatus;
    }
}
