package com.dms.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.Patientstatus;
import com.dms.domain.*; // for static metamodels
import com.dms.repository.PatientstatusRepository;
import com.dms.service.dto.PatientstatusCriteria;
import com.dms.service.dto.PatientstatusDTO;
import com.dms.service.mapper.PatientstatusMapper;

/**
 * Service for executing complex queries for Patientstatus entities in the database.
 * The main input is a {@link PatientstatusCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PatientstatusDTO} or a {@link Page} of {@link PatientstatusDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PatientstatusQueryService extends QueryService<Patientstatus> {

    private final Logger log = LoggerFactory.getLogger(PatientstatusQueryService.class);

    private final PatientstatusRepository patientstatusRepository;

    private final PatientstatusMapper patientstatusMapper;

    public PatientstatusQueryService(PatientstatusRepository patientstatusRepository, PatientstatusMapper patientstatusMapper) {
        this.patientstatusRepository = patientstatusRepository;
        this.patientstatusMapper = patientstatusMapper;
    }

    /**
     * Return a {@link List} of {@link PatientstatusDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PatientstatusDTO> findByCriteria(PatientstatusCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Patientstatus> specification = createSpecification(criteria);
        return patientstatusMapper.toDto(patientstatusRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PatientstatusDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PatientstatusDTO> findByCriteria(PatientstatusCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Patientstatus> specification = createSpecification(criteria);
        return patientstatusRepository.findAll(specification, page)
            .map(patientstatusMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PatientstatusCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Patientstatus> specification = createSpecification(criteria);
        return patientstatusRepository.count(specification);
    }

    /**
     * Function to convert PatientstatusCriteria to a {@link Specification}
     */
    private Specification<Patientstatus> createSpecification(PatientstatusCriteria criteria) {
        Specification<Patientstatus> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Patientstatus_.id));
            }
            if (criteria.getPatientStatusCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPatientStatusCode(), Patientstatus_.patientStatusCode));
            }
            if (criteria.getPatientStatusName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPatientStatusName(), Patientstatus_.patientStatusName));
            }
        }
        return specification;
    }
}
