package com.dms.service;

import com.dms.service.dto.PracticeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Practice.
 */
public interface PracticeService {

    /**
     * Save a practice.
     *
     * @param practiceDTO the entity to save
     * @return the persisted entity
     */
    PracticeDTO save(PracticeDTO practiceDTO);

    /**
     * Get all the practices.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PracticeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" practice.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PracticeDTO> findOne(Long id);
    
    Optional<PracticeDTO> findOne(String id);
    
    /**
     * Get the "practiceName" practice.
     *
     * @param practiceName the practiceName of the entity
     * @return the entity
     */
   Page<PracticeDTO> findByPracticeName(String practiceName, Pageable page);
    
    /**
     * Get the details of the practice.
     *
     * @param practiceName the practiceName of the entity
     * @return the entity
     */


    /**
     * Delete the "id" practice.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
