package com.dms.service.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import com.dms.domain.Documents;
import com.dms.repository.DocumentsRepository;
import com.dms.service.DocumentsService;
import com.dms.service.dto.DocumentsChildDTO;
import com.dms.service.dto.DocumentsDTO;

public class DocumentsUtil {
	 private final DocumentsService documentsService;
	 public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	 public DocumentsUtil(DocumentsService documentsService) {
	        this.documentsService = documentsService;
	    }
	 private final Logger log = LoggerFactory.getLogger(DocumentsUtil.class);
	 
	 public ArrayList<String> splitToMultiplePDF(String fileName,
				byte[] content, ArrayList<String> fromToPages,
				ArrayList<String> documentTypes, ArrayList<String> fromDos,
				ArrayList<String> toDOS, DocumentsChildDTO documentsChildDTO)
				throws IOException {
			ArrayList<String> urls = new ArrayList<String>();
			Path practiceDir;
			Path patientDir;
			Path dosFromDir;
			Path dosToDir;
			Path docTypeDir;
			Path path;
			LocalDate today = LocalDate.now();
			Logger log = LoggerFactory.getLogger(DocumentsUtil.class);
			 log.debug("IN UTIL: orig file name:  "+fileName);
        	// log.debug("IN UTIL: get bytes:  "+content);
        	 log.debug("IN UTIL: pages nums: "+fromToPages);
        	 log.debug("IN UTIL: document types: "+documentTypes);
        	 log.debug("IN UTIL: from DOS:  "+fromDos);
        	 log.debug("IN UTIL: toDOS:  "+toDOS);
        	 log.debug("IN UTIL: Complete documents object:  "+documentsChildDTO);
			
			log.debug("Came here into method splitToMultiplePDF:"+ fileName + ":" + fromToPages);
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
			try (InputStream inputStream = new ByteArrayInputStream(content);
					PDDocument pdfDocument = PDDocument.load(inputStream)) {
				log.debug("In First For loop....");
				int mainIndex = 0;
				for (String outerString : fromToPages) {
					log.debug("In Second for loop...");
					PDDocument outputPDFDocument = new PDDocument();
					log.debug("OuterString is: ..." + outerString);
					String pageArray[] = outerString.split(",");
					log.debug("pagesArray is:   " + pageArray.length);
					for (String innerString : pageArray) {
						log.debug("Page Item IS: "+innerString);
						String pageItem = pageArray[0];
						log.debug("page Item is:  " + pageItem);
						String[] pageSubArray = innerString.split(",");
						log.debug("PageSubArray LENGTH is:  " + pageSubArray.length);
						//log.debug("pageSubArray contents are : "+Arrays.toString(pageSubArray));
						for( int x = 0 ; x < pageSubArray.length ; x++ ) {
							log.debug("X value is: "+x);
							int index=pageSubArray[x].indexOf("*");
							log.debug("pageItem value is: "+pageItem);
							String pageno=pageSubArray[x].substring(0, index);
							//log.debug("Adding Page ["+(Integer.parseInt(pageSubArray[x])-1)+"] to DocType..");
							int rotation=Integer.parseInt(pageSubArray[x].substring(index+1));
							pdfDocument.getPage(Integer.parseInt(pageno)-1).setRotation(rotation);
							outputPDFDocument.addPage(pdfDocument.getPage(Integer.parseInt(pageno)-1));
							//outputPDFDocument.addPage(pdfDocument.getPages().get(Integer.parseInt(pageSubArray[x])));
							//outputPDFDocument.addPage(pdfDocument.getPage(Integer.parseInt(pageSubArray[x])-1));
						}
					}
					practiceDir = Paths.get(documentsChildDTO.getPracticeCode());
					patientDir = Paths.get(documentsChildDTO.getPatientCode());
					dosFromDir = Paths.get(documentsChildDTO.getDosFrom().get(mainIndex));
					dosToDir = Paths.get(documentsChildDTO.getDosTo().get(mainIndex));
					docTypeDir = Paths.get(documentsChildDTO.getDocumentTypes().get(mainIndex));
					path = Paths.get("C:\\ScanDoc\\" + practiceDir + "\\" + patientDir
							+ "\\" + dosFromDir + "-" + dosToDir + "\\"
							+ docTypeDir + "\\");
					try {
						Files.createDirectories(path);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					fileName = documentsChildDTO.getPatientCode()+"-"+documentsChildDTO.getDosFrom().get(mainIndex)+"_"+documentsChildDTO.getDosTo().get(mainIndex)+"-"+documentsChildDTO.getDocumentTypes().get(mainIndex)+"_"+today;
					log.debug("File Name before appending to Tempfile... "
							+ fileName);
					java.io.File tempFile = new java.io.File(path + "/" + fileName+"_"+mainIndex+ ".pdf");
					log.info("temp file is: "+tempFile);
					documentsChildDTO.setFilePath(tempFile.toString());
					outputPDFDocument.save(tempFile);
					outputPDFDocument.close();
					log.debug("TEMP file is :"
							+ tempFile.getAbsoluteFile());

					 urls.add(tempFile.getAbsolutePath());
					// log.debug("*****SAVED Successfully "+urls);
					log.info("Main index is: " + mainIndex);
					DocumentsDTO documentsDTO = new DocumentsDTO();
					log.info("doc types: "+documentsChildDTO.getDocumentTypes());
					documentsDTO.setDocumentName(fileName+".pdf".toString());
					log.info("fromdos: "+documentsChildDTO.getDosFrom());
					documentsDTO.setFromDos(documentsChildDTO.getDosFrom().get(mainIndex));
					log.info("toDos   "+documentsChildDTO.getDosTo());
					documentsDTO.setToDos(documentsChildDTO.getDosTo().get(mainIndex));
					log.info("patientCode:  "+documentsChildDTO.getPatientCode());
					
					documentsDTO.setPatientCode(documentsChildDTO.getPatientCode());
					log.info("Practice Code: "+documentsChildDTO.getPracticeCode());
					documentsDTO.setPracticeCode(documentsChildDTO.getPracticeCode());
					//documentsDTO.setAttorneyCode((documentsChildDTO.getAttorneyCode()));
					documentsDTO.setDocumentTypes(documentsChildDTO.getDocumentTypes().get(mainIndex));
					documentsDTO.setDoctorCode(documentsChildDTO.getDoctorCode());
					log.info("filepath:  "+documentsChildDTO.getFilePath());
					documentsDTO.setFilePath(documentsChildDTO.getFilePath());
					//set attorney code
					DocumentsDTO docWithAttorneyExist=documentsService.getAttorneyDos(documentsDTO);
					log.info("docWithAttorneyExist:  "+docWithAttorneyExist);
					if( docWithAttorneyExist!= null) {
					documentsDTO.setArbAmount(docWithAttorneyExist.getArbAmount());
					documentsDTO.setArbNotes(docWithAttorneyExist.getArbNotes());
					documentsDTO.setAttorneyCode(docWithAttorneyExist.getAttorneyCode());
					documentsDTO.setNatureOfDispute(docWithAttorneyExist.getNatureOfDispute());
					documentsDTO.setPaymentAmount(docWithAttorneyExist.getPaymentAmount());
					documentsDTO.setPaymentDate(docWithAttorneyExist.getPaymentDate());
					documentsDTO.setProcessedDate(docWithAttorneyExist.getProcessedDate());
					documentsDTO.setSentDate(docWithAttorneyExist.getSentDate());
					documentsDTO.setStatusDate(docWithAttorneyExist.getStatusDate());
					documentsDTO.setStatus(docWithAttorneyExist.getStatus());
					documentsDTO.setProcessedFlag(docWithAttorneyExist.isProcessedFlag());
					documentsDTO.setAddendumToPriorFlag(docWithAttorneyExist.isAddendumToPriorFlag());
					
					/*String documentname=docWithAttorneyExist.getDocumentName();
					String firstElementInString=documentname.split("\\.")[0];
					String createdDate=firstElementInString.substring(firstElementInString.length()-10);
		             SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
					 Date d1 = sdfo.parse(today.toString()); 
				     Date d2 = sdfo.parse(createdDate); 
				     log.info("d1 is  "+d1 );
				     log.info("d2 is  "+d2);
				     if(d1.after(d2)) {
				    	 System.out.println("d1 is greater");
				     }
					if(docWithAttorneyExist.isProcessedFlag()==false && d1.after(d2)) {
						documentsDTO.setAddendumToPriorFlag(true);
						System.out.println(documentsDTO.isAddendumToPriorFlag());
					}*/
					}
					documentsService.save(documentsDTO);
					mainIndex++;
					log.debug("Main index is: " + mainIndex);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
			}
			return urls;
		}
	 
// with pagination 
	 public Page <DocumentsDTO> getDOSFileContent(Page <DocumentsDTO> documentsObj){
		 Page <DocumentsDTO> documentDTO = documentsObj;
		   for(int i = 0; i < documentsObj.getContent().size(); i++){
	        	//log.info(documentsObj.getContent().get(i).getFilePath());
	        	File fileObje= new File(documentsObj.getContent().get(i).getFilePath());
	   	     try {
				byte[] fileContent = Files.readAllBytes(fileObje.toPath());
				documentsObj.getContent().get(i).setFileContent(fileContent);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   	     log.info("Index is: "+i);
	   	     log.info("Size is: "+documentsObj.getContent().size());
	        }
		  // System.out.println("documentDto from docUtil "+documentDTO.getContent());
		 return documentDTO;
		 
	 }
	 
	 
	 //without pagination
	 public List <DocumentsDTO> getDOSFileContents(List <DocumentsDTO> documentsObj){
		// Page <DocumentsDTO> documentDTO = documentsObj;
		   for(int i = 0; i < documentsObj.size(); i++){
	        	//log.info(documentsObj.getContent().get(i).getFilePath());
	        	File fileObje= new File(documentsObj.get(i).getFilePath());
	   	     try {
				byte[] fileContent = Files.readAllBytes(fileObje.toPath());
				documentsObj.get(i).setFileContent(fileContent);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   	     log.info("Index is: "+i);
	   	     log.info("Size is: "+documentsObj.size());
	        }
		  // System.out.println("documentDto from docUtil "+documentDTO.getContent());
		 return documentsObj;
		 
	 }
	
	 public byte[] zipPatientDocs(File directory, String[] files) throws IOException {
		
		/* ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
	        ZipOutputStream zipOutputStream = new ZipOutputStream(byteOutputStream);

	        for(String filename: files) {
	            File file = new File(filename); 
	            zipOutputStream.putNextEntry(new ZipEntry(filename));           
	            FileInputStream fileInputStream = new FileInputStream(file);
	            IOUtils.copy(fileInputStream, zipOutputStream);
	            fileInputStream.close();
	            zipOutputStream.closeEntry();
	        }           
	        zipOutputStream.close();
	        return byteOutputStream.toByteArray();
	        
	        */
		 
		 ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ZipOutputStream zos = new ZipOutputStream(baos);
	        byte bytes[] = new byte[8192];
	        int arraySize=files.length;
	        for(int i=0; i<arraySize;i++){
	        	// log.debug("value of i:  "+i+"   array Size:  "+arraySize);
	        	// log.debug("FIS content is:  "+directory.getPath() + DocumentsUtil.FILE_SEPARATOR+files[i]);
	        	 File f = new File(directory.getPath() + DocumentsUtil.FILE_SEPARATOR+files[i]);
	        	 if(f.isDirectory()){
	        		 File[] listOfFiles = f.listFiles();
	        		 for(File finalFile : listOfFiles) {
	        			 FileInputStream fis = new FileInputStream(finalFile);
	        			// log.debug("filename is: "+finalFile.getAbsolutePath());
	        			 BufferedInputStream bis = new BufferedInputStream(fis);
	    	        	// log.debug("file of i is: "+files[i]);
	    	        	  zos.putNextEntry(new ZipEntry(files[i]));
	    	        	  int bytesRead;
	    	              while ((bytesRead = bis.read(bytes)) != -1) {
	    	                  log.debug("in while loop....");
	    	            	  zos.write(bytes, 0, bytesRead);
	    	              }
	    	              log.debug("exit from while...");
	    	              zos.closeEntry();
	    	              bis.close();
	    	              fis.close();
	        		 }
	        	 }/*else if(f.isFile()){
	        		 
	        	 }*/
	        	/*FileInputStream fis = new FileInputStream(directory.getPath() + DocumentsUtil.FILE_SEPARATOR + files[i]);
	        	 BufferedInputStream bis = new BufferedInputStream(fis);
	        	 log.debug("file of i is: "+files[i]);
	        	  zos.putNextEntry(new ZipEntry(files[i]));
	        	  int bytesRead;
	              while ((bytesRead = bis.read(bytes)) != -1) {
	                  log.debug("in while loop....");
	            	  zos.write(bytes, 0, bytesRead);
	              }
	              log.debug("exit from while...");
	              zos.closeEntry();
	              bis.close();
	              fis.close();*/
	        	
	        }
	        zos.flush();
	        baos.flush();
	        zos.close();
	        baos.close();
	        log.info("baos : "+baos.toByteArray());
	        return baos.toByteArray();
	    }

	public void deleteDocumentType(String path) {
		// TODO Auto-generated method stub
		log.debug("path: "+path);
		File fileToDelete = new File(path);
		log.debug("File : "+fileToDelete);
	    boolean flag = fileToDelete.delete();
	    log.debug("boolean value is: "+flag);
		
	}
	 
}
