package com.dms.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.Practice;
import com.dms.domain.*; // for static metamodels
import com.dms.repository.PracticeRepository;
import com.dms.service.dto.PracticeCriteria;
import com.dms.service.dto.PracticeDTO;
import com.dms.service.mapper.PracticeMapper;

/**
 * Service for executing complex queries for Practice entities in the database.
 * The main input is a {@link PracticeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PracticeDTO} or a {@link Page} of {@link PracticeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PracticeQueryService extends QueryService<Practice> {

    private final Logger log = LoggerFactory.getLogger(PracticeQueryService.class);

    private final PracticeRepository practiceRepository;

    private final PracticeMapper practiceMapper;

    public PracticeQueryService(PracticeRepository practiceRepository, PracticeMapper practiceMapper) {
        this.practiceRepository = practiceRepository;
        this.practiceMapper = practiceMapper;
    }

    /**
     * Return a {@link List} of {@link PracticeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PracticeDTO> findByCriteria(PracticeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Practice> specification = createSpecification(criteria);
        return practiceMapper.toDto(practiceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PracticeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PracticeDTO> findByCriteria(PracticeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Practice> specification = createSpecification(criteria);
      // Pageable pageable = PageRequest.of(0, 10,Sort.by("practiceName"));
      //  Pageable pageable = PageRequest.of(0, 100,Sort.by("practiceName"));
        Pageable pageable = PageRequest.of(0, 5000,Sort.by("practiceName"));
        return practiceRepository.findAll(specification, pageable)
            .map(practiceMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PracticeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Practice> specification = createSpecification(criteria);
        return practiceRepository.count(specification);
    }

    /**
     * Function to convert PracticeCriteria to a {@link Specification}
     */
    private Specification<Practice> createSpecification(PracticeCriteria criteria) {
        Specification<Practice> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Practice_.id));
            }
            if (criteria.getPracticeCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPracticeCode(), Practice_.practiceCode));
            }
            if (criteria.getPracticeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPracticeName(), Practice_.practiceName));
            }
            if (criteria.getAddress1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress1(), Practice_.address1));
            }
            if (criteria.getAddress2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress2(), Practice_.address2));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), Practice_.city));
            }
            if (criteria.getStateProvince() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateProvince(), Practice_.stateProvince));
            }
//            if (criteria.getZipCode() != null) {
//                specification = specification.and(buildRangeSpecification(criteria.getZipCode(), Practice_.zipCode));
//            }
         //  if (criteria.getTinNumber() != null) {
         //       specification = specification.and(buildRangeSpecification(criteria.getTinNumber(), Practice_.tinNumber));
         //   }
            
            if (criteria.getZipCode() != null) {
               specification = specification.and(buildStringSpecification(criteria.getZipCode(), Practice_.zipCode));
              }
            if (criteria.getTinNumber() != null) {
              specification = specification.and(buildStringSpecification(criteria.getTinNumber(), Practice_.tinNumber));
            }
            if (criteria.getDocumenttypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeId(),
                    root -> root.join(Practice_.documenttypes, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Practice_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
