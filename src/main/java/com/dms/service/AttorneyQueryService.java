package com.dms.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.Attorney;
import com.dms.domain.*; // for static metamodels
import com.dms.repository.AttorneyRepository;
import com.dms.service.dto.AttorneyCriteria;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.mapper.AttorneyMapper;

/**
 * Service for executing complex queries for Attorney entities in the database.
 * The main input is a {@link AttorneyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AttorneyDTO} or a {@link Page} of {@link AttorneyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AttorneyQueryService extends QueryService<Attorney> {

    private final Logger log = LoggerFactory.getLogger(AttorneyQueryService.class);

    private final AttorneyRepository attorneyRepository;

    private final AttorneyMapper attorneyMapper;

    public AttorneyQueryService(AttorneyRepository attorneyRepository, AttorneyMapper attorneyMapper) {
        this.attorneyRepository = attorneyRepository;
        this.attorneyMapper = attorneyMapper;
    }

    /**
     * Return a {@link List} of {@link AttorneyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AttorneyDTO> findByCriteria(AttorneyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Attorney> specification = createSpecification(criteria);
        return attorneyMapper.toDto(attorneyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AttorneyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AttorneyDTO> findByCriteria(AttorneyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Attorney> specification = createSpecification(criteria);
        return attorneyRepository.findAll(specification, page)
            .map(attorneyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AttorneyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Attorney> specification = createSpecification(criteria);
        return attorneyRepository.count(specification);
    }

    /**
     * Function to convert AttorneyCriteria to a {@link Specification}
     */
    private Specification<Attorney> createSpecification(AttorneyCriteria criteria) {
        Specification<Attorney> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Attorney_.id));
            }
            if (criteria.getAttornyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttornyCode(), Attorney_.attornyCode));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), Attorney_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), Attorney_.lastName));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), Attorney_.phone));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), Attorney_.fax));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Attorney_.address));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), Attorney_.city));
            }
            if (criteria.getStateProvince() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateProvince(), Attorney_.stateProvince));
            }
            if (criteria.getZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getZipCode(), Attorney_.zipCode));
            }
        }
        return specification;
    }
}
