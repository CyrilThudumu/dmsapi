package com.dms.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.Documenttype;
import com.dms.domain.*; // for static metamodels
import com.dms.repository.DocumenttypeRepository;
import com.dms.service.dto.DocumenttypeCriteria;
import com.dms.service.dto.DocumenttypeDTO;
import com.dms.service.mapper.DocumenttypeMapper;

/**
 * Service for executing complex queries for Documenttype entities in the database.
 * The main input is a {@link DocumenttypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DocumenttypeDTO} or a {@link Page} of {@link DocumenttypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DocumenttypeQueryService extends QueryService<Documenttype> {

    private final Logger log = LoggerFactory.getLogger(DocumenttypeQueryService.class);

    private final DocumenttypeRepository documenttypeRepository;

    private final DocumenttypeMapper documenttypeMapper;

    public DocumenttypeQueryService(DocumenttypeRepository documenttypeRepository, DocumenttypeMapper documenttypeMapper) {
        this.documenttypeRepository = documenttypeRepository;
        this.documenttypeMapper = documenttypeMapper;
    }

    /**
     * Return a {@link List} of {@link DocumenttypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DocumenttypeDTO> findByCriteria(DocumenttypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Documenttype> specification = createSpecification(criteria);
        return documenttypeMapper.toDto(documenttypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DocumenttypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DocumenttypeDTO> findByCriteria(DocumenttypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Documenttype> specification = createSpecification(criteria);
        return documenttypeRepository.findAll(specification, page)
            .map(documenttypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DocumenttypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Documenttype> specification = createSpecification(criteria);
        return documenttypeRepository.count(specification);
    }

    /**
     * Function to convert DocumenttypeCriteria to a {@link Specification}
     */
    private Specification<Documenttype> createSpecification(DocumenttypeCriteria criteria) {
        Specification<Documenttype> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Documenttype_.id));
            }
            if (criteria.getDocumentTypeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDocumentTypeName(), Documenttype_.documentTypeName));
            }
            if (criteria.getDocumentTypeCategory() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDocumentTypeCategory(), Documenttype_.documentTypeCategory));
            }
            if (criteria.getPracticeId() != null) {
                specification = specification.and(buildSpecification(criteria.getPracticeId(),
                    root -> root.join(Documenttype_.practice, JoinType.LEFT).get(Practice_.id)));
            }
        }
        return specification;
    }
}
