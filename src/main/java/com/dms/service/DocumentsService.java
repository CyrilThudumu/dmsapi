package com.dms.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.dms.domain.Documents;
import com.dms.service.dto.CaselogDTO;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientDTO;

/**
 * Service Interface for managing Documents.
 */
public interface DocumentsService {

	DocumentsDTO getAttorneyDos(DocumentsDTO documentsDTO);
	Documents previousAssignedAttorneyDates(DocumentsDTO documentsDTO);
	 DocumentsDTO save(DocumentsDTO documentsDTO);
	 List<String> findDocumentsByAttorneyCode(String attorneyCode);
	// List<Documents> findDocumentsByAttorneyCode(String attorneyCode);
	 List<String> findPatientCodeByAttorneyCode(String attorneyCode);
	// List<Documents> findDocumentsByAttorneyCode(String attorneyCode);
	// CaselogDTO savecaselog(CaselogDTO caselogDTO);
	 Page<DocumentsDTO> findByDoctorCode(String doctorCode, String patientCode, Pageable page);
	 Page<DocumentsDTO> findByPatientCode(String patientCode, String practiceCode,Pageable page);
	 Page<DocumentsDTO> findByAttorneyCode(String attorneyCode, String patientCode, Pageable page);
	 //Page<DocumentsDTO> getAttorneyNotProcessedDOS(String attorneyCode, String patientCode, Pageable page);
	 Page<DocumentsDTO> getAttorneyNotProcessedDOS(String attorneyCode, String patientCode, String status, Pageable page);
	 
	// Page<DocumentsDTO> getAttorneyProcessedDOS(String attorneyCode, String patientCode, Pageable page);
	   Page<DocumentsDTO> getAttorneyProcessedDOS(String attorneyCode, String patientCode, String status, Pageable page);
	 
	// Page<DocumentsDTO> findAttorneysAddendumToPriorDOS(String attorneyCode, String patientCode, Pageable page);
	   Page<DocumentsDTO> findAttorneysAddendumToPriorDOS(String attorneyCode, String patientCode, String status, Pageable page);
	                      
	 Page<DocumentsDTO> findAttorneySearchDos(String attorneyCode, String patientCode, Pageable page);
	 void updatePatientDOS(DocumentsDTO documentsDTO);
	 void updatePatientDOSToProcessed(DocumentsDTO documentsDTO);
	 Page<DocumentsDTO> downloadPatientDocs(String patientCode, String practiceCode, String fromDOS, String toDOS, Pageable page);
	 Page<DocumentsDTO> findDocsHistoryByAttorneyCode(String attorneyCode, String patientCode, Pageable page);
	 Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode,String fromDOS, String toDOS,Pageable page);
	 Page<DocumentsDTO> getDocumentsForHistorySearch(String attorneyCode,String processedFrom, String processedTo, Pageable page);
	 void delete(Long id);
	 void updateCaseIdForDOS(DocumentsDTO documentsDTO);
	 Page<DocumentsDTO> getReportsBySearch( String practiceCode, String status, String attorneyCode, String fromDate, String toDate, Pageable page);
}
