package com.dms.service;

import com.dms.service.dto.AttorneyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Attorney.
 */
public interface AttorneyService {

    /**
     * Save a attorney.
     *
     * @param attorneyDTO the entity to save
     * @return the persisted entity
     */
    AttorneyDTO save(AttorneyDTO attorneyDTO);

    /**
     * Get all the attorneys.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AttorneyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" attorney.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AttorneyDTO> findOne(Long id);

    /**
     * Delete the "id" attorney.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
