package com.dms.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the Documents entity. This class is used in DocumentsResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /documents?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DocumentsCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;
    private StringFilter documentType;
    private StringFilter documentName;
    private StringFilter fromDos;
    private StringFilter toDos;
    private StringFilter practiceCode;
    private StringFilter patientCode;
    private StringFilter doctorCode;
	private StringFilter attorneyCode;
	private StringFilter filePath;
	private StringFilter arbAmount;
	private StringFilter arbNotes;
	private LocalDateFilter sentDate;
	private LocalDateFilter paymentDate;
	private StringFilter paymentAmount;
	private StringFilter natureOfDispute;
	private LocalDateFilter processedDate;

	public LongFilter getId() {
		return id;
	}



	public void setId(LongFilter id) {
		this.id = id;
	}



	public StringFilter getDocumentType() {
		return documentType;
	}



	public void setDocumentType(StringFilter documentType) {
		this.documentType = documentType;
	}



	public StringFilter getDocumentName() {
		return documentName;
	}



	public void setDocumentName(StringFilter documentName) {
		this.documentName = documentName;
	}



	public StringFilter getFromDos() {
		return fromDos;
	}



	public void setFromDos(StringFilter fromDos) {
		this.fromDos = fromDos;
	}



	public StringFilter getToDos() {
		return toDos;
	}



	public void setToDos(StringFilter toDos) {
		this.toDos = toDos;
	}



	public StringFilter getPracticeCode() {
		return practiceCode;
	}



	public void setPracticeCode(StringFilter practiceCode) {
		this.practiceCode = practiceCode;
	}



	public StringFilter getPatientCode() {
		return patientCode;
	}



	public void setPatientCode(StringFilter patientCode) {
		this.patientCode = patientCode;
	}



	public StringFilter getDoctorCode() {
		return doctorCode;
	}



	public void setDoctorCode(StringFilter doctorCode) {
		this.doctorCode = doctorCode;
	}



	public StringFilter getAttorneyCode() {
		return attorneyCode;
	}



	public void setAttorneyCode(StringFilter attorneyCode) {
		this.attorneyCode = attorneyCode;
	}



	public StringFilter getFilePath() {
		return filePath;
	}



	public void setFilePath(StringFilter filePath) {
		this.filePath = filePath;
	}

	public StringFilter getArbAmount() {
		return arbAmount;
	}

	public void setArbAmount(StringFilter arbAmount) {
		this.arbAmount = arbAmount;
	}

	public StringFilter getArbNotes() {
		return arbNotes;
	}

	public void setArbNotes(StringFilter arbNotes) {
		this.arbNotes = arbNotes;
	}

	public StringFilter getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(StringFilter paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public StringFilter getNatureOfDispute() {
		return natureOfDispute;
	}

	public void setNatureOfDispute(StringFilter natureOfDispute) {
		this.natureOfDispute = natureOfDispute;
	}
		
 
	public LocalDateFilter getSentDate() {
		return sentDate;
	}

	public void setSentDate(LocalDateFilter sentDate) {
		this.sentDate = sentDate;
	}

	public LocalDateFilter getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDateFilter paymentDate) {
		this.paymentDate = paymentDate;
	}

	public LocalDateFilter getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(LocalDateFilter processedDate) {
		this.processedDate = processedDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final DocumentsCriteria that = (DocumentsCriteria) o;
		return Objects.equals(id, that.id) && Objects.equals(documentType, that.documentType)
				&& Objects.equals(documentName, that.documentName) && Objects.equals(fromDos, that.fromDos)
				&& Objects.equals(toDos, that.toDos) && Objects.equals(practiceCode, that.practiceCode)
				&& Objects.equals(patientCode, that.patientCode) && Objects.equals(doctorCode, that.doctorCode)
				&& Objects.equals(attorneyCode, that.attorneyCode) && Objects.equals(filePath, that.filePath)
				&& Objects.equals(arbAmount, that.arbAmount) && Objects.equals(arbNotes, that.arbNotes)
				&& Objects.equals(sentDate, that.sentDate) && Objects.equals(paymentAmount, that.paymentAmount)
				&& Objects.equals(paymentDate, that.paymentDate)
				&& Objects.equals(natureOfDispute, that.natureOfDispute)
				&& Objects.equals(processedDate, that.processedDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, documentType, fromDos, toDos, practiceCode, patientCode, doctorCode, attorneyCode,
				filePath, arbAmount, arbNotes, sentDate, paymentDate, paymentAmount, natureOfDispute, processedDate);
	}

	@Override
	public String toString() {
		return "DocumentsCriteria [" + "id=" + id + ", documentType=" + documentType + ", documentName=" + documentName
				+ ", fromDos=" + fromDos + ", toDos=" + toDos + ", practiceCode=" + practiceCode + ", patientCode="
				+ patientCode + ", doctorCode=" + doctorCode + ", attorneyCode=" + attorneyCode + ", filePath="
				+ filePath + ", arbAmount=" + arbAmount + ", arbNotes=" + arbNotes + ", sentDate=" + sentDate
				+ ", paymentDate=" + paymentDate + ", paymentAmount=" + paymentAmount + ", natureOfDispute="
				+ natureOfDispute + ", processedDate=" + processedDate + "]";
	}

}
