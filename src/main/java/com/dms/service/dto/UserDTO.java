package com.dms.service.dto;

import com.dms.config.Constants;


import com.dms.domain.Authority;
import com.dms.domain.Practice;
import com.dms.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.validation.constraints.*;

import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;

    @NotNull
    @Size(min = 1, max = 10)
    private String userCode;
    
    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    private String email;
    
    
    private Long taxonomy;
    
    private Long providerNpi;
    
    @Size(max = 20)
    private String speciality;
    
    private String phone;
    
    private String attornyCode;

    public String getAttornyCode() {
		return attornyCode;
	}

	public void setAttornyCode(String attornyCode) {
		this.attornyCode = attornyCode;
	}

	@Size(max = 256)
    private String imageUrl;

    
    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	//@NotNull
    @Size(min = 6, max = 60)
    private String password;
    
    private boolean activated = false;

    @Size(min = 2, max = 6)
    private String langKey;
    
    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    //private String practice;
    
    private Set<String> authorities;
  /*  private Set<Practice> practices;

    public Set<Practice> getPractices() {
		return practices;
	}

	public void setPractices(Set<Practice> practices) {
		this.practices = practices;
	}*/
    
    private Set<String> practices;
   

	public Set<String> getPractices() {
		return practices;
	}

	public void setPractices(Set<String> practices) {
		this.practices = practices;
	}
	
	 private Set<String> subAttorneys;

		public Set<String> getSubAttorneys() {
			return subAttorneys;
		}

		public void setSubAttorneys(Set<String> subAttorneys) {
			this.subAttorneys = subAttorneys;
		}
	

	public UserDTO() {
        // Empty constructor needed for Jackson.
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.phone=user.getPhone();
        this.email = user.getEmail();
        this.activated = user.getActivated();
        this.taxonomy=user.getTaxonomy();
        this.providerNpi=user.getProviderNpi();
        this.speciality=user.getSpeciality();
        this.imageUrl = user.getImageUrl();
        this.langKey = user.getLangKey();
        this.createdBy = user.getCreatedBy();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedBy = user.getLastModifiedBy();
        this.userCode =  user.getUserCode();
        this.password= user.getPassword();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.authorities = user.getAuthorities().stream()
            .map(Authority::getName)
            .collect(Collectors.toSet());
        this.practices= user.getPractices().stream().map(Practice::getPracticeCode).collect(Collectors.toSet());
        this.subAttorneys=user.getSubAttorneys().stream().map(User::getUserCode).collect(Collectors.toSet());
        this.attornyCode=user.getAttornyCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    
    
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }
    
    public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	/*public String getPractice() {
		return practice;
	}

	public void setPractice(String practice) {
		this.practice = practice;
	}*/
	public  Long getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(Long taxonomy) {
		this.taxonomy = taxonomy;
	}

	public Long getProviderNpi() {
		return providerNpi;
	}

	public void setProviderNpi(Long providerNpi) {
		this.providerNpi = providerNpi;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	@Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", phone='" + phone + '\'' +
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", createdBy=" + createdBy +
            ", createdDate=" + createdDate +
            ", lastModifiedBy='" + lastModifiedBy + '\'' +
            ", lastModifiedDate=" + lastModifiedDate +
            ", taxonomy=" + taxonomy +
            ", providerNpi=" + providerNpi +
            ", speciality=" + speciality +
            ", password=" + password +
            ", userCode=" + userCode +
         //   ", practice=" + practice +
            ", authorities=" + authorities +
             ", practices=" + practices +
             ", subAttorneys=" + subAttorneys +
            ", attornyCode=" + attornyCode +
            "}";
    }
}
