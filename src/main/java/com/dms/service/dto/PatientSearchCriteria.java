package com.dms.service.dto;

import java.io.Serializable;


/** global search for patient using a single property and search type **/
/** for example "firstName" equals|contains|startsWith searchTerm **/

public class PatientSearchCriteria implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String searchProperty;
    
    private String searchTerm;
    
    private String searchType;
    
    private String attorneyCode;
    
   // private String status;
    
	private Boolean processedFlag;
    
    private Boolean addendumToPriorFlag;
    
    private String doa;
    
    private String dob;
    
    
    public String getSearchProperty() {
		return searchProperty;
	}



	public void setSearchProperty(String searchProperty) {
		this.searchProperty = searchProperty;
	}



	public String getSearchTerm() {
		return searchTerm;
	}



	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}



	public String getSearchType() {
		return searchType;
	}



	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/* public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}*/

	@Override
    public String toString() {
        return "PatientSearchCriteria{" +
        		 (searchProperty != null ? "searchProperty=" + searchProperty+ ", " : "") +
                 (searchTerm != null ? "searchTerm=" + searchTerm+ ", " : "") +
                 (searchType != null ? "searchType=" + searchType+ ", " : "") +
                 (attorneyCode != null ? "attorneyCode=" + attorneyCode+ ", " : "") +
                // (status != null ? "status=" + status+ ", " : "") +
                 (processedFlag != null ? "processedFlag=" + processedFlag+ ", " : "") +
                 (addendumToPriorFlag != null ? "addendumToPriorFlag=" + addendumToPriorFlag+ ", " : "") +
                 (doa != null ? "doa=" + doa+ ", " : "") +
                 (dob != null ? "dob=" + dob+ ", " : "") +
            "}";
    }



	public String getAttorneyCode() {
		return attorneyCode;
	}



	public void setAttorneyCode(String attorneyCode) {
		this.attorneyCode = attorneyCode;
	}



	public Boolean isProcessedFlag() {
		return processedFlag;
	}



	public void setProcessedFlag(Boolean processedFlag) {
		this.processedFlag = processedFlag;
	}



	public Boolean isAddendumToPriorFlag() {
		return addendumToPriorFlag;
	}



	public void setAddendumToPriorFlag(Boolean addendumToPriorFlag) {
		this.addendumToPriorFlag = addendumToPriorFlag;
	}



	public String getDoa() {
		return doa;
	}



	public void setDoa(String doa) {
		this.doa = doa;
	}



	public String getDob() {
		return dob;
	}



	public void setDob(String dob) {
		this.dob = dob;
	}
}
