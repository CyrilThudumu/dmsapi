package com.dms.service.dto;

import javax.validation.constraints.*;

import com.dms.domain.Practice;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Practice entity.
 */
public class PracticeDTO implements Serializable {
	public PracticeDTO() {
        // Empty constructor needed for Jackson.
    }
    public PracticeDTO(Practice practice) {
		//super();
		this.id = practice.getId();
		this.practiceCode = practice.getPracticeCode();
		this.practiceName = practice.getPracticeName();
		this.address1 = practice.getAddress1();
		this.address2 = practice.getAddress2();
		this.city = practice.getCity();
		this.fileName = practice.getFileName();
		this.stateProvince = practice.getStateProvince();
		this.zipCode = practice.getZipCode();
		this.tinNumber = practice.getTinNumber();
		//this.userId = practice.getUser().geti;
		//this.userLogin = practice.getUser().getUserCode();
		this.groupnpi = practice.getGroupnpi();
		this.phone = practice.getPhone();
		this.fax = practice.getFax();
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    @NotNull
    @Size(max = 30)
    private String practiceCode;

    @NotNull
    @Size(max = 200)
    private String practiceName;

    @Size(max = 100)
    private String address1;

    @Size(max = 50)
    private String address2;

    @Size(max = 20)
    private String city;
    
    @Size(max = 40)
    private String fileName;

    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	@Size(max = 20)
    private String stateProvince;

    private String zipCode;
    
   // private Integer tinNumber;
    
  //  private Long tinNumber;
    
    private String tinNumber;

    private Long userId;

    private String userLogin;
    
   
   // private Long groupnpi;
    private String groupnpi;
    
   
    private String phone;
    

    private String fax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPracticeCode() {
        return practiceCode;
    }

    public void setPracticeCode(String practiceCode) {
        this.practiceCode = practiceCode;
    }

    public String getPracticeName() {
        return practiceName;
    }

    public void setPracticeName(String practiceName) {
        this.practiceName = practiceName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
    
    

    public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	public String getGroupnpi() {
		return groupnpi;
	}

	public void setGroupnpi(String groupnpi) {
		this.groupnpi = groupnpi;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PracticeDTO practiceDTO = (PracticeDTO) o;
        if (practiceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), practiceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    
    @Override
    public String toString() {
        return "PracticeDTO{" +
            "id=" + getId() +
            ", practiceCode='" + getPracticeCode() + "'" +
            ", practiceName='" + getPracticeName() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", city='" + getCity() + "'" +
            ", stateProvince='" + getStateProvince() + "'" +
            ", zipCode=" + getZipCode() +
            ", tinNumber=" + getTinNumber() +
            ", fileName=" + getFileName() +
            ", userId=" + getUserId() +
            ", userLogin='" + getUserLogin() + "'" +
            "}";
    }
}
