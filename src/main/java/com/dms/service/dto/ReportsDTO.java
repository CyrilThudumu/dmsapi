package com.dms.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 */
public class ReportsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8791522842650538253L;

	private Long id;

    private String patientCode;

    private String firstName;

    private String lastName;

    
    private String status;

    private String practice;

    private String arbAmount;

    private String attorneyName;

    
    private String paymentAmount;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate paymentDate;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate sentDate;
    private String arbNotes;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate statusDate;

    private String caseType;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate processedDate;
    
    private String natureOfDispute;

   
    

	public ReportsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ReportsDTO(Long id, String patientCode,String lastName, String firstName,LocalDate sentDate,String attorneyName,
			 String paymentAmount,String arbNotes, LocalDate statusDate, String caseType, LocalDate processedDate,String natureOfDispute, 
			 String status, String practice, String arbAmount,
			 LocalDate paymentDate
			  
			 ) {
		super();
		this.id = id;
		this.patientCode = patientCode;
		this.lastName = lastName;
		this.firstName = firstName;
		this.sentDate = sentDate;
		this.attorneyName = attorneyName;
		this.paymentAmount = paymentAmount;
		this.arbAmount = arbAmount;
		this.arbNotes = arbNotes;
		this.statusDate = statusDate;
		this.caseType = caseType;
		this.processedDate = processedDate;
		this.status = status;
		this.practice = practice;
		
		
		
		this.paymentDate = paymentDate;
		
		
		
		this.natureOfDispute = natureOfDispute;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPractice() {
		return practice;
	}

	public void setPractice(String practice) {
		this.practice = practice;
	}

	public String getArbAmount() {
		return arbAmount;
	}

	public void setArbAmount(String arbAmount) {
		this.arbAmount = arbAmount;
	}

	public String getAttorneyName() {
		return attorneyName;
	}

	public void setAttorneyName(String attorneyName) {
		this.attorneyName = attorneyName;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public LocalDate getSentDate() {
		return sentDate;
	}

	public void setSentDate(LocalDate sentDate) {
		this.sentDate = sentDate;
	}

	public String getArbNotes() {
		return arbNotes;
	}

	public void setArbNotes(String arbNotes) {
		this.arbNotes = arbNotes;
	}

	public LocalDate getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(LocalDate statusDate) {
		this.statusDate = statusDate;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public LocalDate getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(LocalDate processedDate) {
		this.processedDate = processedDate;
	}

	
	
	public String getNatureOfDispute() {
		return natureOfDispute;
	}

	public void setNatureOfDispute(String natureOfDispute) {
		this.natureOfDispute = natureOfDispute;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportsDTO patientDTO = (ReportsDTO) o;
        if (patientDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patientDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
            "id=" + getId() +
            ", patientCode='" + getPatientCode() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", status='" + getStatus() + "'" +
            ", practice='" + getPractice() + "'" +
            ", arbAmount='" + getArbAmount()+ "'" +
            ", attorneyName='" + getAttorneyName()+ "'" +
            ", paymentAmount='" + getPaymentAmount()+ "'" +
            ", paymentDate='" + getPaymentDate()+ "'" +
            ", sentDate='" + getSentDate()+ "'" +
            ", statusDate='" + getStatusDate()+ "'" +
            ", caseType='" + getCaseType() + "'" +
            ", processedDate='" + getProcessedDate() + "'" +
            ", natureOfDispute='" + getNatureOfDispute() + "'" +
            "}";
    }
}
