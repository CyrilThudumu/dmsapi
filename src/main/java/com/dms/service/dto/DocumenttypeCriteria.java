package com.dms.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Documenttype entity. This class is used in DocumenttypeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /documenttypes?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DocumenttypeCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter documentTypeName;

    private IntegerFilter documentTypeCategory;

    private LongFilter practiceId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDocumentTypeName() {
        return documentTypeName;
    }

    public void setDocumentTypeName(StringFilter documentTypeName) {
        this.documentTypeName = documentTypeName;
    }

    public IntegerFilter getDocumentTypeCategory() {
        return documentTypeCategory;
    }

    public void setDocumentTypeCategory(IntegerFilter documentTypeCategory) {
        this.documentTypeCategory = documentTypeCategory;
    }

    public LongFilter getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(LongFilter practiceId) {
        this.practiceId = practiceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DocumenttypeCriteria that = (DocumenttypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(documentTypeName, that.documentTypeName) &&
            Objects.equals(documentTypeCategory, that.documentTypeCategory) &&
            Objects.equals(practiceId, that.practiceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        documentTypeName,
        documentTypeCategory,
        practiceId
        );
    }

    @Override
    public String toString() {
        return "DocumenttypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (documentTypeName != null ? "documentTypeName=" + documentTypeName + ", " : "") +
                (documentTypeCategory != null ? "documentTypeCategory=" + documentTypeCategory + ", " : "") +
                (practiceId != null ? "practiceId=" + practiceId + ", " : "") +
            "}";
    }

}
