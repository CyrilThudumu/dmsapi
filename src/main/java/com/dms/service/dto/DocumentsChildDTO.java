package com.dms.service.dto;

import java.io.Serializable;
import java.util.ArrayList;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * A DTO for the Documents entity.
 */


public class DocumentsChildDTO implements Serializable{

	

	private static final long serialVersionUID = 1L;

	
	@Size(max = 30)
	private Long id;
	
	
	public ArrayList<String> documentTypes;
	public ArrayList<String> documentName;
	public ArrayList<String> dosFrom;
	public ArrayList<String> dosTo;
	public String doa;
	public String dob;
	public ArrayList<String> pageNumber;
	public String practiceCode;
	public String searchBy;
	public String searchValue;
	public String patientCode;
	public String pageNumberStr;
	public String doctorCode;
	private String attorneyCode;
	private String filePath;
	
	
	
	
	public DocumentsChildDTO(@Size(max = 30) Long id,
			ArrayList<String> documentTypes, ArrayList<String> dosFrom,
			ArrayList<String> dosTo, String doa, String dob,
			ArrayList<String> pageNumber, String practiceCode, String searchBy,
			String searchValue, String patientCode, String pageNumberStr,
			String doctorCode, String attorneyCode, String filePath) {
		super();
		this.id = id;
		this.documentTypes = documentTypes;
		this.dosFrom = dosFrom;
		this.dosTo = dosTo;
		this.doa = doa;
		this.dob = dob;
		this.pageNumber = pageNumber;
		this.practiceCode = practiceCode;
		this.searchBy = searchBy;
		this.searchValue = searchValue;
		this.patientCode = patientCode;
		this.pageNumberStr = pageNumberStr;
		this.doctorCode = doctorCode;
		this.attorneyCode = attorneyCode;
		this.filePath = filePath;
	}
	
	public DocumentsChildDTO() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArrayList<String> getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(ArrayList<String> documentTypes) {
		this.documentTypes = documentTypes;
	}

	public ArrayList<String> getDocumentName() {
		return documentName;
	}

	public void setDocumentName(ArrayList<String> documentName) {
		this.documentName = documentName;
	}

	public ArrayList<String> getDosFrom() {
		return dosFrom;
	}

	public void setDosFrom(ArrayList<String> dosFrom) {
		this.dosFrom = dosFrom;
	}

	public ArrayList<String> getDosTo() {
		return dosTo;
	}

	public void setDosTo(ArrayList<String> dosTo) {
		this.dosTo = dosTo;
	}

	public String getDoa() {
		return doa;
	}

	public void setDoa(String doa) {
		this.doa = doa;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public ArrayList<String> getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(ArrayList<String> pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getPracticeCode() {
		return practiceCode;
	}

	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPageNumberStr() {
		return pageNumberStr;
	}

	public void setPageNumberStr(String pageNumberStr) {
		this.pageNumberStr = pageNumberStr;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getAttorneyCode() {
		return attorneyCode;
	}

	public void setAttorneyCode(String attorneyCode) {
		this.attorneyCode = attorneyCode;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "DocumentsDTO [id=" + id + ", documentTypes=" + documentTypes
				+ ", documentName=" + documentName + ", dosFrom=" + dosFrom
				+ ", dosTo=" + dosTo + ", doa=" + doa + ", dob=" + dob
				+ ", pageNumber=" + pageNumber + ", practiceCode="
				+ practiceCode + ", searchBy=" + searchBy + ", searchValue="
				+ searchValue + ", patientCode=" + patientCode
				+ ", pageNumberStr=" + pageNumberStr + ", doctorCode="
				+ doctorCode + ", attorneyCode=" + attorneyCode + ", filePath="
				+ filePath + "]";
	}

	
	
	
	
	
	
	
}
