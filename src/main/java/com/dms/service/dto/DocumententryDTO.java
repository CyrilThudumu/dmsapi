package com.dms.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Documententry entity.
 */
public class DocumententryDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String documentCode;

    @NotNull
    @Size(max = 60)
    private String documentName;

    @NotNull
    private LocalDate fromField;

    @NotNull
    private LocalDate toField;

    @Size(max = 300)
    private String notes;

    
    @Lob
    private byte[] content;
    private String contentContentType;

    @NotNull
    @Size(max = 10)
    private String fromTo1;

    @Size(max = 10)
    private String fromTo2;

    @Size(max = 10)
    private String fromTo3;

    @Size(max = 10)
    private String fromTo4;

    @Size(max = 10)
    private String fromTo5;

    @Size(max = 10)
    private String fromTo6;

    @Size(max = 10)
    private String fromTo7;

    @Size(max = 10)
    private String fromTo8;

    @Size(max = 10)
    private String fromTo9;

    private Long documenttypeForfromTo1Id;

    private String documenttypeForfromTo1DocumentTypeName;

    private Long documenttypeForfromTo2Id;

    private String documenttypeForfromTo2DocumentTypeName;

    private Long documenttypeForfromTo3Id;

    private String documenttypeForfromTo3DocumentTypeName;

    private Long documenttypeForfromTo4Id;

    private String documenttypeForfromTo4DocumentTypeName;

    private Long documenttypeForfromTo5Id;

    private String documenttypeForfromTo5DocumentTypeName;

    private Long documenttypeForfromTo6Id;

    private String documenttypeForfromTo6DocumentTypeName;

    private Long patientId;

    private String patientPatientCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public LocalDate getFromField() {
        return fromField;
    }

    public void setFromField(LocalDate fromField) {
        this.fromField = fromField;
    }

    public LocalDate getToField() {
        return toField;
    }

    public void setToField(LocalDate toField) {
        this.toField = toField;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public String getFromTo1() {
        return fromTo1;
    }

    public void setFromTo1(String fromTo1) {
        this.fromTo1 = fromTo1;
    }

    public String getFromTo2() {
        return fromTo2;
    }

    public void setFromTo2(String fromTo2) {
        this.fromTo2 = fromTo2;
    }

    public String getFromTo3() {
        return fromTo3;
    }

    public void setFromTo3(String fromTo3) {
        this.fromTo3 = fromTo3;
    }

    public String getFromTo4() {
        return fromTo4;
    }

    public void setFromTo4(String fromTo4) {
        this.fromTo4 = fromTo4;
    }

    public String getFromTo5() {
        return fromTo5;
    }

    public void setFromTo5(String fromTo5) {
        this.fromTo5 = fromTo5;
    }

    public String getFromTo6() {
        return fromTo6;
    }

    public void setFromTo6(String fromTo6) {
        this.fromTo6 = fromTo6;
    }

    public String getFromTo7() {
        return fromTo7;
    }

    public void setFromTo7(String fromTo7) {
        this.fromTo7 = fromTo7;
    }

    public String getFromTo8() {
        return fromTo8;
    }

    public void setFromTo8(String fromTo8) {
        this.fromTo8 = fromTo8;
    }

    public String getFromTo9() {
        return fromTo9;
    }

    public void setFromTo9(String fromTo9) {
        this.fromTo9 = fromTo9;
    }

    public Long getDocumenttypeForfromTo1Id() {
        return documenttypeForfromTo1Id;
    }

    public void setDocumenttypeForfromTo1Id(Long documenttypeId) {
        this.documenttypeForfromTo1Id = documenttypeId;
    }

    public String getDocumenttypeForfromTo1DocumentTypeName() {
        return documenttypeForfromTo1DocumentTypeName;
    }

    public void setDocumenttypeForfromTo1DocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeForfromTo1DocumentTypeName = documenttypeDocumentTypeName;
    }

    public Long getDocumenttypeForfromTo2Id() {
        return documenttypeForfromTo2Id;
    }

    public void setDocumenttypeForfromTo2Id(Long documenttypeId) {
        this.documenttypeForfromTo2Id = documenttypeId;
    }

    public String getDocumenttypeForfromTo2DocumentTypeName() {
        return documenttypeForfromTo2DocumentTypeName;
    }

    public void setDocumenttypeForfromTo2DocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeForfromTo2DocumentTypeName = documenttypeDocumentTypeName;
    }

    public Long getDocumenttypeForfromTo3Id() {
        return documenttypeForfromTo3Id;
    }

    public void setDocumenttypeForfromTo3Id(Long documenttypeId) {
        this.documenttypeForfromTo3Id = documenttypeId;
    }

    public String getDocumenttypeForfromTo3DocumentTypeName() {
        return documenttypeForfromTo3DocumentTypeName;
    }

    public void setDocumenttypeForfromTo3DocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeForfromTo3DocumentTypeName = documenttypeDocumentTypeName;
    }

    public Long getDocumenttypeForfromTo4Id() {
        return documenttypeForfromTo4Id;
    }

    public void setDocumenttypeForfromTo4Id(Long documenttypeId) {
        this.documenttypeForfromTo4Id = documenttypeId;
    }

    public String getDocumenttypeForfromTo4DocumentTypeName() {
        return documenttypeForfromTo4DocumentTypeName;
    }

    public void setDocumenttypeForfromTo4DocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeForfromTo4DocumentTypeName = documenttypeDocumentTypeName;
    }

    public Long getDocumenttypeForfromTo5Id() {
        return documenttypeForfromTo5Id;
    }

    public void setDocumenttypeForfromTo5Id(Long documenttypeId) {
        this.documenttypeForfromTo5Id = documenttypeId;
    }

    public String getDocumenttypeForfromTo5DocumentTypeName() {
        return documenttypeForfromTo5DocumentTypeName;
    }

    public void setDocumenttypeForfromTo5DocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeForfromTo5DocumentTypeName = documenttypeDocumentTypeName;
    }

    public Long getDocumenttypeForfromTo6Id() {
        return documenttypeForfromTo6Id;
    }

    public void setDocumenttypeForfromTo6Id(Long documenttypeId) {
        this.documenttypeForfromTo6Id = documenttypeId;
    }

    public String getDocumenttypeForfromTo6DocumentTypeName() {
        return documenttypeForfromTo6DocumentTypeName;
    }

    public void setDocumenttypeForfromTo6DocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeForfromTo6DocumentTypeName = documenttypeDocumentTypeName;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getPatientPatientCode() {
        return patientPatientCode;
    }

    public void setPatientPatientCode(String patientPatientCode) {
        this.patientPatientCode = patientPatientCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DocumententryDTO documententryDTO = (DocumententryDTO) o;
        if (documententryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documententryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DocumententryDTO{" +
            "id=" + getId() +
            ", documentCode='" + getDocumentCode() + "'" +
            ", documentName='" + getDocumentName() + "'" +
            ", fromField='" + getFromField() + "'" +
            ", toField='" + getToField() + "'" +
            ", notes='" + getNotes() + "'" +
            ", content='" + getContent() + "'" +
            ", fromTo1='" + getFromTo1() + "'" +
            ", fromTo2='" + getFromTo2() + "'" +
            ", fromTo3='" + getFromTo3() + "'" +
            ", fromTo4='" + getFromTo4() + "'" +
            ", fromTo5='" + getFromTo5() + "'" +
            ", fromTo6='" + getFromTo6() + "'" +
            ", fromTo7='" + getFromTo7() + "'" +
            ", fromTo8='" + getFromTo8() + "'" +
            ", fromTo9='" + getFromTo9() + "'" +
            ", documenttypeForfromTo1=" + getDocumenttypeForfromTo1Id() +
            ", documenttypeForfromTo1='" + getDocumenttypeForfromTo1DocumentTypeName() + "'" +
            ", documenttypeForfromTo2=" + getDocumenttypeForfromTo2Id() +
            ", documenttypeForfromTo2='" + getDocumenttypeForfromTo2DocumentTypeName() + "'" +
            ", documenttypeForfromTo3=" + getDocumenttypeForfromTo3Id() +
            ", documenttypeForfromTo3='" + getDocumenttypeForfromTo3DocumentTypeName() + "'" +
            ", documenttypeForfromTo4=" + getDocumenttypeForfromTo4Id() +
            ", documenttypeForfromTo4='" + getDocumenttypeForfromTo4DocumentTypeName() + "'" +
            ", documenttypeForfromTo5=" + getDocumenttypeForfromTo5Id() +
            ", documenttypeForfromTo5='" + getDocumenttypeForfromTo5DocumentTypeName() + "'" +
            ", documenttypeForfromTo6=" + getDocumenttypeForfromTo6Id() +
            ", documenttypeForfromTo6='" + getDocumenttypeForfromTo6DocumentTypeName() + "'" +
            ", patient=" + getPatientId() +
            ", patient='" + getPatientPatientCode() + "'" +
            "}";
    }
}
