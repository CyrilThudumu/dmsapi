package com.dms.service.dto;

import java.io.Serializable;

/**
 * A DTO for the CaselogDTO entity.
 */

public class CaselogDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	public String fromDos;
	public String toDos;
	public String patientCode;
	private String caseId;
	private String caseComments;
	private String commentedBy;
	private String commentedDate;
	
	public CaselogDTO(Long id,  String fromDos,
			String toDos, String patientCode, 
			String caseId,String caseComments, String commentedBy, String commentedDate) {
		super();
		this.id = id;
		this.fromDos = fromDos;
		this.toDos = toDos;
		this.patientCode = patientCode;
		this.caseId = caseId;
		this.caseComments = caseComments;
		this.commentedBy=commentedBy;
		this.commentedDate=commentedDate;
	}

	public String getCommentedBy() {
		return commentedBy;
	}

	public void setCommentedBy(String commentedBy) {
		this.commentedBy = commentedBy;
	}

	public String getCommentedDate() {
		return commentedDate;
	}

	public void setCommentedDate(String commentedDate) {
		this.commentedDate = commentedDate;
	}

	public CaselogDTO() {
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFromDos() {
		return fromDos;
	}

	public void setFromDos(String fromDos) {
		this.fromDos = fromDos;
	}

	public String getToDos() {
		return toDos;
	}

	public void setToDos(String toDos) {
		this.toDos = toDos;
	}


	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCaseComments() {
		return caseComments;
	}

	public void setCaseComments(String caseComments) {
		this.caseComments = caseComments;
	}

	@Override
	public String toString() {
		return "CaselogDTO [id=" + id + ", fromDos=" + fromDos + ", toDos=" + toDos + ", patientCode=" + patientCode
				+ ", caseId=" + caseId + ", caseComments=" + caseComments + ", commentedBy=" + commentedBy
				+ ", commentedDate=" + commentedDate + "]";
	}

	/*@Override
	public String toString() {
		return "DocumentsDTO [id=" + id + ", fromDos=" + fromDos + ", toDos=" + toDos
				+ ", patientCode=" + patientCode + ", caseId="
				+ caseId + ", caseComments=" + caseComments + "]";
	}*/
	
	
}
