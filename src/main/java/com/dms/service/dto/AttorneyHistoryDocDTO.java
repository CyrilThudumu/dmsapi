package com.dms.service.dto;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the Documents entity.
 */


public class AttorneyHistoryDocDTO implements Serializable{
	
	
	private PatientDTO patientDTO;
	private List<DocumentsDTO> documentsDTO;
	
	
	public AttorneyHistoryDocDTO() {
		// TODO Auto-generated constructor stub
	}


	public PatientDTO getPatientDTO() {
		return patientDTO;
	}


	public void setPatientDTO(PatientDTO patientDTO) {
		this.patientDTO = patientDTO;
	}


	public List<DocumentsDTO> getDocumentsDTO() {
		return documentsDTO;
	}


	public void setDocumentsDTO(List<DocumentsDTO> documentsDTO) {
		this.documentsDTO = documentsDTO;
	}


	public AttorneyHistoryDocDTO(PatientDTO patientDTO,
			List<DocumentsDTO> documentsDTO) {
		super();
		this.patientDTO = patientDTO;
		this.documentsDTO = documentsDTO;
	}


	@Override
	public String toString() {
		return "AttorneyHistoryDocDTO [patientDTO=" + patientDTO
				+ ", documentsDTO=" + documentsDTO + "]";
	}

	
	
	
	
	
}
