package com.dms.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Documenttype entity.
 */
public class DocumenttypeDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String documentTypeName;

    @NotNull
    private Integer documentTypeCategory;

    private Long practiceId;

    private String practicePracticeCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentTypeName() {
        return documentTypeName;
    }

    public void setDocumentTypeName(String documentTypeName) {
        this.documentTypeName = documentTypeName;
    }

    public Integer getDocumentTypeCategory() {
        return documentTypeCategory;
    }

    public void setDocumentTypeCategory(Integer documentTypeCategory) {
        this.documentTypeCategory = documentTypeCategory;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getPracticePracticeCode() {
        return practicePracticeCode;
    }

    public void setPracticePracticeCode(String practicePracticeCode) {
        this.practicePracticeCode = practicePracticeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DocumenttypeDTO documenttypeDTO = (DocumenttypeDTO) o;
        if (documenttypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documenttypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DocumenttypeDTO{" +
            "id=" + getId() +
            ", documentTypeName='" + getDocumentTypeName() + "'" +
            ", documentTypeCategory=" + getDocumentTypeCategory() +
            ", practice=" + getPracticeId() +
            ", practice='" + getPracticePracticeCode() + "'" +
            "}";
    }
}
