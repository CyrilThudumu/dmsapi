package com.dms.service.dto;

import java.io.Serializable;

import org.springframework.data.domain.Page;

/**
 * A DTO for the Practice entity.
 */
public class DataDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private Page<PatientDTO> patientList;
	private Page<DoctorDTO> doctorList;
	
	public DataDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DataDTO(Page<PatientDTO> patientList, Page<DoctorDTO> doctorList) {
		super();
		this.patientList = patientList;
		this.doctorList = doctorList;
	}

	public Page<PatientDTO> getPatientList() {
		return patientList;
	}

	public void setPatientList(Page<PatientDTO> patientList) {
		this.patientList = patientList;
	}

	public Page<DoctorDTO> getDoctorList() {
		return doctorList;
	}

	public void setDoctorList(Page<DoctorDTO> doctorList) {
		this.doctorList = doctorList;
	}

	@Override
	public String toString() {
		return "DataDTO [patientList=" + patientList + ", doctorList="
				+ doctorList + "]";
	}
	
	
	
}
