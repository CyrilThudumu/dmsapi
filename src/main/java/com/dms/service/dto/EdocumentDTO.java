package com.dms.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Edocument entity.
 */
public class EdocumentDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String documentCode;

    @NotNull
    @Size(max = 60)
    private String documentName;

    @NotNull
    private LocalDate fromField;

    @NotNull
    private LocalDate toField;

    @Size(max = 300)
    private String notes;

    @NotNull
    @Size(max = 300)
    private String url;

    private Long documenttypeId;

    private String documenttypeDocumentTypeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public LocalDate getFromField() {
        return fromField;
    }

    public void setFromField(LocalDate fromField) {
        this.fromField = fromField;
    }

    public LocalDate getToField() {
        return toField;
    }

    public void setToField(LocalDate toField) {
        this.toField = toField;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getDocumenttypeId() {
        return documenttypeId;
    }

    public void setDocumenttypeId(Long documenttypeId) {
        this.documenttypeId = documenttypeId;
    }

    public String getDocumenttypeDocumentTypeName() {
        return documenttypeDocumentTypeName;
    }

    public void setDocumenttypeDocumentTypeName(String documenttypeDocumentTypeName) {
        this.documenttypeDocumentTypeName = documenttypeDocumentTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EdocumentDTO edocumentDTO = (EdocumentDTO) o;
        if (edocumentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), edocumentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EdocumentDTO{" +
            "id=" + getId() +
            ", documentCode='" + getDocumentCode() + "'" +
            ", documentName='" + getDocumentName() + "'" +
            ", fromField='" + getFromField() + "'" +
            ", toField='" + getToField() + "'" +
            ", notes='" + getNotes() + "'" +
            ", url='" + getUrl() + "'" +
            ", documenttype=" + getDocumenttypeId() +
            ", documenttype='" + getDocumenttypeDocumentTypeName() + "'" +
            "}";
    }
}
