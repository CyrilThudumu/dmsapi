package com.dms.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Insurance entity. This class is used in InsuranceResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /insurance?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InsuranceCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter insuranceCode;

    private StringFilter insuranceName;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(StringFilter insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public StringFilter getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(StringFilter insuranceName) {
        this.insuranceName = insuranceName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InsuranceCriteria that = (InsuranceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(insuranceCode, that.insuranceCode) &&
            Objects.equals(insuranceName, that.insuranceCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        insuranceCode,
        insuranceName
        );
    }

    @Override
    public String toString() {
        return "InsuranceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (insuranceCode != null ? "insuranceCode=" + insuranceCode + ", " : "") +
                (insuranceCode != null ? "insuranceCode=" + insuranceCode + ", " : "") +
            "}";
    }

}
