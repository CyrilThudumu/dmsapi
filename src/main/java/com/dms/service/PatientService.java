package com.dms.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dms.domain.Patient;
import com.dms.service.dto.PatientDTO;

/**
 * Service Interface for managing Patient.
 */
public interface PatientService {

	/**
	 * Save a patient.
	 *
	 * @param patientDTO the entity to save
	 * @return the persisted entity
	 */
	PatientDTO save(PatientDTO patientDTO);

	/**
	 * Get all the patients.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	Page<PatientDTO> findAll(Pageable pageable);

	/**
	 * Get all the Patient with eager load of many-to-many relationships.
	 *
	 * @return the list of entities
	 */
	Page<PatientDTO> findAllWithEagerRelationships(Pageable pageable);

	/**
	 * Get the "id" patient.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	Optional<PatientDTO> findOne(Long id);
	
	/**
	 * Get the "patientCode" patient.
	 *
	 * @param id the id of the entity
	 * @return the entity
	Optional<PatientDTO> findByPatientCode(String patientCode);
	
	Patient findByPatientCode(String patientCode); */

	/**
	 * Delete the "id" patient.
	 *
	 * @param id the id of the entity
	 */
	void delete(Long id);

	/**
	 * Get the "attorneyCode" patient.
	 *
	 * @param id the attorneyCode of the entity
	 * @return the entity
	 */
	Page<PatientDTO> getAttorneyPatients(String attorneyCode, Pageable page);
	
	Page<PatientDTO> patientsProcessedByAttorney(String attorneyCode, String status, Pageable page);
	
	Page<PatientDTO> patientsNotProcessedByAttorney(String attorneyCode, String status, Pageable page);
	
	Page<PatientDTO> addendumToPriorPatients(String attorneyCode, String status, Pageable page);
	
	/*Page<PatientDTO> patientsNotProcessedByAttorneySearch(String attorneyCode, String searchOption, String searchKey, Pageable page);*/
	
	
	/*Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingPatientCode(String attorneyCode, String searchKey, Pageable page);
	
	Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingFirstName(String attorneyCode, String searchKey, Pageable page);
	
	Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingLastName(String attorneyCode, String searchKey, Pageable page);
	
	Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingPrimaryInsClaimNo(String attorneyCode, String searchKey, Pageable page);
	
	Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingPrimaryInsPolicyNo(String attorneyCode, String searchKey, Pageable page);
	
    Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingDoa(String attorneyCode, String searchKey, Pageable page);
	
	Page<PatientDTO> searchPatientsNotProcessedByAttorneyUsingDob(String attorneyCode, String searchKey, Pageable page);
	*/
	/**
	 * Get the "practiceCode" patient.
	 *
	 * @param id the attorneyCode of the entity
	 * @return the entity
	 */
	Page<PatientDTO> getPatientsForPractice(String practiceCode, Pageable page);
	
	/**
	 * Get the "doctorCode" patient.
	 *
	 * @param id the attorneyCode of the entity
	 * @return the entity
	 */
	Page<PatientDTO> getDocPatientSearch(String doctorCode, String searchKey, Pageable page);
	
	
	/**
	 * Get the "attorneyCode" patient.
	 *
	 * @param id the attorneyCode of the entity
	 * @return the entity
	 */
	Page<PatientDTO> getAttorneyPatientSearch(String attorneyCode, String searchKey, Pageable page);
	
	
	
	
	/**
	 * Get the "doctorCode" and "doctorCode" patient.
	 *
	 * @param id the doctorCode of the entity
	 * @return the entity
	 */
	Page<PatientDTO> getDoctorPatients(String doctorCode, Pageable page);
	
	/**
	 * Get the "attorneyCode" patient.
	 *
	 * @param id the attorneyCode of the entity
	 * @return the entity
	 */
	
	Optional<PatientDTO> getPatientByCode(String patientCode);
	
	PatientDTO findOneByPatientCode(String attorneyCode);
	/**
	 * Get all the patients associated for the practice
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	Page<PatientDTO> findAllByPractice(String practiceCode, Pageable pageable);
	
	Page<PatientDTO> findAllByPractices(List<String> practiceCode, Pageable pageable);

	/**
	 * Update the practice for the patient
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	public void updatedAttorneyCodeForPatient(String attorneyCode, String patientCode);

	//Optional<PatientDTO> getPatientByPatientCode(String patientCode);

}
