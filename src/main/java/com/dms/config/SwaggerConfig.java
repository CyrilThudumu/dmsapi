package com.dms.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.Collections;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public Docket dmsApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.dms.web.rest"))
				.paths(regex("/users.*"))
				.build()
				.apiInfo(metaData());
		
	}
	
	  private ApiInfo metaData() {
	      
		//ApiInfo apiInfo = new ApiInfo("Spring Boot Rest API", "Document Management System REST API", "1.0", "termsOfServiceUrl", "Cyril Thudumu", "Apache License Version 2.0", "https://www.apache.org/licenses/LICENSE-2.0");
		 ApiInfo apiInfo = new ApiInfo(
	                "Spring Boot REST API",
	                "Spring Boot REST API for Document Management System",
	                "1.0",
	                "Terms of service",
	                new Contact("Cyril Thudumu", "https://dmstest.mdmanage.com", "cyril.thudumu@vcarve.com"),
	               // new Contact("Cyril Thudumu", "https://dmsprod.mdmanage.com", "cyril.thudumu@vcarve.com"),
	               "Apache License Version 2.0",
	                "https://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
	        return apiInfo;
	    }
	  
	  
}
