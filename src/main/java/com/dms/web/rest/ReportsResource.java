package com.dms.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dms.service.ReportsQueryService;
import com.dms.service.ReportsService;
import com.dms.service.dto.ReportsCriteria;
import com.dms.service.dto.ReportsDTO;

/**
 * REST controller for managing Reports.
 */
@RestController
@RequestMapping("/api")
public class ReportsResource {

    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);


  //  @Autowired
  //  ReportsService reportsService;
    
  //  @Autowired
   // private  ReportsQueryService reportsQueryService;
    

 /*   public ReportsResource(ReportsQueryService reportsQueryService) {
    	this.reportsQueryService = reportsQueryService;
    }*/
    
    /**
     * GET  /reports : get content of the given Search Criteria.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of certifier in body
     */
    
	/*	@PostMapping("/reports")
		@ResponseBody
		public Page<ReportsDTO> masterReport(@RequestBody ReportsCriteria reportsCriteria){
			return  reportsService.getFinalReport(reportsCriteria);			
		}*/
   
}
