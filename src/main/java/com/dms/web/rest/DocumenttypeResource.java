package com.dms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.DocumenttypeService;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.dms.service.dto.DocumenttypeDTO;
import com.dms.service.dto.DocumenttypeCriteria;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.DocumenttypeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Documenttype.
 */
@RestController
@RequestMapping("/api")
public class DocumenttypeResource {

    private final Logger log = LoggerFactory.getLogger(DocumenttypeResource.class);

    private static final String ENTITY_NAME = "documenttype";

    private final DocumenttypeService documenttypeService;

    private final DocumenttypeQueryService documenttypeQueryService;

    public DocumenttypeResource(DocumenttypeService documenttypeService, DocumenttypeQueryService documenttypeQueryService) {
        this.documenttypeService = documenttypeService;
        this.documenttypeQueryService = documenttypeQueryService;
    }

    /**
     * POST  /documenttypes : Create a new documenttype.
     *
     * @param documenttypeDTO the documenttypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documenttypeDTO, or with status 400 (Bad Request) if the documenttype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documenttypes")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<DocumenttypeDTO> createDocumenttype(@Valid @RequestBody DocumenttypeDTO documenttypeDTO) throws URISyntaxException {
        log.debug("REST request to save Documenttype : {}", documenttypeDTO);
        if (documenttypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new documenttype cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DocumenttypeDTO result = documenttypeService.save(documenttypeDTO);
        return ResponseEntity.created(new URI("/api/documenttypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /documenttypes : Updates an existing documenttype.
     *
     * @param documenttypeDTO the documenttypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documenttypeDTO,
     * or with status 400 (Bad Request) if the documenttypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the documenttypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/documenttypes")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<DocumenttypeDTO> updateDocumenttype(@Valid @RequestBody DocumenttypeDTO documenttypeDTO) throws URISyntaxException {
        log.debug("REST request to update Documenttype : {}", documenttypeDTO);
        if (documenttypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DocumenttypeDTO result = documenttypeService.save(documenttypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documenttypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /documenttypes : get all the documenttypes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of documenttypes in body
     */
    @GetMapping("/documenttypes")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
   // public ResponseEntity<List<DocumenttypeDTO>> getAllDocumenttypes(DocumenttypeCriteria criteria, Pageable pageable) {
    public ResponseEntity<List<DocumenttypeDTO>> getDocumenttypes(DocumenttypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Documenttypes by criteria: {}", criteria);
        Page<DocumenttypeDTO> page = documenttypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/documenttypes");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    /**
         * GET  /documenttypes : get all the documenttypes.
        *
       */
        @GetMapping("/documenttypes/all")
        @Timed
        @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.USER+ "\")" + " || hasRole(\""+AuthoritiesConstants.USER_ADMIN+ "\")")
        public ResponseEntity<List<DocumenttypeDTO>> getAllDocumenttypes() {
            log.debug("REST request to get ALL Documenttypes");
            List<DocumenttypeDTO> page = documenttypeQueryService.findByCriteria(null);
            HttpHeaders headers = new HttpHeaders();
            return ResponseEntity.ok().headers(headers).body(page);
        }

    /**
    * GET  /documenttypes/count : count all the documenttypes.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/documenttypes/count")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Long> countDocumenttypes(DocumenttypeCriteria criteria) {
        log.debug("REST request to count Documenttypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(documenttypeQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /documenttypes/:id : get the "id" documenttype.
     *
     * @param id the id of the documenttypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documenttypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/documenttypes/{id}")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<DocumenttypeDTO> getDocumenttype(@PathVariable Long id) {
        log.debug("REST request to get Documenttype : {}", id);
        Optional<DocumenttypeDTO> documenttypeDTO = documenttypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(documenttypeDTO);
    }

    /**
     * DELETE  /documenttypes/:id : delete the "id" documenttype.
     *
     * @param id the id of the documenttypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/documenttypes/{id}")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteDocumenttype(@PathVariable Long id) {
        log.debug("REST request to delete Documenttype : {}", id);
        documenttypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
