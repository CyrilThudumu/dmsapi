package com.dms.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.github.jhipster.web.util.ResponseUtil;
import com.codahale.metrics.annotation.Timed;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.InsuranceQueryService;
import com.dms.service.InsuranceService;
import com.dms.service.dto.InsuranceCriteria;
import com.dms.service.dto.InsuranceDTO;
import com.dms.web.rest.util.PaginationUtil;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import java.util.Optional;
/**
 * REST controller for managing Insurance.
 */
@RestController
@RequestMapping("/api")
public class InsuranceResource {

    private final Logger log = LoggerFactory.getLogger(InsuranceResource.class);

    private static final String ENTITY_NAME = "insurance";

    private final InsuranceService insuranceService;
    //private final CasetypeService casetypeService;

    private final InsuranceQueryService insuranceQueryService;

    public InsuranceResource(InsuranceQueryService insuranceQueryService, InsuranceService insuranceService) {
        this.insuranceQueryService = insuranceQueryService;
        this.insuranceService=insuranceService;
    }
/*
    *//**
     * POST  /insurance : Create a new insurance.
     *
     * @param insuranceDTO the insuranceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new insuranceDTO, or with status 400 (Bad Request) if the insurance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/insurances")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<InsuranceDTO> createInsurance(@Valid @RequestBody InsuranceDTO insuranceDTO) throws URISyntaxException {
        log.debug("REST request to save insurance : {}", insuranceDTO);
        if (insuranceDTO.getId() != null) {
            throw new BadRequestAlertException("A new insurance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InsuranceDTO result = insuranceService.save(insuranceDTO);
        return ResponseEntity.created(new URI("/api/casetypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /insurances : Updates an existing insurance.
     *
     * @param insuranceDTO the insuranceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated insuranceDTO,
     * or with status 400 (Bad Request) if the insuranceDTO is not valid,
     * or with status 500 (Internal Server Error) if the insuranceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/insurances")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<InsuranceDTO> updateInsurance(@Valid @RequestBody InsuranceDTO insuranceDTO) throws URISyntaxException {
        log.debug("REST request to update insurance : {}", insuranceDTO);
        if (insuranceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InsuranceDTO result = insuranceService.save(insuranceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, insuranceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /insurances : get all the insurances.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of Insurance in body
     */
    @GetMapping("/insurances")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    //public ResponseEntity<List<InsuranceDTO>> getAllInsurances(InsuranceCriteria criteria, Pageable pageable) {
    public ResponseEntity<List<InsuranceDTO>> getInsurances(InsuranceCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Insurances by criteria: {}", criteria);
        Page<InsuranceDTO> page = insuranceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/insurances");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    
    /**
         * GET  /insurances : get all the insurances.
         *
         * @return the ResponseEntity with status 200 (OK) and the list of Insurance in body
        */
        @GetMapping("/insurances/all")
        @Timed
        @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.USER+ "\")" + " || hasRole(\""+AuthoritiesConstants.USER_ADMIN+ "\")")
        public ResponseEntity<List<InsuranceDTO>> getAllInsurances() {
            log.debug("REST request to get ALL Insurances");
            List<InsuranceDTO> page = insuranceQueryService.findByCriteria(null);
            HttpHeaders headers = new HttpHeaders();
            return ResponseEntity.ok().headers(headers).body(page);
        }
        
    /**
    * GET  /insurances/count : count all the insurances.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/insurances/count")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Long> countCasetypes(InsuranceCriteria criteria) {
        log.debug("REST request to count Insurance by criteria: {}", criteria);
        return ResponseEntity.ok().body(insuranceQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /insurances/:id : get the "id" insurance.
     *
     * @param id the id of the insuranceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the insuranceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/insurances/{id}")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<InsuranceDTO> getInsurance(@PathVariable Long id) {
        log.debug("REST request to get insurance : {}", id);
        Optional<InsuranceDTO> insuranceDTO = insuranceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(insuranceDTO);
    }

    /**
     * DELETE  /insurances/:id : delete the "id" insurance.
     *
     * @param id the id of the insuranceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/insurances/{id}")
    @Timed
     @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteInsurance(@PathVariable Long id) {
        log.debug("REST request to delete insurance : {}", id);
        insuranceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
