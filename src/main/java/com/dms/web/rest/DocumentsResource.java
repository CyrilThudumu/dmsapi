
package com.dms.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlProcessor.DocumentMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.codahale.metrics.annotation.Timed;
import com.dms.domain.Authority;
import com.dms.domain.Documents;
import com.dms.domain.Patient;
import com.dms.domain.Practice;
import com.dms.domain.User;
import com.dms.repository.PatientRepository;
import com.dms.repository.PracticeRepository;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.CaseLogService;
import com.dms.service.DoctorService;
import com.dms.service.DocumentsService;
import com.dms.service.PatientQueryService;
import com.dms.service.PatientService;
import com.dms.service.ReportsQueryService;
import com.dms.service.UserService;
import com.dms.service.dto.AttorneyHistoryDocDTO;
import com.dms.service.dto.CaselogDTO;
import com.dms.service.dto.DataDTO;
import com.dms.service.dto.DoctorDTO;
import com.dms.service.dto.DocumentsChildDTO;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientDTO;
import com.dms.service.dto.PatientSearchCriteria;
import com.dms.service.dto.PracticeDTO;
import com.dms.service.dto.UserDTO;
import com.dms.service.impl.DocumentServiceImpl;
import com.dms.service.mapper.DocumentsMapper;
import com.dms.service.mapper.PatientMapper;
import com.dms.service.util.DocumentsUtil;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.errors.InternalServerErrorException;
import com.dms.web.rest.util.HeaderUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


/**
 * REST controller for managing documents.
 */
@RestController
@RequestMapping("/api")
public class DocumentsResource {

    private final Logger log = LoggerFactory.getLogger(DocumentsResource.class);

    private static final String ENTITY_NAME = "documents";

    @Autowired
    private DocumentsService documentsService;
    
    @Autowired
    private DocumentsMapper documentsMapper;
    
    @Autowired
    private DoctorService doctorService;
    
   // @Autowired
  //  private HttpServletResponse response;

    @Autowired
    private PatientService patientService;
    /*@Autowired
    private DocumentsUtil documentsUtil;*/
    @Autowired
    private ReportsQueryService reportqueryservice;
    
   // @Autowired
   // private CaseLogService caselogservice;
    
    @Autowired
    private PatientQueryService patientQueryService;
    
    @Autowired
    private PatientRepository patientRepository;
    
    @Autowired
    private  UserService userService;
    
    @Autowired
    private  PracticeRepository practiceRepository;
    
	/**
     * POST  /documents : Add documents.
     *
     * @param documentsDTO the documentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentsDTO, or with status 400 (Bad Request) if the documents isn't existss
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documents")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.USER+ "\")" + " || hasRole(\""+AuthoritiesConstants.USER_ADMIN+ "\")")
    public ResponseEntity<ArrayList<String>> createDocuments(@RequestParam("File") final MultipartFile file,	@RequestParam("FormValue") String JSONArray) throws URISyntaxException {
       // log.debug("REST request to save DocumentsDTO : {}", JSONArray);
        ArrayList<String> result =  null;
        try {
        	 ObjectMapper mapper = new ObjectMapper();
        	 ObjectMapper objectMapper=new ObjectMapper();
        	 objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);    
        	 List<DocumentsChildDTO> myObjects = mapper.readValue(JSONArray , new TypeReference<List<DocumentsChildDTO>>(){});
        	// log.debug("My Objects are:  "+myObjects);
        	// log.debug("My Objects list size is:  "+myObjects.size());
        	 int i=0;
        	 for (DocumentsChildDTO listItems : myObjects) {
        		 Gson gson = new Gson();
        		 
            	 //String jsonObj = gson.toJson(myObjects.get(0));
            	 String jsonObj = gson.toJson(listItems);
            	// System.out.println("Json Objects: " + jsonObj);
            	 DocumentsChildDTO documentsChildDTO = mapper.readValue(jsonObj, DocumentsChildDTO.class);
            	 if (documentsChildDTO.getId() != null) {
                     throw new BadRequestAlertException("A new document cannot already have an ID", ENTITY_NAME, "idexists");
                 }
            	// Optional<DoctorDTO> doctorDTO = doctorService.findOne(documentsChildDTO.getId());
            	// documentsChildDTO.setDoctorCode(doctorDTO.get().getDoctorCode());
            /*	 log.debug("Doctor Code is: "+documentsChildDTO.getDoctorCode());
            	 log.debug("DocumentTypes... "+documentsChildDTO.getDocumentTypes());
            	 log.debug("orig file name:  "+file.getOriginalFilename());
            	 log.debug("get bytes:  "+file.getBytes());
            	 log.debug("pages nums: "+documentsChildDTO.getPageNumber());
            	 log.debug("document types: "+documentsChildDTO.getDocumentTypes());
            	 log.debug("from DOS:  "+documentsChildDTO.getDosFrom());
            	 log.debug("toDOS:  "+documentsChildDTO.getDosTo());
            	 log.debug("Complete documents object:  "+documentsChildDTO);*/
            	 //call split pdf method to split all the files which are coming from UI...
            	 DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
            	 result= documentsUtil.splitToMultiplePDF(file.getOriginalFilename(), file.getBytes(), documentsChildDTO.getPageNumber(), 
            			 						  documentsChildDTO.getDocumentTypes(), documentsChildDTO.getDosFrom(), documentsChildDTO.getDosTo(),
            			 						  documentsChildDTO);
            	//return ResponseEntity.created(new URI("/api/documents/" + result)).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
            	 //result = new ResponseEntity.("File splitting process completed", HttpStatus.OK);  
            	 i++;
            	 
            	 
            	 //select * from documents where attorney_code='RF' and 
            	// log.debug("Iterations completed for this item:  "+listItems);
            	// log.info("total list of iterations:"+i);
			}
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
      //  return result;
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
    }
    
    
    @PutMapping(value = "/documents")
  //  @Transactional
	//@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN +"\")")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.USER+ "\")" + " || hasRole(\""+AuthoritiesConstants.USER_ADMIN+ "\")")
    public void updatePatientDOS(@RequestBody DocumentsDTO documentsDTO,Pageable page) {
    	 log.info("documents DTO is  "+documentsDTO );
    	 if(documentsDTO.getAttorneyCode()!=null) {
    	Documents previousAssignedAttorneyDatesExist=documentsService.previousAssignedAttorneyDates(documentsDTO);
    	log.info("outside docWithAttorneyExist@@@@@ " +"docWithAttorneyExist  is  "+previousAssignedAttorneyDatesExist);
    	if( previousAssignedAttorneyDatesExist!= null) {
			log.info("inside docWithAttorneyExist@@@@@ " +"docWithAttorneyExist  is  "+previousAssignedAttorneyDatesExist);
			documentsDTO.setAddendumToPriorFlag(true);
    	}
    	 }
    	
        if(documentsDTO.getStatus().equalsIgnoreCase("ARB")){
        	log.debug(" status is ARB.....updating patient table");
        	patientService.updatedAttorneyCodeForPatient(documentsDTO.getAttorneyCode(),documentsDTO.getPatientCode());
        }
        log.debug("...........starting updating  dos ................");
        documentsService.updatePatientDOS(documentsDTO);
        log.debug("...........ended updating patient dos ................");
       // log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

    }

    /**
	 * GET /documents : get patient documents For MDM User.
	 *
	 * @param pageable the pagination information
	 * @param patientCode the patientCode which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
	 *         body
	 */
	// get dos by patient code and practice code
    @GetMapping(value = "/documentsByPat/{patientCode}/{practiceCode}")
    public Page<DocumentsDTO> findByPatientCode(@PathVariable String patientCode,@PathVariable String practiceCode, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", patientCode, practiceCode);
        page= null;
       // System.out.println("patientcode ="+patientCode+" "+"practicecode ="+practiceCode);
       // Page <DocumentsDTO> documentsDTO = documentsService.findByPatientCode(patientCode,page);
       // log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries"+documentsDTO.getContent().size());
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findByPatientCode(patientCode,practiceCode,page));
       // System.out.println("documentDto by patcode and prac code "+documentsDTO.getContent());
        return documentsDTO;
    }
    /**
	 * GET /documents : get patient documents For Doctors.
	 *
	 * @param pageable the pagination information
	 * @param doctorCode the doctorCode which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
	 *         body
	 */
    
    @GetMapping(value = "/documentsByDoc/{doctorCode}/{patientCode}")
    public Page<DocumentsDTO> findByDoctorCode(@PathVariable String doctorCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", doctorCode,patientCode);
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findByDoctorCode(doctorCode,patientCode,page));
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

        return documentsDTO;
    }
    
    /**
   	 * GET /documents : get patient documents For Attorneys.
   	 *
   	 * @param pageable the pagination information
   	 * @param attorneyCode the attorneyCode which the requested entities should match
   	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
   	 *         body
   	 */
    @GetMapping(value = "/documentsByAttorney/{attorneyCode}/{patientCode}")
    public Page<DocumentsDTO> documentsForAttorney(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findByAttorneyCode(attorneyCode,patientCode,page));

        return documentsDTO;
    }
    
    
    /**
   	 * GET /documents : get patient documents For Attorneys.
   	 *
   	 * @param pageable the pagination information
   	 * @param attorneyCode the attorneyCode which the requested entities should match
   	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
   	 *         body
   	 */
  
    // new documents
    @GetMapping(value = "/documentsByAttorneyNotProcessed/{attorneyCode}/{patientCode}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY + "\")")
    public Page<DocumentsDTO> documentsByAttorneyNotProcessed(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        String status="ARB";
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
       // Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.getAttorneyNotProcessedDOS(attorneyCode,patientCode, page));
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.getAttorneyNotProcessedDOS(attorneyCode,patientCode, status, page));
        return documentsDTO;
    }
    
    // processed documents
    @GetMapping(value = "/documentsByAttorneyProcessed/{attorneyCode}/{patientCode}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY + "\")")
    public Page<DocumentsDTO> documentsForAttorneyProcessed(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        String status="ARB";
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
       // Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.getAttorneyProcessedDOS(attorneyCode,patientCode, page));
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.getAttorneyProcessedDOS(attorneyCode,patientCode, status, page));
        return documentsDTO;
    }
    
    // addendum to prior documents
    @GetMapping(value = "/documentsByAttorneyAddendumToPrior/{attorneyCode}/{patientCode}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY + "\")")
    public Page<DocumentsDTO> findAttorneysAddendumToPriorDOS(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        String status="ARB";
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
       // Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findAttorneysAddendumToPriorDOS(attorneyCode,patientCode, page));
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findAttorneysAddendumToPriorDOS(attorneyCode,patientCode, status, page));
        return documentsDTO;
    }
    
    
    @GetMapping(value = "/attorneySearchDos/{attorneyCode}/{patientCode}")
    public Page<DocumentsDTO> findAttorneySearchDos(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findAttorneySearchDos(attorneyCode,patientCode,page));

        return documentsDTO;
    }
    
   /* @GetMapping(value = "/documentsByAttorney/{attorneyCode}/{practiceCode}")
    public Page<DocumentsDTO> findByAttorneyCode(@PathVariable String attorneyCode,@PathVariable String practiceCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,practiceCode);
        Page <DocumentsDTO> documentsDTO = documentsService.findByAttorneyCode(attorneyCode,practiceCode,page);
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

        return documentsDTO;
    }*/
   
   

    
    @PutMapping(value = "/documentsAttorneyProcessed")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY + "\")")
    public void updatePatientDOSToProcessed(@RequestBody DocumentsDTO documentsDTO) {
        log.info("Updating Patient DOS to processed {} and page request: {}", documentsDTO);

        documentsService.updatePatientDOSToProcessed(documentsDTO);
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

    }
    
    /**
   	 * GET /documentsload : get patient documents For MDM User.
   	 *
   	 * @param patientCode the patientCode which the requested entities should match
   	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
   	 *         body
   	 */
   	
    	@GetMapping(value = "/documentsload/{patientCode}/{practiceCode}/{fromDOS}/{toDOS}")
    	public ResponseEntity<byte[]> downloadPatientDocs(@PathVariable String patientCode, @PathVariable String practiceCode,@PathVariable String fromDOS,@PathVariable String toDOS, Pageable page) {
		log.info("Finding Docoument entries by search term: {} and page request: {}",patientCode);
		Page<DocumentsDTO> documentsdto = (documentsService.downloadPatientDocs(patientCode,practiceCode,fromDOS,toDOS, page));
		DocumentsUtil docsUtil = new DocumentsUtil(documentsService);
		String path = "D:\\"+documentsdto.getContent().get(0).getPracticeCode()+"\\"+documentsdto.
				getContent().get(0).getPatientCode() + "\\"+ documentsdto.getContent().get(0).getFromDos() + "-" + documentsdto.getContent().get(0).getToDos();
		log.info("Path to download is: : " + path);
//		 String path = getServletContext().getRealPath("data");

         File directory = new File(path);
         String[] files = directory.list();
        // log.info("files Size:: "+files.length);
		byte[] zippingDir = null;
		ResponseEntity<byte[]> response = null;
		if (files != null && files.length > 0) {
			try {
					//log.debug("before zippingdir"+directory);
					zippingDir = docsUtil.zipPatientDocs(directory, files);
				//	log.debug("retuned bytearray: "+zippingDir);
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.parseMediaType("application/zip"));
					String outputFilename = documentsdto.getContent().get(0).getPatientCode();
					//log.debug("fileName: "+outputFilename);
					headers.setContentDispositionFormData(outputFilename,outputFilename);
					headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
					response = new ResponseEntity<>(zippingDir, headers, HttpStatus.OK);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		//log.debug("response returned" +response);
		
		return response;
       }
       
    	  @GetMapping(value = "/reports/{practiceCode}/{status}/{attorneyCode}/{fromDate}/{toDate}")
          public Page<DocumentsDTO> getReports(@PathVariable String practiceCode, @PathVariable String status, @PathVariable String attorneyCode,
        		  @PathVariable()  String fromDate,
        		  @PathVariable () String toDate,Pageable page) {
    		 // LocalDate from_date = LocalDate.parse(fromDate, DateTimeFormatter.ofPattern("yyyy-mm-dd"));
    		 // System.out.println("string date "+fromDate);
    		 // System.out.println("locale date "+from_date);
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,practiceCode);
              if (attorneyCode == null) {
      			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
      		}
              if (practiceCode == null) {
      			throw new BadRequestAlertException("Invalid practice Code Code", ENTITY_NAME, "practiceCode is null");
      		}
              
              if (status == null) {
        			throw new BadRequestAlertException("Invalid status Code", ENTITY_NAME, "statuscode is null");
        		}
              
            //  Page <DocumentsDTO> documentDTO= documentsService.getReportsBySearch(practiceCode, status, attorneyCode, fromDate, toDate, page);
           //   System.out.println("getreports "+documentsService.getReportsBySearch(practiceCode, status, attorneyCode, fromDate, toDate, page).getContent());
           //   return new ResponseEntity<>(documentDTO.getContent(), HttpStatus.OK);
              return documentsService.getReportsBySearch(practiceCode, status, attorneyCode, fromDate, toDate, page);
    	  }
    	
        /**
       	 * GET /attorneyHistoryDocuments : get patient documents For Attorneys.
       	 *
       	 * @param pageable the pagination information
       	 * @param attorneyCode the attorneyCode which the requested entities should match
       	 * @param patientCode the patientCode which the requested entities should match
       	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
       	 *         body
       	 */
        @GetMapping(value = "/attorneyHistoryDocuments/{attorneyCode}/{patientCode}")
        public Page<DocumentsDTO> findDocsHistoryByAttorneyCode(@PathVariable String attorneyCode, @PathVariable String patientCode, Pageable page) {
            log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
            if (attorneyCode == null) {
    			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
    		}
            if (patientCode == null) {
    			throw new BadRequestAlertException("Invalid patient Code Code", ENTITY_NAME, "patientCode is null");
    		}
         
            DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
            Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findDocsHistoryByAttorneyCode(attorneyCode,patientCode,page));

            return documentsDTO;
        }
        
        
        
        /**
    	 * GET /attorneyPatientsHistorySearch/:attorneyCode : GET the "attorneyCode" patient.
    	 *
    	 * @param id the attorneyCode of the patientDTO to delete
    	 * @return the ResponseEntity with status 200 (OK)
    	 */
    	/*@GetMapping("/attorneyPatientsHistorySearch2")
    	@Timed
    	public ResponseEntity<List<DocumentsDTO>> searchAttorneyPatientHistory(@RequestBody DocumentsDTO documentsDTO, Pageable page) {
    		log.debug("REST request to get Attorney Patients: {}",documentsDTO);
    		if (documentsDTO == null) {
    			throw new BadRequestAlertException("Invalid Params", ENTITY_NAME, "Given Params are null");
    		}
            DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
    		Page<DocumentsDTO> documentDTO = documentsUtil.getDOSFileContent(documentsService.searchAttorneyPatientHistory(documentsDTO, page));
    		return new ResponseEntity<>(documentDTO.getContent(), HttpStatus.OK);
    	}*/
    	
        
    	@GetMapping("/attorneyPatientsHistorySearch/{attorneyCode}/{processedFrom}/{processedTo}")
    	@Timed
    	public ResponseEntity<List<DocumentsDTO>> searchAttorneyPatientHistory(@PathVariable String attorneyCode, @PathVariable String processedFrom, @PathVariable String processedTo, Pageable page) {
    		log.debug("REST request to get Attorney Patients: {}", attorneyCode,processedFrom,processedTo);
    		if (attorneyCode == null) {
    			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
    		}
    		if (processedFrom == null) {
    			throw new BadRequestAlertException("From Date should not be null  ", ENTITY_NAME, "processedFrom is null");
    		}
    		if (processedTo == null) {
    			throw new BadRequestAlertException("To Date should not be null ", ENTITY_NAME, "processedTo is null");
    		}
    		
    		
    		//get documents object by passing attorneyCode and processedFrom and processedTo params
    		//get patient object by passing attorneyCode and patientCode params
    		// add these object to a list
    		
    		ArrayList<AttorneyHistoryDocDTO> docTypeObj = new ArrayList<>();  
    		Page<DocumentsDTO> documentDTO = documentsService.getDocumentsForHistorySearch(attorneyCode,processedFrom,processedTo, page);
    		List<DocumentsDTO> documentsDtoList = new ArrayList<DocumentsDTO>();
    		for(DocumentsDTO dto: documentDTO){
    			AttorneyHistoryDocDTO attorneyHistoryDTO = new AttorneyHistoryDocDTO();
    			
    			if(documentsDtoList.isEmpty()){
    				documentsDtoList.add(dto);
    			}else{
    				for(DocumentsDTO iterateExistingDTO: documentsDtoList ){
    					if((iterateExistingDTO.getFromDos().equalsIgnoreCase(dto.getFromDos()))&&(iterateExistingDTO.getToDos().equalsIgnoreCase(dto.getToDos()))){
    						documentsDtoList.add(iterateExistingDTO);
    					}else{
    						AttorneyHistoryDocDTO attorneyHistoryDTO2 = new AttorneyHistoryDocDTO();
    					}
    				}
    			}
    		//	log.debug("Patient Code: "+dto.getPatientCode());
    		//	log.debug("From & To DOS are"+dto.getFromDos()+" "+dto.getToDos());
    			Optional<PatientDTO> patientDTO = patientService.getPatientByCode(dto.getPatientCode());
    			documentsDtoList.add(dto);
    			attorneyHistoryDTO.setDocumentsDTO(documentsDtoList);
    		//	log.debug("patientDTO Obj: "+patientDTO.get());
    			attorneyHistoryDTO.setPatientDTO(patientDTO.get());
    			docTypeObj.add(attorneyHistoryDTO);
    		//	log.debug("final object is: "+docTypeObj);
    			
    		}
    		//log.debug("size : "+documentDTO.getContent().get(1));
    		
    		
    		//patientService.getAttorneyPatientHistory(attorneyCode, documentDTO2.getContent(),page);
    		
    		
    		
    		// DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);

     		//Page<DocumentsDTO> documentDTO = documentsUtil.getDOSFileContent(documentsService.searchAttorneyPatientHistory(attorneyCode,fromDOS,toDOS, page));
     		//Page<DocumentsDTO> documentDTO = documentsService.searchAttorneyPatientHistory(attorneyCode,processedFrom,processedTo, page);
    		return new ResponseEntity<>(documentDTO.getContent(), HttpStatus.OK);
    	}
        
    	 /**
    	 * POST /deleteDocs/ deletes the file from local disk and doc related record from DB
    	 *
    	 * @param id the id of the DocumentDTO to delete
    	 * @return the ResponseEntity with status 200 (OK)
    	 */
    	 @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.USER_ADMIN+ "\")")
    	@PostMapping("/deleteDocs")
    	@Timed
    	public ResponseEntity<Void> deleteDocument(@RequestBody JsonNode params) {
    		Long id = Long.parseLong(params.get("value").toString());
    		String path = params.get("path").toString();
    		//log.debug("REST request to delete Document : {}",id, path);
    		documentsService.delete(id);
    		DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
    		documentsUtil.deleteDocumentType(path);
    		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    	}
    	
    	
    	/*@Bean
    	public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
    	    StrictHttpFirewall firewall = new StrictHttpFirewall();
    	    firewall.setAllowUrlEncodedSlash(true);    
    	    return firewall;
    	}*/
    	
    	@Bean
    	public HttpFirewall defaultHttpFirewall() {
    	    return new DefaultHttpFirewall();
    	}
    	
    	
    	  @PutMapping(value = "/updateCaseIdForDOS")
    		//@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN +"\")")
    	    public void updateCaseIdForDOS(@RequestBody DocumentsDTO documentsDTO,Pageable page) {
    	        log.info("Updating CaseId for DOS", documentsDTO);

    	        documentsService.updateCaseIdForDOS(documentsDTO);
    	        log.info("CaseId '"+""+"' updated for the DOS"+documentsDTO.fromDos+" - "+documentsDTO.toDos);

    	    }
    	  
    	  
    	/*  @PostMapping("/caselog")
    	    @Timed
    	    public ResponseEntity<CaselogDTO> createCaseLog(@Valid @RequestBody CaselogDTO caselogDTO) throws URISyntaxException {
    	        log.debug("REST request to save case log : {}", caselogDTO);
    	        if (caselogDTO.getId() != null) {
    	            throw new BadRequestAlertException("A new caselog cannot already have an ID", "caselog", "idexists");
    	        }
    	        CaselogDTO result = caselogservice.save(caselogDTO);
    	        return ResponseEntity.created(new URI("/api/caselog/" + result.getId()))
    	            .headers(HeaderUtil.createEntityCreationAlert("caselog.created", result.getId().toString()))
    	            .body(result);
    	    }
    	  
    	  @PostMapping("/caselogsearch")
  	    @ResponseBody
  	    public List<Documents> searchCaseLog(@RequestBody Documents documents){
  	    //	log.debug("search object is: "+documents);
  	    	return reportqueryservice.searchReport(documents);
  	    }	  
  	  */
    	  
    	  
    	  @GetMapping("/doctorsPatientsByPrac/{practiceCode}")
    	    @Timed
    	    public DataDTO getAllDoctorsAndPatientsForPractice(@PathVariable("practiceCode") String practiceCode, Pageable pageable) {
    	        log.debug("REST request to get Doctors and Patients for Practice : {}", practiceCode);
    	        DataDTO dataDTO = new DataDTO();
    	        Page<DoctorDTO> docList = doctorService.findAllByPractice(practiceCode, pageable);
    	        Page<PatientDTO> patientList = patientService.findAllByPractice(practiceCode, pageable);
    	        dataDTO.setDoctorList(docList);
    	        dataDTO.setPatientList(patientList);
    	        
    	       // HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/doctors");
    	       // return ResponseEntity.ok().headers(headers).body(page.getContent());
    	        return dataDTO;
    	    }

    	  
    	  @PostMapping("/searchReports")
    	    @ResponseBody
    	    public List<Documents> searchReports(@RequestBody Documents documents){
    	    	return reportqueryservice.searchReport(documents);
    	    }	
    	  
    	  @PostMapping("/searchReportsForMasterAttorney")
  	    @ResponseBody
  	  @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY_MASTER_ADMIN + "\")")
  	    public List<Documents> searchReportsForMasterAttorney(@RequestBody Documents documents){
    		  List<String> attorneyCodes = Arrays.asList(documents.getAttorneyCode().split("~"));
    		 // documents.
    		List<Documents>  docs= attorneyCodes.stream().map(code->{
    			  documents.setAttorneyCode(code);
    			  documents.setStatus("ARB");
    			  return reportqueryservice.searchReport(documents);
    		  }).flatMap(List::stream).collect(Collectors.toList());
  	    	return docs;
  	    }
    	  
    	  
    	  @PostMapping("/searchReportsWithFileContent")
  	      @ResponseBody
  	     public List<DocumentsDTO> searchReportsWithFileContent(@RequestBody Documents documents){
    		  if (documents.getAttorneyCode() == null) {
    				throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
    			}
    	        if (documents.getPatientCode() == null) {
    				throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
    			}
    	       List<Documents> docList= reportqueryservice.searchReport(documents);
    	      List<DocumentsDTO> dto= docList.stream().map(documentsMapper::toDto).collect(Collectors.toList());
    	       // String status="ARB";
    	        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
    	        return  documentsUtil.getDOSFileContents(dto);
  	    	 
  	      }	
    	  
    	  
    	  @PostMapping("/attorneyGlobalSearch")
    	  @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY_MASTER_ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.ATTORNEY+ "\")")
    	    public @ResponseBody List<Patient> attorneyGlobalSearch(@RequestBody PatientSearchCriteria patientSearchCriteria){
    	    	log.debug("search patient object is: {} "+patientSearchCriteria);
    	    	List<Documents> docList = null;
    	    	//Set<User> usrs=userService.getUserWithAuthorities().get().getSubAttorneys();
    	    	Set<String> userRoles=userService.getUserWithAuthorities().get()
    	    			.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet());
    	    	System.out.println("usr is "+userRoles);
    	    	if(userRoles!=null && userRoles.contains("ROLE_ATTORNEY_ADMIN")) {
    	    		Documents doc =new Documents();
    	    		List<String> userCodes=userService.getUserWithAuthorities().get().getSubAttorneys().stream().map(User::getUserCode).collect(Collectors.toList());
    	    		System.out.println("user codes are "+userCodes);
    	    		 docList=userCodes.stream().map(code->{
    	    			doc.setAttorneyCode(code);
    	    			doc.setStatus("ARB");
    	    			if(patientSearchCriteria.isAddendumToPriorFlag()!=null) {
    	    				doc.setAddendumToPriorFlag(patientSearchCriteria.isAddendumToPriorFlag());
    	    	    	}
    	    	    	else {}
    	    	    	if(patientSearchCriteria.isProcessedFlag()!=null) {
    	    	    		doc.setProcessedFlag(patientSearchCriteria.isProcessedFlag());
    	    	    		}
    	    	    	else {}
    	    	    	System.out.println("doc is "+doc);
    	    	    	return reportqueryservice.searchReport(doc);
    	    		}).flatMap(List::stream).collect(Collectors.toList());
    	    		 System.out.println("role attorney admin doc list is "+docList);
    	    	}
    	    	//Set<Documents> documentArray=new HashSet<>();
    	    	else if(userRoles!=null && userRoles.contains("ROLE_ATTORNEY")) {
    	    	          Documents document =new Documents();
    	    	         //System.out.println("User is "+subAttorneys.get().getSubAttorneys());
                	    	//Set<Documents> documentArray=new HashSet<>();
    	    	          document.setAttorneyCode(patientSearchCriteria.getAttorneyCode());
    	    	          document.setStatus("ARB");
    	    	          if(patientSearchCriteria.isAddendumToPriorFlag()!=null) {
    	    	             	document.setAddendumToPriorFlag(patientSearchCriteria.isAddendumToPriorFlag());
    	    	            }
    	    	          else {}
    	    	          if(patientSearchCriteria.isProcessedFlag()!=null) {
    	    	            	document.setProcessedFlag(patientSearchCriteria.isProcessedFlag());
    	    		        }
    	    	          else {}
    	    	           //List<Documents> docList=reportqueryservice.searchReport(document);
    	    	           docList=reportqueryservice.searchReport(document);
    	    	           System.out.println("role attorney  doc list is "+docList);
    	    	       }
    	    	  else {}
    	    	 
    	    	List<String> patientcodes=docList.stream().map(Documents::getPatientCode).distinct().collect(Collectors.toList());
    	    	List<Patient> patients=patientcodes.stream().map(patientRepository::findByPatientCode).collect(Collectors.toList());
    	        System.out.println("doc list patient codes are "+patientcodes);
    	        return filterPatientsBySearchCriteria(patients, patientSearchCriteria);
    	    }
    	  
    	
    	
       	@PostMapping("/doctorGlobalSearch")
        @PreAuthorize("hasRole(\"" + AuthoritiesConstants.DOCTOR + "\")")
  	    public @ResponseBody List<Patient> doctorGlobalSearch(@RequestBody PatientDTO patient){
	    	if(patient.getPracticeCode()!=null) {
	    		List<Patient> patientList=patientQueryService.searchPatient(patient);
	    		return patientList;
	    	}
	    	
	    	 UserDTO userDTO= userService.getUserWithAuthorities()
			            .map(UserDTO::new)
			            .orElseThrow(() -> new InternalServerErrorException("User could not be found"));
	    	 /*userDTO.getPractices().stream().map(practiceString->{
	    		 if(patient.getPracticeCode().equalsIgnoreCase(practiceString)) {
	    		return true; 
	    		 }
	    		 return false;
	    	 });*/
				 List<Practice> practices =new ArrayList<>();
		          userDTO.getPractices().stream()
	               .map(practiceRepository::findOneByPracticeCode)
	               .filter(Optional::isPresent)
	               .map(Optional::get)
	               .forEach(practices::add);
		          
		            List<String> pracCodes=   practices.stream().map(Practice::getPracticeCode).collect(Collectors.toList());
		            List<Patient> accumulatedPatients=	 pracCodes.stream().map(pracCode->{
		            	patient.setPracticeCode(pracCode);
		            	List<Patient> patients=patientQueryService.searchPatient(patient);
			        	  return patients;
			          }).flatMap(List::stream).collect(Collectors.toList());
		        	  System.out.println("doc from search  are "+accumulatedPatients);
		        	  return accumulatedPatients;
	    }
    	
	     List<Patient> filterPatientsBySearchCriteria(List<Patient> patients, PatientSearchCriteria patientSearchCriteria){
		   List<Patient> searchResult = null;
	        if("patientCode".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search type is "+patientSearchCriteria.getSearchType());
	    		
	    		if("contains".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    	     searchResult =	patients.stream().filter(str -> str.getPatientCode().toLowerCase().contains(patientSearchCriteria.getSearchTerm().toLowerCase())).collect(Collectors.toList());
	    		}
	    		
	    		else if("equal".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=	patients.stream().filter(str -> str.getPatientCode().equalsIgnoreCase((patientSearchCriteria.getSearchTerm()))).collect(Collectors.toList());
	    		}
	    		
	    		else {
	    	     searchResult=patients.stream().filter(str -> str.getPatientCode().toLowerCase().startsWith((patientSearchCriteria.getSearchTerm().toLowerCase()))).collect(Collectors.toList());
	    		}
	    		
	    	}
	    	else {}
	        
	        
	        if("firstName".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search type is "+patientSearchCriteria.getSearchType());
	    		
	    		if("contains".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getFirstName().toLowerCase().contains(patientSearchCriteria.getSearchTerm().toLowerCase())).collect(Collectors.toList());
	    		}
	    		
	    		else if("equal".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getFirstName().equalsIgnoreCase((patientSearchCriteria.getSearchTerm()))).collect(Collectors.toList());
	    		}
	    		
	    		else {
	    		 searchResult=patients.stream().filter(str -> str.getFirstName().toLowerCase().startsWith((patientSearchCriteria.getSearchTerm().toLowerCase()))).collect(Collectors.toList());
	    		}
	    		
	    	}
	    	else {}
	        
	        if("lastName".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search type is "+patientSearchCriteria.getSearchType());
	    		
	    		if("contains".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getLastName().toLowerCase().contains(patientSearchCriteria.getSearchTerm().toLowerCase())).collect(Collectors.toList());
	    		}
	    		
	    		else if("equal".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getLastName().equalsIgnoreCase((patientSearchCriteria.getSearchTerm()))).collect(Collectors.toList());
	    		}
	    		
	    		else {
	    		 searchResult=patients.stream().filter(str -> str.getLastName().toLowerCase().startsWith((patientSearchCriteria.getSearchTerm().toLowerCase()))).collect(Collectors.toList());
	    		}
	    		
	    	}
	    	else {}
	        
	        
	        if("primaryInsClaimNo".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search type is "+patientSearchCriteria.getSearchType());
	    		
	    		if("contains".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getPrimaryInsClaimNo().toLowerCase().contains(patientSearchCriteria.getSearchTerm().toLowerCase())).collect(Collectors.toList());
	    		}
	    		
	    		else if("equal".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getPrimaryInsClaimNo().equalsIgnoreCase((patientSearchCriteria.getSearchTerm()))).collect(Collectors.toList());
	    		}
	    		
	    		else {
	    		searchResult=patients.stream().filter(str -> str.getPrimaryInsClaimNo().toLowerCase().startsWith((patientSearchCriteria.getSearchTerm().toLowerCase()))).collect(Collectors.toList());
	    		}
	    		
	    	}
	    	else {}
	        
	        
	        if("primaryInsPolicyNo".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search type is "+patientSearchCriteria.getSearchType());
	    		
	    		if("contains".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getPrimaryInsPolicyNo().toLowerCase().contains(patientSearchCriteria.getSearchTerm().toLowerCase())).collect(Collectors.toList());
	    		}
	    		
	    		else if("equal".equalsIgnoreCase(patientSearchCriteria.getSearchType())) {
	    		 searchResult=patients.stream().filter(str -> str.getPrimaryInsPolicyNo().equalsIgnoreCase((patientSearchCriteria.getSearchTerm()))).collect(Collectors.toList());
	    		}
	    		
	    		else {
	    		 searchResult=patients.stream().filter(str -> str.getPrimaryInsPolicyNo().toLowerCase().startsWith(patientSearchCriteria.getSearchTerm().toLowerCase())).collect(Collectors.toList());
	    		}
	    		
	    	}
	    	else {}
	        
	        
	        if("doa".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search term is "+patientSearchCriteria.getSearchTerm());
	    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
				  //convert String to LocalDate
			    LocalDate localDate = LocalDate.parse(patientSearchCriteria.getDoa(), formatter);
	    		
			     searchResult=patients.stream().filter(str -> str.getDoa().isEqual(localDate)).collect(Collectors.toList());
	    		
	    	}
	    	else {}
	        
	        
	        if("dob".equalsIgnoreCase(patientSearchCriteria.getSearchProperty())) {
	    		System.out.println("search property is "+patientSearchCriteria.getSearchProperty());
	    		System.out.println("search term is "+patientSearchCriteria.getSearchTerm());
	    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
				  //convert String to LocalDate
			    LocalDate localDate = LocalDate.parse(patientSearchCriteria.getDob(), formatter);
	    		
			     searchResult=patients.stream().filter(str -> str.getDob().isEqual(localDate)).collect(Collectors.toList());
	    		
	    	}
	    	else {}
	        
	        return searchResult; 
		  
	  }
		
}
