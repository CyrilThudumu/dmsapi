package com.dms.web.rest;

import io.github.jhipster.web.util.ResponseUtil;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.Valid;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.codahale.metrics.annotation.Timed;
import com.dms.domain.Documents;
import com.dms.domain.Patient;
import com.dms.domain.Practice;
import com.dms.repository.DocumentsRepository;
import com.dms.repository.PatientRepository;
import com.dms.repository.PracticeRepository;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.DocumentsService;
import com.dms.service.PracticeQueryService;
import com.dms.service.PracticeService;
import com.dms.service.dto.DocumentsChildDTO;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientDTO;
import com.dms.service.dto.PracticeCriteria;
import com.dms.service.dto.PracticeDTO;
import com.dms.service.impl.DocumentServiceImpl;
import com.dms.service.mapper.PatientMapper;
import com.dms.service.mapper.PracticeMapper;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

/**
 * REST controller for managing Practice.
 */
@RestController
@RequestMapping("/api")
public class PracticeResource {

	private final Logger log = LoggerFactory.getLogger(PracticeResource.class);

	private static final String ENTITY_NAME = "practice";

	private final PracticeService practiceService;

	private final PracticeQueryService practiceQueryService;
	
	private final DocumentsService documentService;
	
	private final PracticeRepository practiceRepository;
	private final PatientRepository patientRepository;
	
	private final DocumentsRepository documentsRepository;

	public PracticeResource(DocumentsRepository documentsRepository, PatientRepository patientRepository, PracticeRepository practiceRepository, PracticeService practiceService, PracticeQueryService practiceQueryService, DocumentsService documentService) {
		this.practiceService = practiceService;
		this.practiceQueryService = practiceQueryService;
		this.documentService=documentService;
		this.practiceRepository=practiceRepository;
		this.patientRepository=patientRepository;
		this.documentsRepository=documentsRepository;
		
	}

	/**
	 * POST /addpractices : Create a new practice.
	 *
	 * @param practiceDTO the practiceDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         practiceDTO, or with status 400 (Bad Request) if the practice has
	 *         already an ID
	 *   saves a file at C:\Practice\PracticeName\filename
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	    @PostMapping("/addpractices")
	    @Timed
	    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
	    public ResponseEntity<PracticeDTO> addPractice(@RequestParam("file") final MultipartFile file,	@RequestParam("formValue") String practice) throws URISyntaxException {
		  log.debug("REST request to save Practice : {}", practice);
		// ObjectMapper mapper = new ObjectMapper();		
		  
 		    Gson gson = new Gson();
 		    PracticeDTO practiceDTO = gson.fromJson(practice, PracticeDTO.class);

			if (practiceDTO.getId() != null) {
				throw new BadRequestAlertException("A new practice cannot already have an ID", ENTITY_NAME, "idexists");
			}
			PracticeDTO result = practiceService.save(practiceDTO);
			Path path = Paths.get("C:\\Practice\\" + practiceDTO.getPracticeName() + "\\");
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//PDDocument outputPDFDocument = new PDDocument();
			String fileName=file.getOriginalFilename();
			//java.io.File tempFile = new java.io.File(path + "/" + fileName + ".pdf");
			java.io.File tempFile = new java.io.File(path + "/" + fileName);
			log.info("temp file is: "+tempFile);
			try {
				file.transferTo(tempFile);
				//outputPDFDocument.save(tempFile);
				//outputPDFDocument.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return ResponseEntity.created(new URI("/api/practices/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	       // ArrayList<String> result =  null;
	       
	  }
	        	
	/**
	 * POST /practices : Create a new practice.
	 *
	 * @param practiceDTO the practiceDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         practiceDTO, or with status 400 (Bad Request) if the practice has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	
	
	@PostMapping("/practices")
	@Timed
	@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
	@Bean(name = "multipartResolver")
	public ResponseEntity<PracticeDTO> createPractice(@Valid @RequestBody PracticeDTO practiceDTO)
			throws URISyntaxException {
		log.debug("REST request to save Practice : {}", practiceDTO);

		if (practiceDTO.getId() != null) {
			throw new BadRequestAlertException("A new practice cannot already have an ID", ENTITY_NAME, "idexists");
		}
		PracticeDTO result = practiceService.save(practiceDTO);
		return ResponseEntity.created(new URI("/api/practices/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	
	/**
	 * POST /editpractices : Updates an existing practice.
	 *
	 * @param practiceDTO the practiceDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         practiceDTO, or with status 400 (Bad Request) if the practiceDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         practiceDTO couldn't be updated
	 *  if a new file is added existing file is deleted and new file is added at C:\Practice\PracticeName\filename
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	 @PostMapping("/editpractices")
	    @Timed
	    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
	    //public ResponseEntity<PracticeDTO> addPractice(@RequestParam("file") final MultipartFile file,	@RequestParam("formValue") String practice) throws URISyntaxException {
	      //public ResponseEntity<PracticeDTO> editPractice(@RequestParam("file") final MultipartFile file,	@RequestParam("formValue") String practice) throws URISyntaxException {
		 public ResponseEntity<PracticeDTO> editPractice(@RequestParam("file") final MultipartFile file,	@RequestParam("formValue") String practice) throws URISyntaxException {
		 //public @ResponseBody PracticeDTO editPractice(@RequestParam("file") final MultipartFile file,	@RequestParam("formValue") String practice) throws URISyntaxException {
	   // public ResponseEntity<PracticeDTO> editPractice(@Valid @RequestBody PracticeDTO practice) throws URISyntaxException {
	        log.debug("REST request to update Practice : {}", practice);
		    Gson gson = new Gson();
		    PracticeDTO practiceDTO = gson.fromJson(practice, PracticeDTO.class);
		    if (practiceDTO.getId() == null) {
				throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
			}
		    Optional<PracticeDTO> existingPractice = practiceService.findOne(practiceDTO.getId());
		    //Optional<PracticeDTO> existingPractice = practiceService.findOne(practiceDTO.getPracticeCode());
		    String existingFileName= existingPractice.get().getFileName();
		    String existingPracticeName=existingPractice.get().getPracticeName();
		    File existingFile = new File("C:\\Practice\\" + existingPracticeName + "\\"+existingFileName ); 
		    boolean isdeleted=existingFile.delete();
		    System.out.println("deleting file  "+isdeleted);
		    practiceService.delete(practiceDTO.getId());
		    PracticeDTO result = practiceService.save(practiceDTO);
			Path path = Paths.get("C:\\Practice\\" + practiceDTO.getPracticeName() + "\\");
			if (Files.isDirectory(path)) {}
			else {
			try {
				Files.createDirectory(path);
			} catch (IOException e) { 	
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			//PDDocument outputPDFDocument = new PDDocument();
			//String fileName=file.getOriginalFilename();
			//log.info("file name is "+fileName);
			//java.io.File tempFile = new java.io.File(path + "/" + fileName);
			//log.info("temp file is: "+tempFile);
			try {
				String fileName=file.getOriginalFilename();
				log.info("file name is "+fileName);
				java.io.File tempFile = new java.io.File(path + "/" + fileName);
				file.transferTo(tempFile);
				//outputPDFDocument.save(tempFile);
				//outputPDFDocument.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, practiceDTO.getId().toString())).body(result);
	  }
	        	

	/**
	 * PUT /practices : Updates an existing practice.
	 *
	 * @param practiceDTO the practiceDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         practiceDTO, or with status 400 (Bad Request) if the practiceDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         practiceDTO couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/practices")
	@Timed
	@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
	public ResponseEntity<PracticeDTO> updatePractice(@Valid @RequestBody PracticeDTO practiceDTO)
			throws URISyntaxException {
		log.debug("REST request to update Practice : {}", practiceDTO);
		if (practiceDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		PracticeDTO result = practiceService.save(practiceDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, practiceDTO.getId().toString())).body(result);
	}

	/**
	 * GET /practices : get all the practices.
	 *
	 * @param pageable the pagination information
	 * @param criteria the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of practices in
	 *         body
	 */
	@GetMapping("/practices")
	@Timed
	public ResponseEntity<List<PracticeDTO>> getAllPractices(PracticeCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Practices by criteria: {}", criteria);
		Page<PracticeDTO> page = practiceQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/practices");
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * GET /practices/count : count all the practices.
	 *
	 * @param criteria the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the count in body
	 */
	@GetMapping("/practices/count")
	@Timed
	public ResponseEntity<Long> countPractices(PracticeCriteria criteria) {
		log.debug("REST request to count Practices by criteria: {}", criteria);
		return ResponseEntity.ok().body(practiceQueryService.countByCriteria(criteria));
	}

	/**
	 * GET /practices/:id : get the "id" practice.
	 *
	 * @param id the id of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/practices/{id}")
	@Timed
	public @ResponseBody ResponseEntity<PracticeDTO> getPractice(@PathVariable("id") Long id) {
		log.debug("REST request to get Practice : {}", id);
		Optional<PracticeDTO> practiceDTO = practiceService.findOne(id);
		return ResponseUtil.wrapOrNotFound(practiceDTO);
	}
	
	/**
	 * GET /practices/:practiceName : get the "id" practice.
	 *
	 * @param id the id of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	/*@GetMapping("/practice/{practiceName}")
	@Timed
	public @ResponseBody ResponseEntity<PracticeDTO> getPracticCode(@PathVariable("practiceName") String practiceName) {
		log.debug("REST request to get Practice : {}", practiceName);
		Optional<PracticeDTO> practiceDTO = practiceService.findByPracticeName(practiceName);
		return ResponseUtil.wrapOrNotFound(practiceDTO);
	}*/
	
	
	
	/**
	 * GET /practices/:searchKey : get the "searchKey" practice.
	 *
	 * @param searchKey the searchKey of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/practice/{searchKey}")
	@Timed
	public @ResponseBody ResponseEntity<List<PracticeDTO>> getPracticeInfo(@PathVariable("searchKey") String pracName, Pageable page) {
		log.debug("REST request to get Practice Info : {}", pracName);
		Page<PracticeDTO> practiceDTO = practiceService.findByPracticeName(pracName,page);
		return new ResponseEntity<>(practiceDTO.getContent(), HttpStatus.OK);
	}
	

	/**
	 * GET /AttorneyPatientsPractice/:attorneyCode : get the attorney practices and paatients.
	 *
	 * @param attorneyCode the attorneyCode of the documentDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         map of attorney related practices and patients, or with status 404 (Not Found)
	 */
	/*@GetMapping("/AttorneyPatientsPractice/{attorneyCode}")
	@Timed
	public @ResponseBody ResponseEntity<?> getAttorneyPatientPractices(@PathVariable("attorneyCode") String attorneyCode) {
		log.debug("REST request to get AttorneyPatientPractices : {}", attorneyCode);
		
		List<String> practiceCodes = documentService.findDocumentsByAttorneyCode(attorneyCode);
		log.debug("practice  codes are : {}", practiceCodes);
		List<PracticeDTO> practices=practiceCodes.stream()
		.map(practiceRepository::findOneByPracticeCode)
		.filter(Optional::isPresent)
		.map(Optional::get).map(PracticeDTO::new)
		.collect(Collectors.toList());
		
		List<Patient> patients=practiceCodes.stream()
		.map(patientRepository::findPatientsByPracticeCode)
		.flatMap(List::stream)
		.collect(Collectors.toList());
		
		
		Map<String, List<?>> map=new HashMap<>();
		map.put("prac", practices);
		map.put("pat", patients);
		System.out.println(" map contents are "+map);
		  return new ResponseEntity<Map<String, List<?>>>(map, HttpStatus.OK);
	}*/
	
	@PostMapping("/AttorneyPatientsPractice")
	@Timed
	 @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ATTORNEY_MASTER_ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.ATTORNEY+ "\")")
	public @ResponseBody ResponseEntity<?> getAttorneyPatientsAndPractices(@RequestBody  Map<String, List<String>> attorneyCodes) {
		
		//Gson gson = new Gson(); 
		//String json = gson.toJson(attorneyCodes);
		log.debug("REST request to get AttorneyPatientPractices : {}", attorneyCodes.entrySet());
		
		List<String> practiceCodes=attorneyCodes.get("val").stream()
				.map(attorneyCode->documentService.findDocumentsByAttorneyCode(attorneyCode))
				.flatMap(List::stream).distinct()
				.collect(Collectors.toList());
		log.debug("practiceCodes    are : {}", practiceCodes);
		
		List<String> patientCodes=attorneyCodes.get("val").stream()
				.map(attorneyCode->documentService.findPatientCodeByAttorneyCode(attorneyCode))
				.flatMap(List::stream).distinct()
				.collect(Collectors.toList());
		log.debug("patientCodes   are : {}", patientCodes);
	
		List<PracticeDTO> practices=practiceCodes.stream()
		.map(practiceRepository::findOneByPracticeCode)
		.filter(Optional::isPresent)
		.map(Optional::get).map(PracticeDTO::new)
		.collect(Collectors.toList());
		
		List<Patient> patients=patientCodes.stream()
		.map(patientRepository::findOneByPatientCode)
		.filter(Optional::isPresent).map(Optional::get)
		.collect(Collectors.toList());
		
		/*List<String> pCodes=attorneyCodes.get("val").stream()
				.map(attorneyCode->documentsRepository.findDistinctByAttorneyCodeAndStatus(attorneyCode, "ARB"))
				.flatMap(List::stream).map(Documents::getPatientCode)
				.collect(Collectors.toList());
				log.debug("unique pCodes   are : {}", pCodes);*/
		
		Map<String, List<?>> map=new HashMap<>();
		map.put("prac", practices);
		map.put("pat", patients);
		//map.put("pcode", pCodes);
		System.out.println(" map contents are "+map);
		  return new ResponseEntity<Map<String, List<?>>>(map, HttpStatus.OK);
	}
	
	/**
	 * DELETE /practices/:id : delete the "id" practice.
	 *
	 * @param id the id of the practiceDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/practices/{id}")
	@Timed
	@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
	public @ResponseBody ResponseEntity<Void> deletePractice(@PathVariable("id") Long id) {
		log.debug("REST request to delete Practice : {}", id);
		practiceService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
	/**
	 * GET /practicesSearch/:practiceName : get the all details of the practice.
	 *
	 * @param practiceName the practiceName of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	/*@GetMapping("/practicesSearch/{practiceName}")
	@Timed
	public @ResponseBody ResponseEntity<PracticeDTO> getPracticeInfo(@PathVariable("practiceName") String practiceName) {
		log.debug("REST request to get Practice : {}", practiceName);
		Optional<PracticeDTO> practiceDTO = practiceService.findPracticeInfo(practiceName);
		return ResponseUtil.wrapOrNotFound(practiceDTO);
	}*/

}
