package com.dms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.CasetypeService;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.CasetypeCriteria;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.CasetypeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Casetype.
 */
@RestController
@RequestMapping("/api")
public class CasetypeResource {

    private final Logger log = LoggerFactory.getLogger(CasetypeResource.class);

    private static final String ENTITY_NAME = "casetype";

    private final CasetypeService casetypeService;

    private final CasetypeQueryService casetypeQueryService;

    public CasetypeResource(CasetypeService casetypeService, CasetypeQueryService casetypeQueryService) {
        this.casetypeService = casetypeService;
        this.casetypeQueryService = casetypeQueryService;
    }

    /**
     * POST  /casetypes : Create a new casetype.
     *
     * @param casetypeDTO the casetypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new casetypeDTO, or with status 400 (Bad Request) if the casetype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/casetypes")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CasetypeDTO> createCasetype(@Valid @RequestBody CasetypeDTO casetypeDTO) throws URISyntaxException {
        log.debug("REST request to save Casetype : {}", casetypeDTO);
        if (casetypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new casetype cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CasetypeDTO result = casetypeService.save(casetypeDTO);
        return ResponseEntity.created(new URI("/api/casetypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /casetypes : Updates an existing casetype.
     *
     * @param casetypeDTO the casetypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated casetypeDTO,
     * or with status 400 (Bad Request) if the casetypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the casetypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/casetypes")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CasetypeDTO> updateCasetype(@Valid @RequestBody CasetypeDTO casetypeDTO) throws URISyntaxException {
        log.debug("REST request to update Casetype : {}", casetypeDTO);
        if (casetypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CasetypeDTO result = casetypeService.save(casetypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, casetypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /casetypes : get all the casetypes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of casetypes in body
     */
    @GetMapping("/casetypes")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
   // public ResponseEntity<List<CasetypeDTO>> getAllCasetypes(CasetypeCriteria criteria, Pageable pageable) {
    public ResponseEntity<List<CasetypeDTO>> getCasetypes(CasetypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Casetypes by criteria: {}", criteria);
        Page<CasetypeDTO> page = casetypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/casetypes");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    /**
         * GET  /casetypes : get all the casetypes.
        */
       @GetMapping("/casetypes/all")
       @Timed
       @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasRole(\""+AuthoritiesConstants.USER+ "\")" + " || hasRole(\""+AuthoritiesConstants.USER_ADMIN+ "\")")
        public ResponseEntity<List<CasetypeDTO>> getAllCasetypes() {
            log.debug("REST request to get ALL Casetypes");
            List<CasetypeDTO> page = casetypeQueryService.findByCriteria(null);
            HttpHeaders headers = new HttpHeaders();
            return ResponseEntity.ok().headers(headers).body(page);
       }
        

    /**
    * GET  /casetypes/count : count all the casetypes.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/casetypes/count")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Long> countCasetypes(CasetypeCriteria criteria) {
        log.debug("REST request to count Casetypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(casetypeQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /casetypes/:id : get the "id" casetype.
     *
     * @param id the id of the casetypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the casetypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/casetypes/{id}")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CasetypeDTO> getCasetype(@PathVariable Long id) {
        log.debug("REST request to get Casetype : {}", id);
        Optional<CasetypeDTO> casetypeDTO = casetypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(casetypeDTO);
    }

    /**
     * DELETE  /casetypes/:id : delete the "id" casetype.
     *
     * @param id the id of the casetypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/casetypes/{id}")
    @Timed
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteCasetype(@PathVariable Long id) {
        log.debug("REST request to delete Casetype : {}", id);
        casetypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
