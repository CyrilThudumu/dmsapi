package com.dms.web.rest;

import com.codahale.metrics.annotation.Timed;

import com.dms.service.CaseLogService;
import com.dms.service.ReportsQueryService;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.dto.CaselogDTO;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.AttorneyCriteria;
import com.dms.domain.Caselog;
import com.dms.service.AttorneyQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing case log.
 */
@RestController
@RequestMapping("/api")
public class CaseLogResource {

    private final Logger log = LoggerFactory.getLogger(CaseLogResource.class);

    private static final String ENTITY_NAME = "caselog";

    private final CaseLogService caselogService;
    
    private final ReportsQueryService reportQueryService;

    //private final AttorneyQueryService attorneyQueryService;

    public CaseLogResource(CaseLogService caselogService, ReportsQueryService reportQueryService) {
        this.caselogService = caselogService;
        this.reportQueryService=reportQueryService;
        //this.attorneyQueryService = attorneyQueryService;
    }

    /**
     * POST  /caselog : Create a new caselog.
     *
     * @param CaselogDTO the CaselogDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new CaselogDTO, or with status 400 (Bad Request) if the attorney has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/caselog")
    @Timed
    public ResponseEntity<CaselogDTO> createCaseLog(@Valid @RequestBody CaselogDTO caselogDTO) throws URISyntaxException {
        log.debug("REST request to save case log : {}", caselogDTO);
        if (caselogDTO.getId() != null) {
            throw new BadRequestAlertException("A new caselog cannot already have an ID", "caselog", "idexists");
        }
        CaselogDTO result = caselogService.save(caselogDTO);
        return ResponseEntity.created(new URI("/api/caselog/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
  
   /* @GetMapping(value = "/reports/{patientCode}/{caseId}/{attorneyCode}/{fromDate}/{toDate}")
    public Page<DocumentsDTO> getReports(@PathVariable String patientCode, @PathVariable String caseId, @PathVariable String attorneyCode,
  		  @PathVariable()  String fromDate,
  		  @PathVariable () String toDate,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid patientCode  Code", ENTITY_NAME, "practiceCode is null");
		}
        
        if (caseId == null) {
  			throw new BadRequestAlertException("Invalid caseId Code", ENTITY_NAME, "statuscode is null");
  		}
        
        return documentsService.getReportsBySearch(practiceCode, status, attorneyCode, fromDate, toDate, page);
	  }*/
  @PostMapping("/caselogsearch")
  @ResponseBody
  public List<Caselog> searchCaseLog(@RequestBody Caselog caselog){
  //	log.debug("search object is: "+documents);
  	return reportQueryService.searchcaselog(caselog);
  }	 

    /**
     * PUT  /attorneys : Updates an existing attorney.
     *
     * @param attorneyDTO the attorneyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated attorneyDTO,
     * or with status 400 (Bad Request) if the attorneyDTO is not valid,
     * or with status 500 (Internal Server Error) if the attorneyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
   /* @PutMapping("/attorneys")
    @Timed
    public ResponseEntity<AttorneyDTO> updateAttorney(@Valid @RequestBody AttorneyDTO attorneyDTO) throws URISyntaxException {
        log.debug("REST request to update Attorney : {}", attorneyDTO);
        if (attorneyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AttorneyDTO result = attorneyService.save(attorneyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attorneyDTO.getId().toString()))
            .body(result);
    }*/

    /**
     * GET  /attorneys : get all the attorneys.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of attorneys in body
     */
  /*  @GetMapping("/attorneys")
    @Timed
    public ResponseEntity<List<AttorneyDTO>> getAllAttorneys(AttorneyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Attorneys by criteria: {}", criteria);
        Page<AttorneyDTO> page = attorneyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/attorneys");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
*/
    /**
    * GET  /attorneys/count : count all the attorneys.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
   /* @GetMapping("/attorneys/count")
    @Timed
    public ResponseEntity<Long> countAttorneys(AttorneyCriteria criteria) {
        log.debug("REST request to count Attorneys by criteria: {}", criteria);
        return ResponseEntity.ok().body(attorneyQueryService.countByCriteria(criteria));
    }
*/
    /**
     * GET  /attorneys/:id : get the "id" attorney.
     *
     * @param id the id of the attorneyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the attorneyDTO, or with status 404 (Not Found)
     */
  /*  @GetMapping("/attorneys/{id}")
    @Timed
    public ResponseEntity<AttorneyDTO> getAttorney(@PathVariable Long id) {
        log.debug("REST request to get Attorney : {}", id);
        Optional<AttorneyDTO> attorneyDTO = attorneyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(attorneyDTO);
    }
*/
    /**
     * DELETE  /attorneys/:id : delete the "id" attorney.
     *
     * @param id the id of the attorneyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
   /* @DeleteMapping("/attorneys/{id}")
    @Timed
    public ResponseEntity<Void> deleteAttorney(@PathVariable Long id) {
        log.debug("REST request to delete Attorney : {}", id);
        attorneyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
