package com.dms.repository;

import com.dms.domain.Casetype;
import com.dms.domain.Certifier;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Certifier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CertifierRepository extends JpaRepository<Certifier, Long>, JpaSpecificationExecutor<Certifier> {

}
