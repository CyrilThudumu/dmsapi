package com.dms.repository;

import com.dms.domain.Edocument;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Edocument entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EdocumentRepository extends JpaRepository<Edocument, Long>, JpaSpecificationExecutor<Edocument> {

}
