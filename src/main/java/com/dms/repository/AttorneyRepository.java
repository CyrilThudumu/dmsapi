package com.dms.repository;

import com.dms.domain.Attorney;
import com.dms.domain.Doctor;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Attorney entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AttorneyRepository extends JpaRepository<Attorney, Long>, JpaSpecificationExecutor<Attorney> {

	
	
	@Query(value = "select attorney from Attorney attorney where attorney.attornyCode =:attorneyCode")
    Attorney findOneByAttorneyCode(@Param("attorneyCode") String attorneyCode);
}
