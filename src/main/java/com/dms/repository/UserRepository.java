
package com.dms.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dms.domain.Authority;
import com.dms.domain.User;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.dto.UserDTO;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    String USERS_BY_LOGIN_CACHE = "usersByLogin";

    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByLogin(String login);
    
    Optional<User> findOneByUserCode(String userCode);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesById(Long id);

    
    		
    @EntityGraph(attributePaths = {"authorities", "practices", "subAttorneys"})
    @Cacheable(cacheNames = USERS_BY_LOGIN_CACHE)
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    @Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
   // @Query("SELECT user FROM User user LEFT OUTER JOIN FETCH user.authorities WHERE user.email = :email")
    Optional<User> findOneWithAuthoritiesByEmail(String email);
    
   /* @EntityGraph(attributePaths = "authorities")
    @Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
    Optional<User> findOneWithAuthoritiesByEmail(String email);*/
    
    Page<User> findOneWithAuthoritiesByEmail(String email,Pageable pageable);

    Page<User> findAllByLoginNot(Pageable pageable, String login);
    
    @Query("select user from User user where user.firstName like ?1% or user.lastName like ?1%")
    Page<User> findByUserName(String practiceName, Pageable page); 
    
    Page<User> findAllByAuthorities(Authority role, Pageable page);
    
    List<UserDTO> findAllByAuthorities(Authority role);
   // @Query("SELECT u FROM user u LEFT JOIN FETCH u.practices")
     //   List<User> getUserWithPractices();
    
    //@Query("select  u from User u where u.userCode =:userCode")
   // Optional<User> findOneWithUserCode(@Param("userCode") String userCode);
   Optional<User> findByUserCode(String userCode);
}
