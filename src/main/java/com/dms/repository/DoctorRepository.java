package com.dms.repository;

import com.dms.domain.Doctor;
import com.dms.domain.Documents;
import com.dms.domain.Patient;
import com.dms.service.dto.DoctorDTO;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Doctor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long>, JpaSpecificationExecutor<Doctor> {

	
	 /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param doctorCode  The given patientCode.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
     @Query("select doctor from Doctor doctor where doctor.practice =:practiceCode")
     Page<Doctor> findAll(@Param("practiceCode") String practiceCode, Pageable page);
    
    
     @Query(value = "select doctor from Doctor doctor where doctor.doctorCode =:doctorCode")
     DoctorDTO findByDoctorCode(@Param("doctorCode") String doctorCode);
    
     @Query(value = "select doctor from Doctor doctor where doctor.doctorCode =:doctorCode")
     Doctor findByDocCode(@Param("doctorCode") String doctorCode);
}
