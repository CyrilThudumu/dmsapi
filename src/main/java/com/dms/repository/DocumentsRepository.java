package com.dms.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dms.domain.Documents;

/**
 * Spring Data  repository for the Documents entity.
 */
@Repository
public interface DocumentsRepository extends JpaRepository<Documents, Long>, JpaSpecificationExecutor<Documents> {

	
	
	/**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param doctorCode  The given doctorCode.
     * @param pageRequest   The information of the requested page.
     * @return  A page of Documents entries when the doctorCode matches . The content of
     *          the returned page depends from the page request given as a method parameter.
     */
	
	 
    @Query("select documents from Documents documents  where documents.doctorCode =:doctorCode")
    Page<Documents> findByDocumentName(@Param("doctorCode") String doctorCode, Pageable page);
    
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param patientCode  The given patientCode.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    //display patient dos admin
    @Query("select documents from Documents documents  where documents.patientCode =:patientCode and documents.practiceCode = :practiceCode")
    Page<Documents> getDOSForPatient(@Param("patientCode") String patientCode,@Param("practiceCode") String practiceCode, Pageable page);
    
    
    @Query(value="select * from documents where patient_code=:patientCode and from_dos=:fromDos and to_dos=:toDos and practice_code=:practiceCode and attorney_code is not null limit 1", nativeQuery=true)
    Documents getAttorneyForDos(@Param("patientCode") String patientCode,@Param("practiceCode") String practiceCode, @Param("fromDos") String fromDos, @Param("toDos") String toDos/* @Param("attorneyCode") String attorneyCode*/ );
    
  //  @Query(value="select * from documents where patient_code=:patientCode  and practice_code=:practiceCode and attorney_code is not null limit 1", nativeQuery=true)
 //   Documents getAttorneyForDos(@Param("patientCode") String patientCode, @Param("practiceCode") String practiceCode/* @Param("attorneyCode") String attorneyCode*/ );
    /*     */
   @Query("select distinct documents.practiceCode from Documents documents  where documents.attorneyCode =:attorneyCode and documents.status =:status")
    List<String> findDocumentsByAttorneyCode(@Param("attorneyCode") String attorneyCode, @Param("status") String status);
   
   @Query("select distinct documents.patientCode from Documents documents  where documents.attorneyCode =:attorneyCode and documents.status =:status")
   List<String> findPatientCodeByAttorneyCode(@Param("attorneyCode") String attorneyCode, @Param("status") String status);
   
  // @Query("select distinct documents.patientCode from Documents documents  where documents.attorneyCode =:attorneyCode")
  // List<String> findDistinctPatients(@Param("attorneyCode") String attorneyCode);
   
  // List<Documents> findDistinctByPracticeCodeAndPatientCode(String attorneyCode);
   
  //  @Query("select distinct documents from Documents documents  where documents.attorneyCode =:attorneyCode")
   // List<Documents> findDocumentsByAttorneyCode(@Param("attorneyCode") String attorneyCode);
    List<Documents> findDistinctByAttorneyCodeAndStatus(String attorneyCode, String status);
   /*     */
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param doctorCode  The given patientCode.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.doctorCode =:doctorCode and documents.patientCode = :patientCode")
    Page<Documents> getDOSForDoctor(@Param("doctorCode") String doctorCode,@Param("patientCode") String practiceCode, Pageable page);
   
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param attorneyCode  The given patientCode.
     * @param fromDos   The information of the requested page.
     * @param toDos   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode ")
    Page<Documents> getDOSForAttorney(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, Pageable page);
   
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param attorneyCode  The given patientCode.
     * @param fromDos   The information of the requested page.
     * @param toDos   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    
    //new documents
  //  @Query("select distinct documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = FALSE and documents.addendumToPriorFlag = FALSE and documents.status= 'ARB'")
  //  Page<Documents> findAttorneyNotProcessedDOS(@Param("attorneyCode") String attorneyCode, @Param("patientCode") String patientCode, Pageable page);
    
    @Query("select distinct documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = FALSE and documents.addendumToPriorFlag = FALSE and documents.status = :status")
    Page<Documents> findAttorneyNotProcessedDOS(@Param("attorneyCode") String attorneyCode, @Param("patientCode") String patientCode, @Param("status") String status, Pageable page);
    
    // processed documents
    //  @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = TRUE and documents.status= 'ARB'")
    //  Page<Documents> findAttorneyProcessedDOS(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, Pageable page);
    
       @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = TRUE and documents.status = :status")
       Page<Documents> findAttorneyProcessedDOS(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, @Param("status")String status, Pageable page);
    
    // addendum to prior documents
    @Query("select distinct documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = FALSE and documents.addendumToPriorFlag=TRUE and documents.status = :status")
    Page<Documents> findAttorneysAddendumToPriorDOS(@Param("attorneyCode") String attorneyCode, @Param("patientCode") String patientCode, @Param("status")String status,  Pageable page);
    
    
    @Query("select distinct documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode")
    Page<Documents> findAttorneySearchDos(@Param("attorneyCode") String attorneyCode, @Param("patientCode") String patientCode, Pageable page);
    
    
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param patientCode  The given patientCode.
     * @param fromDos   The information of the requested page.
     * @param toDos   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
	    /*@Query("select documents from Documents documents  where documents.patientCode =:patientCode AND documents.fromDos = :fromDos AND documents.toDos = :toDos")
	    Page<Documents> findByPatientCode(@Param("patientCode") String patientCode, @Param("fromDos") String fromDos, @Param("toDos") String toDos, Pageable page);  , Boolean addendumToPriorFlag  AND documents.addendumToPriorFlag= ?13
	    */
	    
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Documents documents SET documents.status = ?1, documents.statusDate = ?2, documents.attorneyAssignedDate= ?3, documents.addendumToPriorFlag= ?4,  documents.arbAmount = ?5, documents.sentDate = ?6,"
    		+ "documents.paymentAmount = ?7, documents.paymentDate = ?8, documents.arbNotes = ?9, documents.natureOfDispute = ?10, "
    		+ "documents.attorneyCode = ?11 WHERE documents.patientCode = ?12 AND documents.fromDos = ?13 AND documents.toDos = ?14")
    public void updatePatientDOS(String status, String statusDate, String attorneyAssignedDate,  Boolean addendumToPriorFlag, String arbAmount,String sentDate,String paymentAmount, 
    		String paymentDate, String arbNotes,String natureOfDispute,String attorneyCode, String patientCode, String fromDos, String toDos);
    
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Documents documents SET documents.caseId = ?1, documents.caseComments = ?2 WHERE documents.patientCode = ?3 AND documents.fromDos = ?4 AND documents.toDos = ?5")
    public void updateCaseIdForDOS(String caseId,String caseComments, String patientCode, String fromDOS, String toDOS);
    
   
   
    @Modifying
    @Query("update Documents documents SET documents.processedFlag = ?1, documents.processedDate = ?2 WHERE documents.attorneyCode = ?3 AND documents.fromDos = ?4 and documents.toDos = ?5")
    public void updatePatientDOSToProcessed(boolean processedFlag, String processedDate, String attorneyCode, String fromDOS, String toDOS);
	
    

    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param patientCode  The given patientCode.
     * @param practiceCode  The given practiceCode.
     * @param FromDOS  The given FromDOS
     * @param ToDOS  The given ToDOS
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    
    @Query("select documents from Documents documents  where documents.patientCode =:patientCode and documents.practiceCode = :practiceCode and documents.fromDos = :fromDos and documents.toDos = :toDos")
    Page<Documents> getPatientDocsDownload(@Param("patientCode") String patientCode,@Param("practiceCode") String practiceCode,@Param("fromDos") String fromDos,@Param("toDos") String toDos, Pageable page);

    
    @Query("select documents from Documents documents  where documents.practiceCode =:practiceCode and documents.status = :status and documents.attorneyCode = :attorneyCode and documents.fromDos >= :fromDate and documents.toDos <= :toDate")
    Page<Documents> reportsBySearch(@Param("practiceCode") String practiceCode, @Param("status") String status, @Param("attorneyCode") String attorneyCode, @Param("fromDate") String fromDate, @Param("toDate") String toDate, Pageable page);
    
    //@Query("select documents from Documents documents  where documents.practiceCode =:practiceCode and documents.patientCode=:patientCode and documents.attorneyCode = :attorneyCode and documents.assignedAttorneyDate < :assignedAttorneyDate")
    //Page<Documents> previousAssignedAttorneyDates(@Param("practiceCode") String practiceCode, @Param("patientCode") String patientCode, @Param("attorneyCode") String attorneyCode, @Param("assignedAttorneyDate") String assignedAttorneyDate, Pageable page);

    @Query(value="select * from documents where patient_code=:patientCode  and practice_code=:practiceCode and ATTORNEY_ASSIGNED_DATE<:assignedAttorneyDate and attorney_code=:attorneyCode limit 1", nativeQuery=true)
    Documents previousAssignedAttorneyDates(@Param("patientCode") String patientCode, @Param("practiceCode") String practiceCode, @Param("attorneyCode") String attorneyCode, @Param("assignedAttorneyDate") String assignedAttorneyDate);
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param attorneyCode  The given patientCode.
     * @param patientCode   The information of the requested page.
     * @param processedFlag   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = TRUE ")
    Page<Documents> getHistoryDOSForAttorney(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, Pageable page);
   
    
    @Query("select documents from Documents documents  where documents.attorneyCode = ?1 and documents.fromDos = ?2 and documents.toDos = ?3 and documents.processedFlag = TRUE")
    Page<Documents> searchPatientHistoryByAttorney(String attorneyCode, String fromDos,String toDos, Pageable page);
    
    @Query("select documents from Documents documents  where documents.attorneyCode = ?1 and documents.processedDate >= ?2 and documents.processedDate <= ?3 and documents.processedFlag = TRUE")
    Page<Documents> searchPatientHistoryDocs(String attorneyCode, String processedFrom,String processedTo, Pageable page);
    
	    
}
