package com.dms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dms.domain.Reports;

/**
 */
@Repository
public interface ReportsRepository extends JpaRepository<Reports, Long> {

   
	
}
