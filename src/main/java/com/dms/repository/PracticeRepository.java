package com.dms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import com.dms.domain.Practice;
import com.dms.domain.User;

/**
 * Spring Data  repository for the Practice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PracticeRepository extends JpaRepository<Practice, Long>, JpaSpecificationExecutor<Practice> {

    @Query("select practice from Practice practice where practice.user.login = ?#{principal.username}")
    List<Practice> findByUserIsCurrentUser();
    
    @Query("select practice from Practice practice where practice.practiceName like %?1%")
    Page<Practice> findByPracticeName(String practiceName, Pageable page);
    
    public List<Practice> findAllByOrderByPracticeNameDesc(@Nullable Specification<Practice> spec, Pageable pageable);
   
    Optional<Practice> findOneByPracticeCode(String PracticeCode);
    
   // Optional<PracticeDTO> findOneByPracticeCode(String PracticeCode);
    
    Optional<Practice> findById(String id);
}
