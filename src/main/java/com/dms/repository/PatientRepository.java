package com.dms.repository;

import com.dms.domain.Doctor;
import com.dms.domain.Documents;
import com.dms.domain.Patient;
import com.dms.domain.Practice;
import com.dms.domain.User;
import com.dms.service.dto.PatientChildDTO;
import com.dms.service.dto.PatientDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

/**
 * Spring Data  repository for the Patient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientRepository extends JpaRepository<Patient, Long>, JpaSpecificationExecutor<Patient> {

    @Query(value = "select distinct patient from Patient patient left join fetch patient.edocuments",
        countQuery = "select count(distinct patient) from Patient patient")
    Page<Patient> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct patient from Patient patient left join fetch patient.edocuments")
    List<Patient> findAllWithEagerRelationships();

    @Query("select  patient from Patient patient where patient.id =:id")
    Optional<Patient> findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode")
    Page<Patient> getAttorneyByPatients(@Param("attorneyCode") String attorneyCode, Pageable page);
    
    //new patients
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode and documents.processedFlag = FALSE and documents.addendumToPriorFlag = FALSE and documents.status=:status")
    Page<Patient> patientsNotProcessedByAttorney(@Param("attorneyCode") String attorneyCode, @Param("status") String status, Pageable page);
    
    // processed patients
    @Query(value = "select DISTINCT patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode and documents.processedFlag = TRUE and documents.status=:status")
    Page<Patient> patientsProcessedByAttorney(@Param("attorneyCode") String attorneyCode, @Param("status") String status, Pageable page);
    
    // addendum patients
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode and documents.processedFlag=FALSE and documents.addendumToPriorFlag = TRUE and documents.status=:status ")
    Page<Patient> addendumToPriorPatients(@Param("attorneyCode") String attorneyCode,  @Param("status") String status, Pageable page);
         
    
    
 /*   @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = TRUE and ?2 like ?3%")
    Page<Patient> patientsNotProcessedByAttorneySearch( String attorneyCode, String searchOption,  String searchKey, Pageable page);
    
    */
    
   /* @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.patientCode like ?2%")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingPatientCode( String attorneyCode, String searchKey, Pageable page);
    
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.firstName like ?2%")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingFirstName( String attorneyCode, String searchKey, Pageable page);
    
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.lastName like ?2%")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingLastName( String attorneyCode, String searchKey, Pageable page);
    
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.primaryInsClaimNo like ?2%")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingPrimaryInsClaimNo( String attorneyCode, String searchKey, Pageable page);
    
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.primaryInsPolicyNo like ?2%")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingPrimaryInsPolicyNo( String attorneyCode, String searchKey, Pageable page);
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.doa like ?2")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingDoa( String attorneyCode, String searchKey, Pageable page);
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=?1 and documents.processedFlag = FALSE and patient.dob like ?2%")
    Page<Patient> searchPatientsNotProcessedByAttorneyUsingDob( String attorneyCode, String searchKey, Pageable page);
    */
    
    @Query(value = "select DISTINCT  patient from Patient patient where patient.practiceCode=:practiceCode")
    Page<Patient> getPatientsForPractice(@Param("practiceCode") String practiceCode, Pageable page);

    
    @Query(value = "select DISTINCT patient from Patient patient inner join fetch Documents documents on patient.patientCode = documents.patientCode where documents.doctorCode=:doctorCode")
    Page<Patient> getDoctorPatients(@Param("doctorCode") String doctorCode, Pageable page);
    
    @Query(value = "select DISTINCT patient from Patient patient inner join fetch Documents documents on patient.patientCode = documents.patientCode where documents.doctorCode= ?1 and patient.patientCode like ?2%")
    Page<Patient> docPatientSearch(String doctorCode,String searchKey, Pageable page);
    
    @Query(value = "select DISTINCT patient from Patient patient inner join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode= ?1 and patient.patientCode like ?2%")
    Page<Patient> attorneyPatientSearch(String attorneyCode,String searchKey, Pageable page);
    
    
    
    @Query(value = "select patient from Patient patient  where patient.patientCode=:patientCode")
    Optional<Patient> getPatientByCode(@Param("patientCode") String patientCode);

    
    @Query(value = "select  patient from Patient patient  where patient.patientCode=:patientCode")
    Patient findByPatientCode(@Param("patientCode") String patientCode);
    
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode and documents.processedFlag = TRUE") 
    Page<Patient> searchPatientHistoryByAttorney(@Param("attorneyCode") String attorneyCode, Pageable page);
    
  // @Query(value = "select DISTINCT patient.patientCode, patient.firstName,patient.lastName,patient.sex,patient.mobileNo,patient.practiceCode, patient.doa  from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode")
  // Optional<Patient>getPatientByPatientCode(@Param("attorneyCode") String attorneyCode);
   // or
  //  Optional<Practice> findOneByPatientCode(String PatientCode);
    
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param doctorCode  The given patientCode.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    
    //@Query("select patient from Patient patient where patient.practiceCode IN (:practiceCode)")
    @Query("select patient from Patient patient where patient.practiceCode = :practiceCode")
    Page<Patient> findAll(@Param("practiceCode") String practiceCode, Pageable page);
    
    
    @Query("select patient from Patient patient where patient.practiceCode IN (:practiceCode)")
    Page<Patient> findAll(@Param("practiceCode") List<String> practiceCode, Pageable page);

    @Query("select patient from Patient patient where patient.practiceCode = :practiceCode")
    List<Patient> findPatientsByPracticeCode(@Param("practiceCode") String practiceCode);
    
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Patient patient SET patient.attorneyCode = ?1  where patient.patientCode = ?2")
	void updateAttorneyCodeForPatient(String attorneyCode, String patientCode);

	
	
	 Optional<Patient> findOneByPatientCode(String patientCode);
	/* @Query("select patient from Patient patient where patient.practiceCode = :practiceCode and patient.attorneyCode = :attorneyCode")
	 List<Patient> findPatients(@Param("practiceCode") String practiceCode, @Param("attorneyCode") String attorneyCode);*/
    
    
}
