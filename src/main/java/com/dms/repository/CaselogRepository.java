package com.dms.repository;

import com.dms.domain.Caselog;
import com.dms.domain.Documents;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


@SuppressWarnings("unused")
@Repository
public interface CaselogRepository extends JpaRepository<Caselog, Long>, JpaSpecificationExecutor<Caselog> {
	
	 @Query("select caselog from Caselog caselog  where caselog.caseId =?1 and caselog.patientCode =?2 and caselog.fromDos =?3 and caselog.toDos =?4")
	    Page<Caselog> findCaseLogs(String caseId, String patientCode, String fromDOS, String toDOS, Pageable page);
	   

}
