package com.dms.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Attorney.
 */
@Entity
@Table(name = "reports")
public class Reports implements Serializable {

	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Size(max = 10)
    @Column(name = "patientCode", length = 10)
    private String patientCode;

    @Size(max = 10)
    @Column(name = "firstName", length = 10)
    private String firstName;

    @Size(max = 10)
    @Column(name = "lastName", length = 10)
    private String lastName;

    @Size(max = 10)
    @Column(name = "status", length = 10)
    private String status;

    @Size(max = 10)
    @Column(name = "practice_code", length = 10)
    private String practice;

    @Size(max = 10)
    @Column(name = "arbAmount", length = 10)
    private String arbAmount;

    @Size(max = 10)
    @Column(name = "attorneyName", length = 10)
    private String attorneyName;

    @Size(max = 10)
    @Column(name = "paymentAmount", length = 10)
    private String paymentAmount;
    
    @Size(max = 10)
    @Column(name = "paymentDate")
    private LocalDate paymentDate;
    
    @Size(max = 10)
    @Column(name = "sendDate", length = 10)
    private LocalDate sentDate;
    @Size(max = 10)
    @Column(name = "arbNotes", length = 10)
    private String arbNotes;
    
    @Size(max = 10)
    @Column(name = "statusDate")
    private LocalDate statusDate;

    @Size(max = 10)
    @Column(name = "caseType", length = 10)
    private String caseType;

    @Size(max = 10)
    @Column(name = "processedDate")
    private LocalDate processedDate;

    @Size(max = 200)
    @Column(name = "nature_of_dispute")
    private String natureOfDispute;
    
    
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPractice() {
		return practice;
	}

	public void setPractice(String practice) {
		this.practice = practice;
	}

	public String getArbAmount() {
		return arbAmount;
	}

	public void setArbAmount(String arbAmount) {
		this.arbAmount = arbAmount;
	}

	public String getAttorneyName() {
		return attorneyName;
	}

	public void setAttorneyName(String attorneyName) {
		this.attorneyName = attorneyName;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public LocalDate getSentDate() {
		return sentDate;
	}

	public void setSentDate(LocalDate sentDate) {
		this.sentDate = sentDate;
	}

	public String getArbNotes() {
		return arbNotes;
	}

	public void setArbNotes(String arbNotes) {
		this.arbNotes = arbNotes;
	}

	public LocalDate getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(LocalDate statusDate) {
		this.statusDate = statusDate;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public LocalDate getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(LocalDate processedDate) {
		this.processedDate = processedDate;
	}

	
	public String getNatureOfDispute() {
		return natureOfDispute;
	}

	public void setNatureOfDispute(String natureOfDispute) {
		this.natureOfDispute = natureOfDispute;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reports attorney = (Reports) o;
        if (attorney.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), attorney.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "Reports [id=" + id + ", patientCode=" + patientCode
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", status=" + status + ", practice=" + practice
				+ ", arbAmount=" + arbAmount + ", attorneyName=" + attorneyName
				+ ", paymentAmount=" + paymentAmount + ", paymentDate="
				+ paymentDate + ", sentDate=" + sentDate + ", arbNotes="
				+ arbNotes + ", statusDate=" + statusDate + ", caseType="
				+ caseType + ", processedDate=" + processedDate + ", natureOfDispute="+natureOfDispute+ "]";
	}

    
}
