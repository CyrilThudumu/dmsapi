package com.dms.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Practice.
 */
@Entity
@Table(name = "practice")
public class Practice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "practice_code", length = 30, nullable = false, unique = true)
    private String practiceCode;

    @NotNull
    @Size(max = 200)
    @Column(name = "practice_name", length = 200, nullable = false)
    private String practiceName;

    @Size(max = 100)
    @Column(name = "address_1", length = 100)
    private String address1;

    @Size(max = 50)
    @Column(name = "address_2", length = 50)
    private String address2;

    @Size(max = 20)
    @Column(name = "city", length = 20)
    private String city;

    @Size(max = 20)
    @Column(name = "state_province", length = 20)
    private String stateProvince;

    @Column(name = "zip_code")
    private String zipCode;
    
 
    @Column(name = "tin_number")
    private String tinNumber;
       

	@Column(name = "group_npi")
    private String groupnpi;
    
    @Column(name = "file_name")
    private String fileName;
    
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name = "phone")
    private String phone;
    
    @Column(name = "fax")
    private String fax;
   
     
    @OneToMany(mappedBy = "practice")
    private Set<Documenttype> documenttypes = new HashSet<>();
    @ManyToOne(optional = false)
    
    @NotNull
    @JsonIgnoreProperties("")
    private User user;
    
    public User getUser() {
        return user;
    }

    public Practice user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Practice documenttypes(Set<Documenttype> documenttypes) {
        this.documenttypes = documenttypes;
        return this;
    }

    public Practice addDocumenttype(Documenttype documenttype) {
        this.documenttypes.add(documenttype);
        documenttype.setPractice(this);
        return this;
    }

    public Practice removeDocumenttype(Documenttype documenttype) {
        this.documenttypes.remove(documenttype);
        documenttype.setPractice(null);
        return this;
    }
    
  
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPracticeCode() {
        return practiceCode;
    }

    public Practice practiceCode(String practiceCode) {
        this.practiceCode = practiceCode;
        return this;
    }

    public void setPracticeCode(String practiceCode) {
        this.practiceCode = practiceCode;
    }

    public String getPracticeName() {
        return practiceName;
    }

    public Practice practiceName(String practiceName) {
        this.practiceName = practiceName;
        return this;
    }

    public void setPracticeName(String practiceName) {
        this.practiceName = practiceName;
    }

    public String getAddress1() {
        return address1;
    }

    public Practice address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public Practice address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public Practice city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public Practice stateProvince(String stateProvince) {
    	
        this.stateProvince = stateProvince;
        return this;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getZipCode() {
        return zipCode;
    }
     
    public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	public Practice zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Set<Documenttype> getDocumenttypes() {
        return documenttypes;
    }

   

    public void setDocumenttypes(Set<Documenttype> documenttypes) {
        this.documenttypes = documenttypes;
    }

    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public String getGroupnpi() {
		return groupnpi;
	}

	public void setGroupnpi(String groupnpi) {
		this.groupnpi = groupnpi;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Practice practice = (Practice) o;
        if (practice.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), practice.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Practice{" +
            "id=" + getId() +
            ", practiceCode='" + getPracticeCode() + "'" +
            ", practiceName='" + getPracticeName() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", city='" + getCity() + "'" +
            ", stateProvince='" + getStateProvince() + "'" +
            ", zipCode=" + getZipCode() +
            ", tinNumber=" + getTinNumber() +
            ", fileName=" + getFileName() +
            "}";
    }
}
