package com.dms.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "caselog")
public class Caselog implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(min = 0,max = 30)
	@Column(name = "case_id")
	private String caseId;
	
	@Size(max = 30)
	@NotBlank
	@Column(name = "patient_code")
	private String patientCode;
	
	@Size(max = 30)
	@Column(name = "from_dos")
	@NotBlank
	private String fromDos;
	
	@Size(max = 30)
	@Column(name = "to_dos")
	@NotNull
	private String toDos;
	
	@Size(min = 0,max = 200)
	@Column(name = "case_comments")
	private String caseComments;
	
	@Size(max = 30)
	@Column(name = "commented_by")
	@NotNull
	private String commentedBy;
	
	@Size(max = 30)
	@Column(name = "commented_date")
	@NotNull
	private String commentedDate;
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPatientCode() {
		return patientCode;
	}


	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}


	public String getFromDos() {
		return fromDos;
	}


	public void setFromDos(String fromDos) {
		this.fromDos = fromDos;
	}


	public String getToDos() {
		return toDos;
	}


	public void setToDos(String toDos) {
		this.toDos = toDos;
	}

	
	public String getCaseId() {
		return caseId;
	}


	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}


	public String getCaseComments() {
		return caseComments;
	}


	public void setCaseComments(String caseComments) {
		this.caseComments = caseComments;
	}


	public String getCommentedBy() {
		return commentedBy;
	}


	public void setCommentedBy(String commentedBy) {
		this.commentedBy = commentedBy;
	}


	public String getCommentedDate() {
		return commentedDate;
	}


	public void setCommentedDate(String commentedDate) {
		this.commentedDate = commentedDate;
	}


	@Override
	public String toString() {
		return "Caselog [id=" + id + ", caseId=" + caseId + ", patientCode=" + patientCode + ", fromDos=" + fromDos
				+ ", toDos=" + toDos + ", caseComments=" + caseComments + ", commentedBy=" + commentedBy
				+ ", commentedDate=" + commentedDate + "]";
	}


	/*@Override
	public String toString() {
		return "Documents [id=" + id 
				+ ", fromDos=" + fromDos + ", toDos=" + toDos
				+ ", patientCode=" + patientCode 
				+ ", caseId=" + caseId
				+ ", caseComments=" + caseComments + "]";
	}*/

	
}
