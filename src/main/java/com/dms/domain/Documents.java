package com.dms.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "documents")
public class Documents implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(max = 30)
	@Column(name = "DOCUMENT_TYPE")
	private String documentTypes;
	
	@Size(max = 30)
	@Column(name = "FROM_DOS")
	@NotBlank
	private String fromDos;
	
	@Size(max = 30)
	@Column(name = "to_dos")
	@NotNull
	private String toDos;
	
	@Size(max = 30)
	@NotBlank
	@Column(name = "PATIENT_CODE")
	private String patientCode;

	@Size(max = 100)
	@Column(name = "DOCUMENT_NAME")
	@NotBlank
	private String documentName;

	@Size(max = 30)
	@Column(name = "PRACTICE_CODE")
	private String practiceCode;
	
	@Size(max = 30)
	@Column(name = "DOCTOR_CODE")
	private String doctorCode;
	
	
	@Size(max = 300)
	@Column(name = "FILE_PATH")
	private String filePath;
	
	@Size(min = 0,max = 30)
	@Column(name = "STATUS")
	private String status;
	
	
	@Size(min = 0,max = 30)
	@Column(name = "STATUS_DATE")
	private String statusDate;
	
	@Size(min = 0,max = 30)
	@Column(name = "ATTORNEY_ASSIGNED_DATE")
	private String attorneyAssignedDate;


	@Size(min = 0,max = 30)
	@Column(name = "ARB_AMOUNT")
	private String arbAmount;

	
	@Size(max = 30)
	@Column(name = "ATTORNEY_CODE")
	private String attorneyCode;
	
	@Column(name = "PAYMENT_DATE")
	private String paymentDate;
	
	@Size(min = 0,max = 30)
	@Column(name = "PAYMENT_AMOUNT")
	private String paymentAmount;

	@Size(min = 0,max = 50)
	@Column(name = "ARB_NOTES")
	private String arbNotes;

	@Size(min = 0,max = 50)
	@Column(name = "NATURE_OF_DISPUTE")
	private String natureOfDispute;
	
	@Column(name = "SENT_DATE")
	private String sentDate;
	
	
	@Column(name = "PROCESSED_DATE")
	private String processedDate;
	
	@Column(name = "PROCESSED_FLAG")
	private Boolean processedFlag;
	
	//, columnDefinition="BOOLEAN DEFAULT false"   @dynamicInsert=true
	@Column(name = "ADDENDUM_TO_PRIOR_FLAG")
	private Boolean addendumToPriorFlag;
		
	@Size(min = 0,max = 30)
	@Column(name = "case_id")
	private String caseId;

	@Size(min = 0,max = 200)
	@Column(name = "case_comments")
	private String caseComments;
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDocumentName() {
		return documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getPatientCode() {
		return patientCode;
	}


	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}



	public String getPracticeCode() {
		return practiceCode;
	}


	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}


	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String getFromDos() {
		return fromDos;
	}


	public void setFromDos(String fromDos) {
		this.fromDos = fromDos;
	}


	public String getToDos() {
		return toDos;
	}


	public void setToDos(String toDos) {
		this.toDos = toDos;
	}


	public String getDocumentTypes() {
		return documentTypes;
	}


	public void setDocumentTypes(String documentTypes) {
		this.documentTypes = documentTypes;
	}


	public String getDoctorCode() {
		return doctorCode;
	}


	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}


	public String getAttorneyCode() {
		return attorneyCode;
	}


	public void setAttorneyCode(String attorneyCode) {
		this.attorneyCode = attorneyCode;
	}
	
	
	public String getArbAmount() {
		return arbAmount;
	}


	public void setArbAmount(String arbAmount) {
		this.arbAmount = arbAmount;
	}


	public String getArbNotes() {
		return arbNotes;
	}


	public void setArbNotes(String arbNotes) {
		this.arbNotes = arbNotes;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}


	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	public String getNatureOfDispute() {
		return natureOfDispute;
	}


	public void setNatureOfDispute(String natureOfDispute) {
		this.natureOfDispute = natureOfDispute;
	}



	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDate() {
		return statusDate;
	}


	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}

	public String getAttorneyAssignedDate() {
		return attorneyAssignedDate;
	}


	public void setAttorneyAssignedDate(String attorneyAssignedDate) {
		this.attorneyAssignedDate = attorneyAssignedDate;
	}
	
	public String getPaymentDate() {
		return paymentDate;
	}


	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}


	public String getSentDate() {
		return sentDate;
	}


	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}


	public String getProcessedDate() {
		return processedDate;
	}


	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}


	public Boolean isProcessedFlag() {
		return processedFlag;
	}


	public void setProcessedFlag(Boolean processedFlag) {
		this.processedFlag = processedFlag;
	}

	public Boolean isAddendumToPriorFlag() {
		return addendumToPriorFlag;
	}


	public void setAddendumToPriorFlag(Boolean addendumToPriorFlag) {
		this.addendumToPriorFlag = addendumToPriorFlag;
	}

	public String getCaseId() {
		return caseId;
	}


	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}


	public String getCaseComments() {
		return caseComments;
	}


	public void setCaseComments(String caseComments) {
		this.caseComments = caseComments;
	}


	@Override
	public String toString() {
		return "Documents [id=" + id + ", documentTypes=" + documentTypes
				+ ", fromDos=" + fromDos + ", toDos=" + toDos
				+ ", patientCode=" + patientCode + ", documentName="
				+ documentName + ", practiceCode=" + practiceCode
				+ ", doctorCode=" + doctorCode + ", filePath=" + filePath
				+ ", status=" + status + ", statusDate=" + statusDate
				+ ", arbAmount=" + arbAmount + ", attorneyCode=" + attorneyCode
				+ ", paymentDate=" + paymentDate + ", paymentAmount="
				+ paymentAmount + ", arbNotes=" + arbNotes
				+ ", natureOfDispute=" + natureOfDispute + ", sentDate="
				+ sentDate + ", processedDate=" + processedDate
				+ ", processedFlag=" + processedFlag + ","
				+ ", attorneyAssignedDate=" + attorneyAssignedDate + ","
				+ ", addendumToPriorFlag=" + addendumToPriorFlag + ","
				+ " caseId=" + caseId
				+ ", caseComments=" + caseComments + "]";
	}


	
	
	/*@Override
	public String toString() {
		return "Documents [id=" + id + ", documentTypes=" + documentTypes
				+ ", fromDos=" + fromDos + ", toDos=" + toDos
				+ ", patientCode=" + patientCode + ", documentName="
				+ documentName + ", practiceCode=" + practiceCode
				+ ", doctorCode=" + doctorCode + ", filePath=" + filePath
				+ ", status=" + status + ", statusDate=" + statusDate
				+ ", arbAmount=" + arbAmount + ", attorneyCode=" + attorneyCode
				+ ", paymentDate=" + paymentDate + ", paymentAmount="
				+ paymentAmount + ", arbNotes=" + arbNotes
				+ ", natureOfDispute=" + natureOfDispute + ", sentDate="
				+ sentDate + ", processedDate=" + processedDate
				+ ", processedFlag=" + processedFlag + "]";
	}*/


/*	@Override
	public String toString() {
		return "Documents [id=" + id + ", "
				+ "documentName=" + documentName 
				+ ", fromDos=" + fromDos 
				+ ", toDos=" + toDos
				+ ", patientCode=" + patientCode 
				+ ", documentTypes=" + documentTypes 
				+ ", practiceCode=" + practiceCode 
				+ ", doctorCode=" + doctorCode 
				+ ", attorneyCode=" + attorneyCode 
				+ ", filePath=" + filePath 
				+ ", arbAmount=" + arbAmount 
				+ ", arbNotes=" + arbNotes
				+ ", paymentAmount=" + paymentAmount 
				+ ", sentDate=" + sentDate 
				+ ", paymentDate=" + paymentDate
				+ ", natureOfDispute=" + natureOfDispute 
				+ ", processedDate=" + processedDate + "]";
	}*/
	


	
	
}
