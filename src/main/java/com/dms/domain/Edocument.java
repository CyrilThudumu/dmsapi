package com.dms.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Edocument.
 */
@Entity
@Table(name = "edocument")
public class Edocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "document_code", length = 30, nullable = false, unique = true)
    private String documentCode;

    @NotNull
    @Size(max = 60)
    @Column(name = "document_name", length = 60, nullable = false)
    private String documentName;

    @NotNull
    @Column(name = "from_field", nullable = false)
    private LocalDate fromField;

    @NotNull
    @Column(name = "to_field", nullable = false)
    private LocalDate toField;

    @Size(max = 300)
    @Column(name = "notes", length = 300)
    private String notes;

    @NotNull
    @Size(max = 300)
    @Column(name = "url", length = 300, nullable = false)
    private String url;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttype;

    @ManyToMany(mappedBy = "edocuments")
    @JsonIgnore
    private Set<Patient> patients = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public Edocument documentCode(String documentCode) {
        this.documentCode = documentCode;
        return this;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getDocumentName() {
        return documentName;
    }

    public Edocument documentName(String documentName) {
        this.documentName = documentName;
        return this;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public LocalDate getFromField() {
        return fromField;
    }

    public Edocument fromField(LocalDate fromField) {
        this.fromField = fromField;
        return this;
    }

    public void setFromField(LocalDate fromField) {
        this.fromField = fromField;
    }

    public LocalDate getToField() {
        return toField;
    }

    public Edocument toField(LocalDate toField) {
        this.toField = toField;
        return this;
    }

    public void setToField(LocalDate toField) {
        this.toField = toField;
    }

    public String getNotes() {
        return notes;
    }

    public Edocument notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUrl() {
        return url;
    }

    public Edocument url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Documenttype getDocumenttype() {
        return documenttype;
    }

    public Edocument documenttype(Documenttype documenttype) {
        this.documenttype = documenttype;
        return this;
    }

    public void setDocumenttype(Documenttype documenttype) {
        this.documenttype = documenttype;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public Edocument patients(Set<Patient> patients) {
        this.patients = patients;
        return this;
    }

    public Edocument addPatient(Patient patient) {
        this.patients.add(patient);
        patient.getEdocuments().add(this);
        return this;
    }

    public Edocument removePatient(Patient patient) {
        this.patients.remove(patient);
        patient.getEdocuments().remove(this);
        return this;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Edocument edocument = (Edocument) o;
        if (edocument.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), edocument.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Edocument{" +
            "id=" + getId() +
            ", documentCode='" + getDocumentCode() + "'" +
            ", documentName='" + getDocumentName() + "'" +
            ", fromField='" + getFromField() + "'" +
            ", toField='" + getToField() + "'" +
            ", notes='" + getNotes() + "'" +
            ", url='" + getUrl() + "'" +
            "}";
    }
}
