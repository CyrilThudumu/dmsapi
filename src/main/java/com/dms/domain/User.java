package com.dms.domain;

import com.dms.config.Constants;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.BatchSize;
import javax.validation.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.time.Instant;

/**
 * A user.
 */
@Entity	
@Table(name = "jhi_user")

public class User extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 6, max = 60)
    @Column(name = "password_hash", length = 60, nullable = false)
    private String password;

    @NotNull
    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @NotNull
    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = true)
    private String email;

    @Column(name = "taxonomy")
    private Long taxonomy;
    
    @Column(name = "provider_npi")
    private Long providerNpi;
    
    @Size(max = 25)
    @Column(name = "speciality")
    private String speciality;
    
    public String getAttornyCode() {
		return attornyCode;
	}

	public void setAttornyCode(String attornyCode) {
		this.attornyCode = attornyCode;
	}

	@Size(max = 30)
	@Column(name = "ATTORNY_CODE")
	private String attornyCode;
    
    private String phone;
    
    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@NotNull
    @Column(nullable = false)
    private boolean activated = false;

    @Size(min = 2, max = 6)
    @Column(name = "lang_key", length = 6)
    private String langKey;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    private String resetKey;
    
    @NotNull
    @Size(max = 10)
    @Column(name = "user_code", length = 10)
    private String userCode;

    @Column(name = "reset_date")
    private Instant resetDate = null;	
	
    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "jhi_user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    
    @BatchSize(size = 20)
    private Set<Authority> authorities = new HashSet<>();
    
    
    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "user_practice",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "practice_code", referencedColumnName = "practice_code")})
    
    @BatchSize(size = 20)
    private Set<Practice> practices = new HashSet<>();

	public Set<Practice> getPractices() {
		return practices;
	}

	public void setPractices(Set<Practice> practices) {
		this.practices = practices;
	}
	
	
	@ManyToMany
	@JoinTable(name="attorney_masterAttorney",
		joinColumns={@JoinColumn(name="user_id")},
		inverseJoinColumns={@JoinColumn(name="master_attorney_id")})
	private Set<User> masterAttorney = new HashSet<User>();

	@ManyToMany(mappedBy="masterAttorney")
	private Set<User> subAttorneys = new HashSet<User>();
	
	public void removeMasterAttorney(User u) {
	    this.masterAttorney.remove(u);
	    u.getSubAttorneys().remove(this);
	   
	    }
		
		 public Set<User> getMasterAttorney() {
				return masterAttorney;
			}

			public void setMasterAttorney(Set<User> masterAttorney) {
				this.masterAttorney = masterAttorney;
			}

			public Set<User> getSubAttorneys() {
				return subAttorneys;
			}

			public void setSubAttorneys(Set<User> subAttorneys) {
				this.subAttorneys = subAttorneys;
			}
			
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    // Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = StringUtils.lowerCase(login, Locale.ENGLISH);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    
    public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	

    public  Long getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(Long taxonomy) {
		this.taxonomy = taxonomy;
	}

	public Long getProviderNpi() {
		return providerNpi;
	}

	public void setProviderNpi(Long providerNpi) {
		this.providerNpi = providerNpi;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}



	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;
        return !(user.getId() == null || getId() == null) && Objects.equals(getId(), user.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "User{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated='" + activated + '\'' +
            ", langKey='" + langKey + '\'' +
            ", userCode='" + userCode + '\'' +
            ", attornyCode='" + attornyCode + '\'' +
            ", activationKey='" + activationKey + '\'' +
            ", taxonomy='" + taxonomy + '\'' +
            ", speciality='" + speciality + '\'' +
           //  ", practices='" + practices + '\'' +
            ", providerNpi='" + providerNpi + '\'' +
            "}";
    }
}
