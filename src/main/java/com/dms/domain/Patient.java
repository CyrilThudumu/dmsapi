
package com.dms.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Patient.
 */
@Entity
@Table(name = "patient")
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
   
    
    @NotNull
    @Size(max = 30)
    @Column(name = "patient_code", length = 30, nullable = false, unique = true)
    private String patientCode;

    @NotNull
    @Size(max = 50)
    @Column(name = "first_name", length = 50, nullable = false)
    private String firstName;

    @NotNull
    @Size(max = 50)
    @Column(name = "last_name", length = 50, nullable = false)
    private String lastName;
 
    @Size(max = 50)
    @Column(name = "initial", length = 50, nullable = false)
    private String initial;
    
    @Column(name = "sex", length = 10, nullable = false)
    private String sex;

    @Size(max = 50)
    @Column(name = "address", length = 50)
    private String address;

    @Size(max = 20)
    @Column(name = "city", length = 10)
    private String city;

    @Column(name = "state", length = 10)
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "email", length = 20)
    private String email;

    
    @Column(name = "mobile_no")
    private String mobileNo;

    @Column(name = "jhi_enable")
    private Boolean enable;

    @Column(name = "patient_status", length = 10)
    private String patientStatus;

    @Column(name = "status_notes", length = 50)
    private String statusNotes;

    @NotNull
    @Column(name = "dob", nullable = false)
    private LocalDate dob;

    @Column(name = "case_type", length = 10)
    private String caseType;

    @Column(name = "work_phone")
    private String workPhone;

    @Column(name = "home_phone")
    private String homePhone;

    @Column(name = "age")
    private Integer age;

    @Column(name = "ins_first_name")
    private String insFirstName;

    @Column(name = "ins_last_name")
    private String insLastName;

    @Column(name = "ins_initial")
    private String insInitial;

    @Column(name = "ins_address")
    private String insAddress;

    @Column(name = "ins_city")
    private String insCity;

    @Column(name = "ins_state")
    private String insState;

    @Column(name = "ins_zip_code")
    private String insZipCode;

    @Column(name = "ins_phone_no")
    private String insPhoneNo;

    @Column(name = "primary_ins_co")
    private String primaryInsCo;

    @Column(name = "primary_ins_claim_no")
    private String primaryInsClaimNo;

    @Column(name = "primary_ins_policy_no")
    private String primaryInsPolicyNo;

    @Column(name = "primary_ins_relation")
    private String primaryInsRelation;

    @Column(name = "primary_ins_certifier")
    private String primaryInsCertifier;
    
    @Column(name = "primary_ins_adjuster")
    private String primaryInsAdjuster;
       
    @Column(name = "primary_ins_adjuster_fax")
    private String primaryInsAdjusterFax;

    @Column(name = "primary_ins_notes")
    private String primaryInsNotes;
    

    @Column(name = "doa")
    private LocalDate doa;
    
    @Column(name = "first_visit")
    private LocalDate firstVisit;

   
    @Column(name = "sec_ins_co")
    private String secInsCo;

    
    @Column(name = "sec_ins_claim_no")
    private String secInsClaimNo;

    @Column(name = "sec_ins_policy_no")
    private String secInsPolicyNo;

    @Column(name = "sec_ins_relation")
    private String secInsRelation;

    @Column(name = "sec_ins_notes")
    private String secInsNotes;
    
    @Column(name = "sec_ins_certifier")
    private String secInsCertifier;
    
    @Column(name = "sec_ins_adjuster")
    private String secInsAdjuster;
    
    @Column(name = "sec_ins_adjuster_fax")
    private String secInsAdjusterFax;


    @Column(name = "addn_info")
    private String addnInfo;

    @Column(name = "attorney_first_name")
    private String attorneyFirstName;

    
    @Column(name = "attorney_last_name")
    private String attorneyLastName;
    
    @Column(name = "attorney_address")
    private String attorneyAddress;

    @Column(name = "attorney_city")
    private String attorneyCity;

    @Column(name = "attorney_state")
    private String attorneyState;

    
    @Column(name = "attorney_zip_code")
    private String attorneyZipCode;
    	
	@Column(name = "attorney_mobile_no")
	private String attorneyMobileNo;
	
    @Column(name = "attorney_fax_no")
    private String attorneyFaxNo;
    
    @Column(name = "practice_code")
    private String practiceCode;
    
    @Column(name = "attorney_code")
    private String attorneyCode;
    
    @Column(name = "missing_info")
    private String missingInfo;
    
    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;


    @ManyToMany
    @JoinTable(name = "patient_edocument",
               joinColumns = @JoinColumn(name = "patients_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "edocuments_id", referencedColumnName = "id"))
    private Set<Edocument> edocuments = new HashSet<>();

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public Patient patientCode(String patientCode) {
        this.patientCode = patientCode;
        return this;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public Patient firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Patient lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public Patient sex(String sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public Patient address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public Patient city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public Patient state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Patient zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public Patient email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public Patient mobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Boolean isEnable() {
        return enable;
    }

    public Patient enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public Patient patientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
        return this;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public String getStatusNotes() {
        return statusNotes;
    }

    public Patient statusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
        return this;
    }

    public void setStatusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
    }

    public LocalDate getDob() {
        return dob;
    }

    public Patient dob(LocalDate dob) {
        this.dob = dob;
        return this;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getCaseType() {
        return caseType;
    }

    public Patient caseType(String caseType) {
        this.caseType = caseType;
        return this;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public Patient workPhone(String workPhone) {
        this.workPhone = workPhone;
        return this;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public Integer getAge() {
        return age;
    }

    public Patient age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getInsFirstName() {
        return insFirstName;
    }

    public Patient insFirstName(String insFirstName) {
        this.insFirstName = insFirstName;
        return this;
    }

    public void setInsFirstName(String insFirstName) {
        this.insFirstName = insFirstName;
    }

    public String getInsLastName() {
        return insLastName;
    }

    public Patient insLastName(String insLastName) {
        this.insLastName = insLastName;
        return this;
    }

    public void setInsLastName(String insLastName) {
        this.insLastName = insLastName;
    }

    public String getInsInitial() {
        return insInitial;
    }

    public Patient insInitial(String insInitial) {
        this.insInitial = insInitial;
        return this;
    }

    public void setInsInitial(String insInitial) {
        this.insInitial = insInitial;
    }

    public String getInsAddress() {
        return insAddress;
    }

    public Patient insAddress(String insAddress) {
        this.insAddress = insAddress;
        return this;
    }

    public void setInsAddress(String insAddress) {
        this.insAddress = insAddress;
    }

    public String getInsCity() {
        return insCity;
    }

    public Patient insCity(String insCity) {
        this.insCity = insCity;
        return this;
    }

    public void setInsCity(String insCity) {
        this.insCity = insCity;
    }

    public String getInsState() {
        return insState;
    }

    public Patient insState(String insState) {
        this.insState = insState;
        return this;
    }

    public void setInsState(String insState) {
        this.insState = insState;
    }

    public String getInsZipCode() {
        return insZipCode;
    }

    public Patient insZipCode(String insZipCode) {
        this.insZipCode = insZipCode;
        return this;
    }

    public void setInsZipCode(String insZipCode) {
        this.insZipCode = insZipCode;
    }

    public String getInsPhoneNo() {
        return insPhoneNo;
    }

    public Patient insPhoneNo(String insPhoneNo) {
        this.insPhoneNo = insPhoneNo;
        return this;
    }

    public void setInsPhoneNo(String insPhoneNo) {
        this.insPhoneNo = insPhoneNo;
    }

    public String getPrimaryInsCo() {
        return primaryInsCo;
    }

    public Patient primaryInsCo(String primaryInsCo) {
        this.primaryInsCo = primaryInsCo;
        return this;
    }

    public void setPrimaryInsCo(String primaryInsCo) {
        this.primaryInsCo = primaryInsCo;
    }

    public String getPrimaryInsClaimNo() {
        return primaryInsClaimNo;
    }

    public Patient primaryInsClaimNo(String primaryInsClaimNo) {
        this.primaryInsClaimNo = primaryInsClaimNo;
        return this;
    }

    public void setPrimaryInsClaimNo(String primaryInsClaimNo) {
        this.primaryInsClaimNo = primaryInsClaimNo;
    }

    public String getPrimaryInsPolicyNo() {
        return primaryInsPolicyNo;
    }

    public Patient primaryInsPolicyNo(String primaryInsPolicyNo) {
        this.primaryInsPolicyNo = primaryInsPolicyNo;
        return this;
    }

    public void setPrimaryInsPolicyNo(String primaryInsPolicyNo) {
        this.primaryInsPolicyNo = primaryInsPolicyNo;
    }

    public String getPrimaryInsRelation() {
        return primaryInsRelation;
    }

    public Patient primaryInsRelation(String primaryInsRelation) {
        this.primaryInsRelation = primaryInsRelation;
        return this;
    }

    public void setPrimaryInsRelation(String primaryInsRelation) {
        this.primaryInsRelation = primaryInsRelation;
    }

    public String getPrimaryInsNotes() {
        return primaryInsNotes;
    }

    public Patient primaryInsNotes(String primaryInsNotes) {
        this.primaryInsNotes = primaryInsNotes;
        return this;
    }

    public void setPrimaryInsNotes(String primaryInsNotes) {
        this.primaryInsNotes = primaryInsNotes;
    }

    public LocalDate getDoa() {
        return doa;
    }

    public Patient doa(LocalDate doa) {
        this.doa = doa;
        return this;
    }

    public void setDoa(LocalDate doa) {
        this.doa = doa;
    }

    public String getSecInsClaimNo() {
        return secInsClaimNo;
    }

    public Patient secInsClaimNo(String secInsClaimNo) {
        this.secInsClaimNo = secInsClaimNo;
        return this;
    }

    public void setSecInsClaimNo(String secInsClaimNo) {
        this.secInsClaimNo = secInsClaimNo;
    }

    public String getSecInsPolicyNo() {
        return secInsPolicyNo;
    }

    public Patient secInsPolicyNo(String secInsPolicyNo) {
        this.secInsPolicyNo = secInsPolicyNo;
        return this;
    }

    public void setSecInsPolicyNo(String secInsPolicyNo) {
        this.secInsPolicyNo = secInsPolicyNo;
    }

    public String getSecInsRelation() {
        return secInsRelation;
    }

    public Patient secInsRelation(String secInsRelation) {
        this.secInsRelation = secInsRelation;
        return this;
    }

    public void setSecInsRelation(String secInsRelation) {
        this.secInsRelation = secInsRelation;
    }

    public String getSecInsNotes() {
        return secInsNotes;
    }

    public Patient secInsNotes(String secInsNotes) {
        this.secInsNotes = secInsNotes;
        return this;
    }

    public void setSecInsNotes(String secInsNotes) {
        this.secInsNotes = secInsNotes;
    }

    public String getAddnInfo() {
        return addnInfo;
    }

    public Patient addnInfo(String addnInfo) {
        this.addnInfo = addnInfo;
        return this;
    }

    public void setAddnInfo(String addnInfo) {
        this.addnInfo = addnInfo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Patient createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Patient creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Patient updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public Patient updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

	/*
	 * public User getAttorny() { return attorny; }
	 * 
	 * public Patient attorny(User user) { this.attorny = user; return this; }
	 * 
	 * public void setAttorny(User user) { this.attorny = user; }
	 * 
	 * public User getDoctor() { return doctor; }
	 * 
	 * public Patient doctor(User user) { this.doctor = user; return this; }
	 * 
	 * public void setDoctor(User user) { this.doctor = user; }
	 */

    public Set<Edocument> getEdocuments() {
        return edocuments;
    }

    public Patient edocuments(Set<Edocument> edocuments) {
        this.edocuments = edocuments;
        return this;
    }

    public Patient addEdocument(Edocument edocument) {
        this.edocuments.add(edocument);
        edocument.getPatients().add(this);
        return this;
    }

    public String getPrimaryInsCertifier() {
		return primaryInsCertifier;
	}

	public void setPrimaryInsCertifier(String primaryInsCertifier) {
		this.primaryInsCertifier = primaryInsCertifier;
	}

	public String getPrimaryInsAdjuster() {
		return primaryInsAdjuster;
	}

	public void setPrimaryInsAdjuster(String primaryInsAdjuster) {
		this.primaryInsAdjuster = primaryInsAdjuster;
	}

	public String getPrimaryInsAdjusterFax() {
		return primaryInsAdjusterFax;
	}

	public void setPrimaryInsAdjusterFax(String primaryInsAdjusterFax) {
		this.primaryInsAdjusterFax = primaryInsAdjusterFax;
	}

	public LocalDate getFirstVisit() {
		return firstVisit;
	}

	public void setFirstVisit(LocalDate firstVisit) {
		this.firstVisit = firstVisit;
	}

	public String getSecInsCo() {
		return secInsCo;
	}

	public void setSecInsCo(String secInsCo) {
		this.secInsCo = secInsCo;
	}

	public String getSecInsCertifier() {
		return secInsCertifier;
	}

	public void setSecInsCertifier(String secInsCertifier) {
		this.secInsCertifier = secInsCertifier;
	}

	public String getSecInsAdjuster() {
		return secInsAdjuster;
	}

	public void setSecInsAdjuster(String secInsAdjuster) {
		this.secInsAdjuster = secInsAdjuster;
	}

	public String getSecInsAdjusterFax() {
		return secInsAdjusterFax;
	}

	public void setSecInsAdjusterFax(String secInsAdjusterFax) {
		this.secInsAdjusterFax = secInsAdjusterFax;
	}

	public String getAttorneyFirstName() {
		return attorneyFirstName;
	}

	public void setAttorneyFirstName(String attorneyFirstName) {
		this.attorneyFirstName = attorneyFirstName;
	}

	public String getAttorneyLastName() {
		return attorneyLastName;
	}

	public void setAttorneyLastName(String attorneyLastName) {
		this.attorneyLastName = attorneyLastName;
	}

	public String getAttorneyAddress() {
		return attorneyAddress;
	}

	public void setAttorneyAddress(String attorneyAddress) {
		this.attorneyAddress = attorneyAddress;
	}

	public String getAttorneCity() {
		return attorneyCity;
	}

	public void setAttorneyCity(String attorneyCity) {
		this.attorneyCity = attorneyCity;
	}

	public String getAttorneyState() {
		return attorneyState;
	}

	public void setAttorneyState(String attorneyState) {
		this.attorneyState = attorneyState;
	}

	public String getAttorneyZipCode() {
		return attorneyZipCode;
	}

	public void setAttorneyZipCode(String attorneyZipCode) {
		this.attorneyZipCode = attorneyZipCode;
	}

	public String getAttorneyMobileNo() {
		return attorneyMobileNo;
	}

	public void setAttorneyMobileNo(String attorneyMobileNo) {
		this.attorneyMobileNo = attorneyMobileNo;
	}

	public String getAttorneyFaxNo() {
		return attorneyFaxNo;
	}

	public void setAttoneyFaxNo(String attorneyFaxNo) {
		this.attorneyFaxNo = attorneyFaxNo;
	}

	public String getPracticeCode() {
		return practiceCode;
	}

	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}

	
//	public Integer getAttornyId() {
//		return attornyId;
//	}
//
//	public void setAttornyId(Integer attornyId) {
//		this.attornyId = attornyId;
//	}

	public String getAttorneyCity() {
		return attorneyCity;
	}

	public Patient removeEdocument(Edocument edocument) {
        this.edocuments.remove(edocument);
        edocument.getPatients().remove(this);
        return this;
    }

    public void setEdocuments(Set<Edocument> edocuments) {
        this.edocuments = edocuments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove



	public String getMissingInfo() {
		return missingInfo;
	}

	public void setMissingInfo(String missingInfo) {
		this.missingInfo = missingInfo;
	}
    
    public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	
	

	public String getAttorneyCode() {
		return attorneyCode;
	}

	public void setAttorneyCode(String attorneyCode) {
		this.attorneyCode = attorneyCode;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Patient patient = (Patient) o;
        if (patient.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patient.getId());
    }

  

	@Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	/*public Attorney getAttorney() {
		return attorney;
	}

	public void setAttorney(Attorney attorney) {
		this.attorney = attorney;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}*/

	public Boolean getEnable() {
		return enable;
	}

	

	@Override
	public String toString() {
		return "Patient [id=" + id + 
				", patientCode=" + patientCode +
				", firstName=" + firstName + 
				", lastName=" + lastName +
				", initial=" + initial + 
				", homePhone=" + homePhone +
				", sex=" + sex + 
				", address=" + address + 
				", city=" + city + 
				", state=" + state + 
				", zipCode=" + zipCode + 
				", email=" + email + 
				", mobileNo=" + mobileNo + 
				", enable=" + enable + 
				", patientStatus=" + patientStatus + 
				", statusNotes=" + statusNotes + 
				", dob=" + dob + 
				", caseType=" + caseType + 
				", workPhone=" + workPhone + 
				", age=" + age + 
				", insFirstName=" + insFirstName + 
				", insLastName=" + insLastName + 
				", insInitial=" + insInitial + 
				", insAddress="	+ insAddress + 
				", insCity=" + insCity + 
				", insState="+ insState + 
				", insZipCode=" + insZipCode + 
				", insPhoneNo=" + insPhoneNo + 
				", primaryInsCo=" + primaryInsCo + 
				", primaryInsClaimNo=" + primaryInsClaimNo +
				", primaryInsPolicyNo=" + primaryInsPolicyNo +
				", primaryInsRelation=" + primaryInsRelation +
				", primaryInsCertifier=" + primaryInsCertifier +
				", primaryInsAdjuster=" + primaryInsAdjuster +
				", primaryInsAdjusterFax=" + primaryInsAdjusterFax +
				", primaryInsNotes=" + primaryInsNotes + 
				", doa=" + doa +
				", firstVisit=" + firstVisit + 
				", secInsCo=" + secInsCo + 
				", secInsClaimNo=" + secInsClaimNo + 
				", secInsPolicyNo="	+ secInsPolicyNo + 
				", secInsRelation=" + secInsRelation +
				", secInsNotes=" + secInsNotes + 
				", secInsCertifier=" + secInsCertifier + 
				", secInsAdjuster=" + secInsAdjuster +
				", secInsAdjusterFax=" + secInsAdjusterFax + 
				", addnInfo=" + addnInfo + 
				", attorneyFirstName=" + attorneyFirstName +
				", attorneyLastName=" + attorneyLastName +
				", attorneyAddress=" + attorneyAddress + 
				", attorney_city=" + attorneyCity + 
				", attorneyState=" + attorneyState +
				", attorneyZipCode=" + attorneyZipCode + 
				", attorneyMobileNo=" + attorneyMobileNo + 
				", attorneyFaxNo=" + attorneyFaxNo +
				", missingInfo=" + missingInfo +
				", practiceCode=" + practiceCode + 
				", attorneyCode=" + attorneyCode + 
				", createdBy=" + createdBy +
//				", attorny_id=" + attornyId +
				", creationDate=" + creationDate + 
				", updatedBy=" + updatedBy + "]";
	}    
    
}
