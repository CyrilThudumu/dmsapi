package com.dms.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Casetype.
 */
@Entity
@Table(name = "casetype")
public class Casetype implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "case_type_code", length = 20, nullable = false, unique = true)
    private String caseTypeCode;

    @NotNull
    @Size(max = 20)
    @Column(name = "case_type_name", length = 20, nullable = false)
    private String caseTypeName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseTypeCode() {
        return caseTypeCode;
    }

    public Casetype caseTypeCode(String caseTypeCode) {
        this.caseTypeCode = caseTypeCode;
        return this;
    }

    public void setCaseTypeCode(String caseTypeCode) {
        this.caseTypeCode = caseTypeCode;
    }

    public String getCaseTypeName() {
        return caseTypeName;
    }

    public Casetype caseTypeName(String caseTypeName) {
        this.caseTypeName = caseTypeName;
        return this;
    }

    public void setCaseTypeName(String caseTypeName) {
        this.caseTypeName = caseTypeName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Casetype casetype = (Casetype) o;
        if (casetype.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), casetype.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Casetype{" +
            "id=" + getId() +
            ", caseTypeCode='" + getCaseTypeCode() + "'" +
            ", caseTypeName='" + getCaseTypeName() + "'" +
            "}";
    }
}
