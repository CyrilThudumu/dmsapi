package com.dms.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Certifier.
 */
@Entity
@Table(name = "certifier")
public class Certifier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "certifier_code", length = 20, nullable = false, unique = true)
    private String certifierCode;

    @NotNull
    @Size(max = 20)
    @Column(name = "certifier_name", length = 20, nullable = false)
    private String certifierName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertifierCode() {
        return certifierCode;
    }

    public Certifier certifierCode(String certifierCode) {
        this.certifierCode = certifierCode;
        return this;
    }

    public void setCertifierCode(String certifierCode) {
        this.certifierCode = certifierCode;
    }

    public String getCertifierName() {
        return certifierName;
    }

    public Certifier certifierName(String certifierName) {
        this.certifierName = certifierName;
        return this;
    }

    public void setCertifierName(String certifierName) {
        this.certifierName = certifierName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Certifier casetype = (Certifier) o;
        if (casetype.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), casetype.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Certifier{" +
            "id=" + getId() +
            ", certifierCode='" + getCertifierCode() + "'" +
            ", certifierName='" + getCertifierName() + "'" +
            "}";
    }
}
